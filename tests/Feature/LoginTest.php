<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginTest extends TestCase
{
  /**
   * Redirect the request / to /login
   *
   * @test
   */
  public function it_redirect_to_login()
  {
    $this->withoutExceptionHandling();

    $this->get('/')
      ->assertRedirect(route('login'));
  }

  /**
   * show the login page
   *
   * @test
   */
  public function it_display_the_login_page()
  {
    $this->get(route('login'))
      ->assertStatus(200)
      ->assertViewIs('auth.login')
      ->assertSee('login');
  }

  /**
   * validates that the user and password fields have a value
   *
   * @test
   */
  public function it_user_and_password_are_required()
  {
    // $this->withoutExceptionHandling();

    $this->from(route('login'))
      ->post(route('login'), [
        'user' => '',
        'password' => ''
      ])
      ->assertRedirect(route('login'))
      ->assertSee('login')
      ->assertSessionHasErrors(['user','password']);
  }

  /**
   * validates that the user field has a value
   *
   * @test
   */
  public function it_user_is_required()
  {
    $this->from(route('login'))
      ->post(route('login'), [
        'user' => '',
        'password' => bcrypt(12345)
      ])
      ->assertRedirect(route('login'))
      ->assertSee('login')
      ->assertSessionHasErrors(['user']);
  }

  /**
   * validates that the password field has a value
   *
   * @test
   */
  public function it_password_is_required()
  {
    // $this->withoutExceptionHandling();

    $this->from(route('login'))
      ->post(route('login'), [
        'user' => 'user',
        'password' => ''
      ])
      ->assertRedirect(route('login'))
      ->assertSee('login')
      ->assertSessionHasErrors(['password']);
  }
}
