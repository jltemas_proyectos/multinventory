<?php

Route::get('/', function () {
    return redirect()->route('login');
});

//Rutas de Login
Auth::routes();

//Rutas de Dashboard
Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
Route::get('/dashboard/list-desktops', 'DashboardController@desktop')->name('dashboard.list-desktops');
Route::get('/dashboard/list-laptops', 'DashboardController@laptop')->name('dashboard.list-laptops');
Route::get('/dashboard/list-servers', 'DashboardController@server')->name('dashboard.list-servers');
Route::get('/dashboard/list-peripherals', 'DashboardController@peripheral')->name('dashboard.list-peripherals');
Route::get('/dashboard/list-printers', 'DashboardController@printer')->name('dashboard.list-printers');
Route::get('/dashboard/list-networks', 'DashboardController@network')->name('dashboard.list-networks');

//Rutas de empleados
Route::post('/employees/ajax', 'EmployeeController@ajax');
Route::resource('/employees', 'EmployeeController');

//Rutas de departamentos
Route::resource('/departments', 'DepartmentController');
Route::resource('/positions', 'PositionController');
Route::resource('/articles', 'ArticleController');
Route::resource('/categories', 'CategoryController');
Route::resource('/brands', 'BrandController');

//Rutas de Activos Fijos
Route::get('/fixed-assets/assign-fixed-asset', 'FixedassetController@showFormAssignFixedAsset')->name('fixed-assets.assign-fixed-asset');
Route::post('/fixed-assets/assign-fixed-asset', 'FixedassetController@assignFixedAsset')->name('fixed-assets.assign-fixed-asset');
Route::get('/fixed-assets/assign-fixed-asset/{fixed_asset}/edit', 'FixedassetController@showFormEditAssignFixedAsset')->name('fixed-assets.edit-assign-fixed-asset');
Route::put('/fixed-assets/assign-fixed-asset/{fixed_asset}', 'FixedassetController@updateAssignFixedAsset')->name('fixed-assets.update-assign-fixed-asset');

Route::resource('/fixed-assets', 'FixedassetController');
Route::resource('/maintenances', 'MaintenanceController');
Route::resource('/storage-cellars', 'StoragecellarController');
Route::post('/delivery-certificates/ajax/edit', 'DeliverycertificateController@ajaxEdit');
Route::post('/delivery-certificates/ajax/delete', 'DeliverycertificateController@ajaxDelete');
Route::resource('/delivery-certificates', 'DeliverycertificateController');
Route::resource('/users', 'UserController');
Route::resource('/roles', 'RoleController');
Route::resource('/status', 'StatusController');
