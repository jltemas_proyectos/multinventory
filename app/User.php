<?php

namespace App;

use App\Role;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'users';

    protected $fillable = ['user', 'email', 'password',];

    protected $hidden = ['password', 'remember_token'];


    public function roles()
    {
        return $this->belongsToMany(Role::class)->withTimestamps();
        //belongsTO() va a buscar en el modelo User la foranea (rol_id)
        // return $this->belongsTo(Rol::class,'rol');
    }

    public function employee()
    {
        return $this->hasOne(Employee::class);
    }

    public function deliveries()
    {
        //hasMany() va a buscar en el modelo DeliveryCertificate la foranea (user_id)
        return $this->hasMany(DeliveryCertificate::class, 'user_id', 'id');
    }

    public function state()
    {
        return $this->belongsTo(Status::class, 'status');
    }
}
