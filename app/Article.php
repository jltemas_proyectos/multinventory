<?php

namespace App;

use App\Status;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Article extends Model
{

  use SoftDeletes;

  protected $dates = ['deleted_at'];

  protected $fillable = [
    'name',
    'brand',
    'model',
    'serial',
    'quantity',
    'price',
    'image',
    'description',
    'status',
    'category_id',
  ];

  public function fixed()
  {
    return $this->belongsTo(FixedAsset::class, 'fixedasset_code');
  }

  public function category()
  {
    return $this->belongsTo(Category::class);
  }

  // public function brand()
  // {
  //     return $this->belongsTo(Brand::class);
  // }

  public function state()
  {
    return $this->belongsTo(Status::class, 'status');
  }

  public function scopeDesktop($query)
  {
    return $query;
  }
}
