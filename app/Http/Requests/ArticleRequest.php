<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'name' => 'required|string',
      'brand' => 'required|string',
      'model' => 'required|string',
      'serial' => 'required|string',
      'quantity' => 'required|integer',
      'price' => 'required|integer',
      'image' => 'sometimes|required|image|file|unique:articles,image',
      'description' => 'required|string',
      'category_id' => 'required|integer|exists:categories,id',
    ];
  }
}
