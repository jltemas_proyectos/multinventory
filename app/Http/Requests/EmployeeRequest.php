<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // return true;
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string|max:15',
            'first_lastname' => 'required|string|max:15',
            'second_lastname' => 'required|string|max:15',
            'cedula' => 'required|string|max:10|min:6',
            // 'cedula' => 'required|string|max:10|min:6|unique:employees,cedula',
            'phone' => 'required|string|max:20|min:7',
            'department_id' => 'required|exists:departments,id',
            'position_id' => 'required|exists:positions,id',
            'image' => 'sometimes|required|image',
            'user_name' => 'required|string|min:3|max:15',
            // 'user_name' => 'required|string|min:3|max:15|unique:users,user_name',
            'email'=>'required|string|email'
            // 'email'=>'required|string|email|unique:users,email|unique:users,email'
        ];
    }
}
