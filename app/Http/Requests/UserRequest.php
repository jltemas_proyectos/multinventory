<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_name' => 'required|string|min:2|unique:users,user_name',
            'email' => 'required|string|email|unique:users,email',
            'password' => 'required|string|min:4',
            'confirm_password' => 'required|string|min:4|same:password',
            'first_name'=> 'required|string|min:5',
            'second_name'=> 'required|string|min:5',
            'first_lastname'=> 'required|string|min:2',
            'second_lastname'=> 'required|string|min:2',
            'cedula'=> 'required|string|min:6|unique:employees,cedula',
            'phone'=> 'required|string|min:7',
            'department_id'=> 'required|integer|min:1',
            'position_id'=> 'required|integer|min:1',
        ];
    }
}
