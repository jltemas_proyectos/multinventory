<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AssignFixedRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return auth()->check();
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'code'=>'',
      'storage_cellar_id'=>'',
      'articles'=>'',
      'brands'=>'',
      'models'=>'',
      'quantities'=>'',
      'serials'=>'',
      'fixeds'=>'',
      'prices'=>''
    ];
  }
}
