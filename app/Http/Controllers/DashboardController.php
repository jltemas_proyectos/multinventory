<?php

namespace App\Http\Controllers;

use App\{Article, Category};

class DashboardController extends Controller
{

  public function __construct()
  {
    $this->middleware('auth');
  }

  public function index()
  {
    $categories = Category::categories('name')->get();
    return view('dashboard', compact(['categories']));
  }

  public function desktop()
  {
    $count = 1;
    $desktops = Article::where('category_id',  6)->orderBy('name')->paginate(10);
    $countAricles = $desktops->count();

    return view('modules.dashboard.desktops.list-desktop', compact(['desktops', 'count']));
  }

  public function laptop()
  {
    $count = 1;
    $laptops = Article::where('category_id', 1)->orderBy('name')->paginate(10);
    return view('modules.dashboard.laptops.list-laptop', compact(['laptops', 'count']));
  }

  public function server()
  {
    $count = 1;
    $servers = Article::where('category_id', 1)->orderBy('name')->paginate(10);
    return view('modules.dashboard.servers.list-server', compact(['servers', 'count']));
  }

  public function peripheral()
  {
    $count = 1;
    $peripherals = Article::where('category_id', 4)->orderBy('name')->paginate(10);
    return view('modules.dashboard.peripherals.list-peripheral', compact(['peripherals', 'count']));
  }

  public function printer()
  {
    $count = 1;
    $printers = Article::where('category_id',  2)->orderBy('name')->paginate(10);
    return view('modules.dashboard.printers.list-printer', compact(['printers', 'count']));
  }

  public function network()
  {
    $count = 1;
    $networks = Article::where('category_id',  3)->orderBy('name')->paginate(10);
    return view('modules.dashboard.networks.list-network', compact(['networks', 'count']));
  }
}
