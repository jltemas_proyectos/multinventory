<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\EmployeeRequest;
use Illuminate\Support\Facades\Storage;
use App\{Employee, Department, Position, User};
use PHPUnit\Framework\MockObject\Stub\ReturnStub;

class EmployeeController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $employees = Employee::all();
    $count = 1;
    return view('modules.employees.index', compact(['employees', 'count']));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $departments = Department::all();
    $positions = Position::all();
    return view('modules.employees.create-employee', compact(['positions', 'departments']));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(EmployeeRequest $request)
  {
    if ($request->ajax()) {
      return 'Hola';
    }

    User::create($request->only(['user_name', 'email']));
    $user_id = User::all()->last()->id;
    $url_image = $request->file('image') ? $request->file('image')->store('images/employees') : 'images/users/user.png';
    // $url_image = $request->file('image')->store('images/employees');

    $dataE = [
      'image' => $url_image,
      'first_name' => $request->first_name,
      'second_name' => $request->second_name,
      'first_lastname' => $request->first_lastname,
      'second_lastname' => $request->second_lastname,
      'cedula' => $request->cedula,
      'phone' => $request->phone,
      'department_id' => $request->department_id,
      'position_id' => $request->position_id,
      'user_id' => $user_id
    ];

    Employee::create($dataE);
    return back()->with('status', 'Registro éxito!');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $employee = Employee::findOrFail($id);
    $departments = Department::all();
    $positions = Position::all();
    // return view('modules.employees.show', compact(['employee', 'departments', 'positions']));
    return view('modules.employees.show-employee', compact(['employee', 'departments', 'positions']));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $departments = Department::all();
    $positions = Position::all();
    $employee = Employee::findOrFail($id);
    return view('modules.employees.edit-employee', compact(['departments', 'positions', 'employee']));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(EmployeeRequest $request, $id)
  {

    // return $request->all();
    // return $request->image ? 'algo' : 'Vacio';
    // $image = $request->file('image')->store('images/employees') ?? 'images/users/user.png';
    // return $url_image;
    return  $url_image = $request->file('image') ? $request->file('image')->store('images/employees') : 'images/users/user.png';
    $user = User::findOrFail($id);

    $dataE = [
      'first_name' => $request->first_name,
      'second_name' => $request->second_name,
      'first_lastname' => $request->first_lastname,
      'second_lastname' => $request->second_lastname,
      'cedula' => $request->cedula,
      'phone' => $request->phone,
      'image' => $url_image,
      'department_id' => $request->department_id,
      'position_id' => $request->position_id,
      'user_id' => $user->id
    ];

    Employee::findOrFail($id)->update($dataE);
    return back()->with('status', 'Registro actualizadio con éxito!');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    // // dd(Auth::id(), (int)$id);
    // if (Auth::id() === (int)$id) {
    //   return  back()->with('status', 'No se puede eliminar a un Usuario Logeado!');
    // }

    // dd(gettype($id));
    $user_id = Employee::findOrFail($id)->user->id;
    Employee::findOrFail($id)->delete();
    User::findOrFail($user_id)->delete();
    return back();
  }


  public function ajax(Request $request)
  {
    if ($request->ajax() && $request->is('employees/ajax')) {
      return Employee::where('id', $request->employee)->select('cedula')->get();
    }
  }
}
