<?php

namespace App\Http\Controllers;

use App\{Employee, Department, FixedAsset, DeliveryCertificate, Article};
use App\Http\Controllers\TraitMultinventory\CodeResolution;
use Illuminate\Http\Request;
// use App\Http\Controllers\Trait\Codes;
// use Illuminate\Support\Facades\DB;
// use phpDocumentor\Reflection\DocBlock\Tags\Return_;

class DeliverycertificateController extends Controller
{

  use CodeResolution;
  public function __construct()
  {
    $this->middleware('auth');
    // $this->code = $this->code(DeliveryCertificate::all()->last()->code + 1);
    // $this->codes = $this->codes(DeliveryCertificate::where('id', '<>', 1)->get());
  }


  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    // return 'Hola';
    // $codes = $this->codes(DeliveryCertificate::all()->last()->code + 1);
    dd($codes = $this->codes);
    $deliveriesC = DeliveryCertificate::where('id', '<>', 1)->orderBy('code')->paginate('10');
    $count = 1;
    return view('modules.deliveries_certificates.list-delivery-certificate', compact(['deliveriesC', 'count', 'codes']));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $deliveryC = DeliveryCertificate::all()->last();
    $departments = Department::all();
     $fixedassets = $this->codes(FixedAsset::all()->where('status', '=', 2));
     $codefixedassets = FixedAsset::all()->where('status', '=', 2);
     $code = $this->code;
    return view('modules.deliveries_certificates.create-delivery-certificate', compact(['deliveryC', 'departments', 'fixedassets', 'codefixedassets', 'code']));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    if ($request->ajax()) {
      return FixedAsset::findOrFail($request->fixedasset)->articles;
    }

    $data = [
      'code' => $request->code,
      'date' => $request->date,
      'employee_id' => $request->employee_id,
      'department' => Department::findOrFail($request->department_id)->name,
      'user_id' => $request->deliveredBy,
      'observation' => $request->observation,
      'status' => 1
    ];

    DeliveryCertificate::create($data);

    $deliveryCertificate = DeliveryCertificate::where('code', $request->code)->firstOrFail();
    $employee = Employee::where('id', $request->employee_id)->firstOrFail();

    foreach ($request->fixedassets as $fixedasset) {
      FixedAsset::findOrFail($fixedasset)->update(['status' => 1]);
      $deliveryCertificate->fixedassets()->attach($fixedasset);
      $employee->fixedassets()->attach($fixedasset);
    }
    return back()->with('status', 'Registro exitoso!');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $delivery = DeliveryCertificate::findOrFail($id);
    $code = $this->codeIn($delivery);
    $total = 0;
    return view('modules.deliveries_certificates.show-delivery-certificate', compact(['delivery', 'total', 'code']));
  }

  /**r
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $code = $this->codeIn($id);
    $fixedAssets = FixedAsset::where('status', 2)->get();
    $deliveryC = DeliveryCertificate::findOrFail($id);
    $deliveryFixedLength = $deliveryC->fixedassets()->count();
    $total = 0;
    return view('modules.deliveries_certificates.edit-delivery-certificate', compact(['deliveryC', 'total', 'fixedAssets', 'deliveryFixedLength', 'code']));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $deliveryC = DeliveryCertificate::findOrFail($id);
    $fixedAssetsDelivery = $deliveryC->fixedassets->count();
    $fixedAssetsLength = (int) $request->fixedassets;
    return $request->all();

    if ($fixedAssetsDelivery === $fixedAssetsLength) {
      foreach ($request->articles as $article) {
        # code...
      }
      return 'iguales';
    } else {
      return 'No son iguales';
    }
    return back()->with('status', 'Registro Actualizado con éxito!');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    DeliveryCertificate::findOrFail($id)->delete();
    return back();
  }

  /**
   * [ajaxEdit Verifica que se esta haciendo una peticion Ajax y que esta viene desde edit-delivery-certificate]
   *
   * @param   Request  $request  [$request contiene toda la petición Ajax]
   *
   * @return  [Object] $fixedasset->articles [return Instancia del Modela Article]
   */
  public function ajaxEdit(Request $request)
  {
    if ($request->ajax()) {
      $fixedasset = FixedAsset::where('status', 2)->where('code', $request->fixedasset)->firstOrFail();
      return $fixedasset->articles;
    }
  }

  /**
   * [ajaxDelete Verifica que se esta haciendo una peticion Ajax y que esta viene desde edit-delivery-certificate]
   *
   * @param   Request  $request  [$request contiene toda la petición Ajax]
   *
   */
  public function ajaxDelete(Request $request)
  {
    if ($request->ajax()) {

      /**
       * [$response Respuesta de confirmacion de la eliminacion de un articulo]
       *
       * @var [String]
       */
      $response = 'Registro Actualizado';

      /**
       * [$countArticles Contiene la cantidad de articulos de un Activo Fijo con estado "Activo"]
       *
       * @var [Integer]
       */
      $countArticles = FixedAsset::find($request->fixedasset)->articles->where('status', 1)->count();

      if ($countArticles) {
        // return $request->all();
        if (($request->reason === 'damagedAjax') || ($request->reason === 'btn-danger')) {
          Article::findOrFail($request->article)->update(['status' => 11]);
          return gettype($countArticles);
          if ($countArticles === 1) {
            // return $request->fixedasset;
            DeliveryCertificate::findOrFail($request->deliveryC)->fixedassets()->detach($request->fixedasset);
            return 'Activo eliminado';
          }
          return $response;
        } elseif (($request->reason === 'failureAjax') || ($request->reason === 'btn-warning')) {
          Article::findOrFail($request->article)->update(['status' => 10]);
          return $response;
        } elseif (($request->reason === 'disconfortAjax') || ($request->reason === 'btn-primary')) {
          Article::findOrFail($request->article)->update(['status' => 12]);
          return $response;
        }
      } else {
        return 'Es 0';
        // DeliveryCertificate::
      }
    }
  }
}
