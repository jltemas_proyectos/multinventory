<?php

namespace App\Http\Controllers;

use App\{Employee, Department, FixedAsset, DeliveryCertificate};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DeliverycertificateController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $deliveriesC = DeliveryCertificate::where('id', '<>', 1)->get();
    $count = 1;
    return view('modules.deliveries_certificates.list-delivery-certificate', compact(['deliveriesC', 'count']));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $deliveryC = DeliveryCertificate::all()->last();
    $departments = Department::all();
    $fixedassets = FixedAsset::all()->where('status', '=', 2);
    return view('modules.deliveries_certificates.create-delivery-certificate', compact(['deliveryC', 'departments', 'fixedassets']));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {

    if ($request->ajax()) {
      return FixedAsset::findOrFail($request->fixedasset)->articles;
    }

    $data = [
      'code' => $request->code,
      'date' => $request->date,
      'employee_id' => $request->employee_id,
      'department' => Department::findOrFail($request->department_id)->name,
      'user_id' => $request->deliveredBy,
      'observation' => $request->observation,
      'status' => 1
    ];

    DeliveryCertificate::create($data);

    $deliveryCertificate = DeliveryCertificate::where('code', $request->code)->firstOrFail();
    $employee = Employee::where('id', $request->employee_id)->firstOrFail();

    foreach ($request->fixedassets as $fixedasset) {
      FixedAsset::findOrFail($fixedasset)->update(['status' => 1]);
      $deliveryCertificate->fixedassets()->attach($fixedasset);
      $employee->fixedassets()->attach($fixedasset);
    }
    return back()->with('status', 'Registro exitoso!');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $delivery = DeliveryCertificate::findOrFail($id);
    $total = 0;
    return view('modules.deliveries_certificates.show-delivery-certificate', compact(['delivery', 'total']));
  }

  /**r
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {

    $fixedAssets = FixedAsset::where('status', 2)->get();
    $deliveryC = DeliveryCertificate::findOrFail($id);
    $total = 0;
    return view('modules.deliveries_certificates.edit-delivery-certificate-copy-funtional', compact(['deliveryC', 'total', 'fixedAssets']));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    return $request->all();
    return $deliveryC = DeliveryCertificate::findOrFail($id);
    return $fixedAssets = $deliveryC->fixedassets;
    return $request->all();
    return back()->with('status', 'Registro Actualizado con éxito!');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    DeliveryCertificate::findOrFail($id)->delete();
    return back();
  }

  public function ajaxEdit(Request $request)
  {
    if ($request->ajax()) {
      $fixedasset = FixedAsset::where('status', 2)->where('code', $request->fixedasset)->firstOrFail();
      return $fixedasset->articles;
    }
  }
}
