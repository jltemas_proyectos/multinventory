<?php

namespace App\Http\Controllers;

use App\{Brand, Article, FixedAsset, StorageCellar, DeliveryCertificate, FixedAssetArticle};
use App\Http\Controllers\TraitMultinventory\CodeResolution;
use App\Http\Requests\FixedAssetRequest;
use Illuminate\Auth\Access\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\{DB, Auth};
use PHPUnit\Framework\MockObject\Stub\ReturnStub;

class FixedassetController extends Controller
{

  use CodeResolution;

  public function __construct()
  {
    $this->middleware('auth');
    // $this->codeLast = $this->code(FixedAsset::all()->last()->code + 1);
    // $this->codes = $this->codes(FixedAsset::where('id', '<>', 1)->get());
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {

    if (request()->ajax()) {
      return $article = $articles = Article::where('status', 2)->where('fixedasset_code', 1)->get();
    }
    $codes = $this->codes;
    $fixedassets = FixedAsset::where('id', '<>', 1)->orderBy('code')->paginate(10);
    return view('modules.fixed_assets.list-fixed-asset', compact(['fixedassets', 'codes']));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $code =  $this->codeLast;
    $fixedasset = FixedAsset::all()->last()->code + 1;
    $storage_cellars = StorageCellar::all();
    return view('modules.fixed_assets.create-fixed-asset', compact(['storage_cellars', 'fixedasset', 'code']));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    if ($request->ajax()) {
      $article = Article::where('id', '=', $request->article_id)->first();
      return $article;
    }

    $request->validate([
      'code' => 'required|integer|min:1|unique:fixed_assets,code',
      'storage_cellar_id' => 'required|integer|exists:storage_cellars,id',
    ]);

    FixedAsset::create($request->all());
    return back()->with('status', 'Registro exitoso!');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show(Request $request, $id)
  {
    $fixedasset = FixedAsset::findOrFail($id);
    $code =  $this->codeIn($fixedasset);
    $articles = $fixedasset->articles;
    $total = 0;
    return view('modules.fixed_assets.show-fixed-asset', compact(['fixedasset', 'articles', 'total', 'code']));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $code = $this->codeIn(FixedAsset::findOrFail($id));
    $fixedasset = FixedAsset::findOrFail($id);
    $storagecs = StorageCellar::all();
    return view('modules.fixed_assets.edit-fixed-asset', compact(['fixedasset', 'storagecs', 'code']));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(FixedAssetRequest $request, $id)
  {
    FixedAsset::findOrFail($id)->update($request->all());
    return back()->with('status', 'Registro actualizado con éxito!');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    FixedAsset::findOrFail($id)->delete();
    return back();
  }

  /**
   * showFormAssignFixedAsset
   * 
   *  @return \Illuminate\Http\Response
   */
  public function showFormAssignFixedAsset()
  {
    $storagecs = StorageCellar::all();
    $articles = Article::where('status', 2)->where('fixedasset_code', 1)->get();
    $brands = Brand::all();
    $fixedassets = FixedAsset::where('status', 3)->where('id', '<>', 1)->get();
    $codesFasset = $this->codes(FixedAsset::where('status', 3)->where('id', '<>', 1)->get());
    return view('modules.fixed_assets.assign-fixed-asset', compact(['fixedassets', 'brands', 'storagecs', 'articles', 'codesFasset']));
  }

  public function showFormEditAssignFixedAsset(Request $request, $id)
  {
    // return $request->all();
    $fixedasset = FixedAsset::findOrFail($id);
    $code = $this->codeIn($fixedasset);
    $articles = Article::all()->where('status', 2);
    $brands = Brand::all();
    $storagecs = StorageCellar::all();
    $total = 0;
    return view('modules.fixed_assets.edit-assign-fixed-asset', compact(['fixedasset', 'brands', 'storagecs', 'articles', 'total', 'code']));
  }

  public function assignFixedAsset(Request $request)
  {
    // return $request->all();
    $fixedasset = FixedAsset::where('code', $request->code)->first();
    $fixedasset->update(['status' => 2]);
    $articles = count($request->articles);

    for ($i = 0; $i < $articles; $i++) {
      Article::where('id', $request->articles[$i])->update(['status' => 1, 'fixedasset_code' => $fixedasset->id]);
    }
    return back()->with('status', "Articulos asignados al Actio Fijo {$request->code} con Exíto!");
  }

  /**
   * [updateAssignFixedAsset Actualiza los datos de los articulos asignados a un Fixed Asset]
   *
   * @param   Request  $request  [$request datos de la petición ]
   * @param   [type]   $id       [$id identificador del Fixed Asset]
   *
   * @return  [view]             [return vista de edición de los asignacion de Fixed Asset]
   */
  public function updateAssignFixedAsset(Request $request, $id)
  {
    // return $request->all();

    $request->validate([
      'code' => 'filled|required|integer|exists:fixed_assets,code',
      'storage_cellar_id' => 'filled|required|integer|exists:fixed_assets,storage_cellar_id',
    ]);

    $fixedasset = FixedAsset::where('code', $id)->firstOrFail();
    $fixedAssetArticles = $fixedasset->articles->count();
    $articlesLength = (int) !empty($request->articles) ? count($request->articles) : 0;

    if ($fixedasset->storage_cellar_id != (int) $request->storage_cellar_id) {
      $fixedasset->update(['storage_cellar_id' => $request->storage_cellar_id]);
      return back()->with('status', 'Activo actualizado con éxito, se cambió de bodega el Activo Fijo');
    } elseif ($articlesLength === $fixedAssetArticles) {
      return back()->with('status', "No haz hecho modificaciones en el Activo Fijo");
    } elseif ($articlesLength === 0) {
      $fixedasset->update(['status' => 3]);
      foreach ($fixedasset->articles as $article) {
        $article->update(['status' => 2, 'fixedasset_code' => 1]);
      }
      return back()->with('status', 'Activo actualizado con éxito, se eliminaron todos los artuculos del Activo Fijo!');
    } elseif (($articlesLength != 0 && $articlesLength < $fixedAssetArticles) || ($articlesLength > $fixedAssetArticles)) {
      foreach ($fixedasset->articles as $articleOld) {
        Article::where('id', $articleOld->id)->update(['status' => 2, 'fixedasset_code' => 1]);
      }
      foreach ($request->articles as $articleNew) {
        Article::where('id', $articleNew)->update(['status' => 1, 'fixedasset_code' => $fixedasset->id]);
      }
      return back()->with('status', 'Activo actualizado con éxito!');
    }
  }
}
