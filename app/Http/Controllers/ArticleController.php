<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\{Auth,Storage};
use App\Http\Requests\ArticleRequest;
use App\{Article, Brand, Category, FixedAsset};

class ArticleController extends Controller
{

  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $count=1;
    $articles = Article::orderBy('name')->paginate(10);
    return view('modules.articles.list-articles', compact(['articles','count']));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $brands = Brand::all();
    $categories = Category::all();
    $fixedassets = FixedAsset::where('code', '<>', 0)->get();
    return view('modules.articles.create-article', compact(['categories', 'fixedassets', 'brands']));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(ArticleRequest $request)
  {
    $url_image = $request->file('image')->store('images/articles');
    $data = [
      'name' => $request->name,
      'brand' => $request->brand,
      'model' => $request->model,
      'serial' => $request->serial,
      'quantity' => $request->quantity,
      'price' => $request->price,
      'image' => $url_image,
      'description' => $request->description,
      'category_id' => $request->category_id,
    ];

    Article::create($data);
    return back()->with('status', 'Registro exitoso!');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $article = Article::findOrFail($id);
    $fixedassets = FixedAsset::all();
    $categories = Category::all();
    $path = 'storage';

    return view('modules.articles.show-article', compact(['article', 'fixedassets', 'categories', 'path']));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $path = 'storage';
    $categories = Category::all();
    $fixedassets = FixedAsset::all();
    $article = Article::findOrFail($id);
    return view('modules.articles.edit-article', compact(['article', 'path', 'categories', 'fixedassets']));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(ArticleRequest $request, $id)
  {
    // return $request->all();
    $article = Article::findOrFail($id);
    $url_image = $request->file('image')->store('images/articles');

    $data = [
      'name' => $request->name,
      'brand' => $request->brand,
      'model' => $request->model,
      'serial' => $request->serial,
      'quantity' => $request->quantity,
      'price' => $request->price,
      'image' => $url_image,
      'description' => $request->description,
      'category_id' => $request->category_id,
    ];

    $article->update($data);
    
    return back()->with('status', 'Registro Actualizado con éxito!');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    Article::findOrFail($id)->delete();
    return back();
  }
}
