<?php

namespace App\Http\Controllers\TraitMultinventory;

trait CodeResolution
{
  /**
   * [code Es el codigo de la nueva de Acta de entrega o Activo Fijo]
   *
   * @param   [Int]  $lastCode  [$lastCode Indica el Ultimo codigo de Acta de entrega o de activo fijo]
   *
   * @return  [Int]  $code      [return Indica el código del nuevo número consecutivo de Acta o Activo fijo]
   */
  public function code($lastCode)
  {
    switch (strlen($lastCode)) {
      case '1':
        return "000$lastCode";
        break;
      case '2':
        return "00$lastCode";
        break;
      case '3':
        return "0$lastCode";
        break;
      case '4':
        return $lastCode;
        break;
    }
  }

  /**
   * [codeIn Indica el codigo de un acta seleccionada]
   *
   * @param   [Int]  $id  [Es el id del acta de entrega a visualizar]
   *
   * @return  [Int]   [return Codigo del acta a visualizar]
   */
  public function codeIn($id)
  {
    $codeC = $id;
    $dc = strlen($codeC->code);
    switch ($dc) {
      case '1':
        return $code = "000$codeC->code";
        break;
      case '2':
        return $code = "00$codeC->code";
        break;
      case '3':
        return $code = "0$codeC->code";
        break;
      case '4':
        return $code = $codeC->code;
        break;
    }
  }
  /**
   * [codes Son los codigos de todas las actas de entrega]
   *
   * @param   [Array]  $arr  [$arr codigos existentes]
   *
   * @return  [Array]        [return Lista de codigos]
   */
  public function codes($arr)
  {

    $codesC = $arr;
    $codes = [];
    foreach ($codesC as $code) {
      $dc = strlen($code->code);
      switch ($dc) {
        case '1':
          array_push($codes, "000$code->code");
          break;
        case '2':
          array_push($codes, "00$code->code");
          break;
        case '3':
          array_push($codes, "0$code->code");
          break;
        case '4':
          array_push($codes, $code->code);
          break;
      }
    }
    return $codes;
  }
}
