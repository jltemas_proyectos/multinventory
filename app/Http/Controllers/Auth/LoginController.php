<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
  protected $redirectTo = '/login';

  public function __construct()
  {
    return $this->middleware('guest', ['only' => 'showLoginForm']);
  }

  public function showLoginForm()
  {
    return view('auth.login');
  }

  public function login()
  {
    $credentials = $this->validate(request(), [
      $this->username() => 'string|required',
      'password' => 'string|required',
    ]);

    if (Auth::attempt($credentials)) {
      return redirect()->route('dashboard');
    }

    return back()
      ->withErrors([
        $this->username() => trans('auth.failed'),
        'password' => trans('auth.password')
      ])
      ->withInput(request([$this->username()]));
  }

  public function logout()
  {
    Auth::logout();
    return redirect()->route('login');
  }

  public function username()
  {
    return  'user';
  }
}
