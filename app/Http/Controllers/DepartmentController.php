<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Department;
use App\Http\Requests\DepartmentRequest;

class DepartmentController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $departments = Department::orderBy('id')->paginate(10);
    return view('modules.departments.list-department', compact(['departments']));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('modules.departments.create-department');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\DepartmentRequest  $request
   * @return \Illuminate\Http\Response
   */
  public function store(DepartmentRequest $request)
  {
    if ($request->ajax()) {
      return $employees = Department::findOrFail($request->department)->employees;
    }

    Department::create($request->all());
    return back()->with('status', 'Registro exitoso!');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    return redirect()->route('departments.index');
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $department = Department::findOrFail($id);
    return view('modules.departments.edit-department', compact('department'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(DepartmentRequest $request, $id)
  {
    Department::findOrFail($id)->update($request->all());
    return back()->with('status', 'Registro Actualizado con éxito!');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    Department::findOrFail($id)->delete();
    return back();
  }
}
