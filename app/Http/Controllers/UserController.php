<?php

namespace App\Http\Controllers;

use App\{User, Employee, Position, Department};
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

  public function __construct()
  {
    $this->middleware('auth');
    $this->nameModule = strtoupper('user module');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    if (Auth::check()) {
      $module = $this->nameModule;
      // dd($module);
      $users = User::all();
      $count = 1;
      return view('modules.users.index', compact(['count', 'users','module']));
    }
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('modules.users.create-user');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(UserRequest $request)
  {
    $data = [
      'user_name' => $request->user_name,
      'email' => $request->email,
      'password' => bcrypt($request->password)
    ];
    User::create($data);
    return back()->with('status', 'Registro exitoso!');
  }

  /** 
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $positions = Position::all();
    $departments = Department::all();
    $user = User::findOrFail($id);
    return view('modules.users.show-user', compact(['user', 'departments', 'positions']));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $user = User::findOrFail($id);
    return view('modules.users.edit-user', compact('user'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(UserRequest $request, $id)
  {

    $long = count($request->all());

    $data_u = [
      'user_name' => $request->user_name,
      'email' => $request->email,
      'password' => bcrypt($request->password),
    ];

    $data_e = [
      'first_name' => $request->first_name,
      'second_name' => $request->second_name,
      'first_lastname' => $request->first_lastname,
      'second_lastname' => $request->second_lastname,
      'cedula' => $request->cedula,
      'phone' => $request->phone,
      'department_id' => $request->department,
      'position_id' => $request->position,
    ];

    if ($long == 6) {
      User::findOrFail($id)->update($data_u);
      return back()->with('status', 'Registro actualizadio con éxito!');
    } else {
      return $request->all();
      User::findOrFail($id)->update($data_u);
      Employee::first()->where('user_id', '=', $id)->update($data_e);
      return back();
      // return back()->with('status', 'Registro actualizadio con éxito!');
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    User::findOrFail($id)->delete();
    return back();
  }
}
