<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\CategoryRequest;
use App\{Article,Category};

class CategoryController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $categories = Category::all();
    $count = 1;
    return view('modules.categories.list-category', compact(['categories', 'count']));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('modules.categories.create-category');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    if ($request->ajax()) {
      return Article::select('id', 'name', 'serial')->where('status', 2)->where('fixedasset_code', 1)->get();
    }

    $request->validate([
      'image' => 'required|image|sometimes',
      'name' => 'required|string|unique:categories,name',
      'description' => 'required|string|min:10|max:200',
    ]);

    return $request->all();
    $url_image = $request->file('image')->store('images/home');
    $data = [
      'name' => $request->name,
      'description' => $request->description,
      'image' => $url_image,
    ];
    Category::create($data);
    return back()->with('status', 'Registro exitoso!');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    return redirect()->route('categories.index');
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $category = Category::findOrFail($id);
    return view('modules.categories.edit-category', compact('category'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(CategoryRequest $request, $id)
  {
    return $request->all();
    $url_image = $request->file('image')->store('images/home') ;
    $data = [
      'name' => $request->name,
      'description' => $request->description,
      'image' => $url_image,
    ];
    Category::findOrFail($id)->update($data);
    return back()->with('status', 'Registro Actualizado con éxito!');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    Category::findOrFail($id)->delete();
    return back();
  }
}
