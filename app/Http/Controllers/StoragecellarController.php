<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StorageCellar;
use App\Http\Requests\StorageCellarRequest;

class StoragecellarController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $storage_cellars = StorageCellar::all();
        $count = 1;
        return view('modules.storage_cellar.list-storage-cellar', compact(['storage_cellars', 'count']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modules.storage_cellar.create-storage-cellar');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorageCellarRequest $request)
    {
        StorageCellar::create($request->all());
        return back()->with('status', 'Registro exitoso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $storage_cellar = StorageCellar::findOrFail($id);
        return view('modules.storage_cellar.edit-storage-cellar', compact('storage_cellar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StorageCellarRequest $request, $id)
    {
        StorageCellar::findOrFail($id)->update($request->all());
        return back()->with('status', 'Registro Actualizado con éxito!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        StorageCellar::findOrFail($id)->delete();
        return back();
    }
}
