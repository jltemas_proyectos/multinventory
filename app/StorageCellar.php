<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StorageCellar extends Model
{

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    
    protected $table = 'storage_cellars';

    protected $fillable = ['name', 'description','status'];

    public function fixedAssets()
    {
       return $this->hasMany(FixedAsset::class);
    }
}
