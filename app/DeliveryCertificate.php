<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeliveryCertificate extends Model
{

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'delivery_certificates';

    protected $fillable = ['code', 'date', 'employee_id', 'department', 'user_id', 'status', 'observation'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function fixedassets()
    {
        return $this->belongsToMany(FixedAsset::class, 'delivery_certificate_fixed_asset')
            ->as('details')
            ->withPivot('deleted_at')
            ->withTimestamps();
    }

    public function state()
    {
        return $this->belongsTo(Status::class, 'status');
    }
}
