<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{

  use SoftDeletes;

  protected $table = 'roles';

  protected $dates = ['deleted_at'];

  protected $fillable = ['name', 'description', 'status'];
  
  public function users()
  {
    return $this->belongsToMany(User::class)->withTimestamps();
  }

  public function state()
  {
    return $this->belongsTo(Status::class, 'status');
  }

}
