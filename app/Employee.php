<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'employees';

    protected $fillable = ['first_name', 'second_name', 'first_lastname', 'second_lastname', 'cedula', 'phone', 'department_id', 'user_id', 'position_id', 'status', 'image'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function position()
    {
        return $this->belongsTo(Position::class);
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function deliveries()
    {
        return $this->hasMany(DeliveryCertificate::class);
    }

    public function fixedassets()
    {
        return $this->belongsToMany(FixedAsset::class, 'employee_fixed_asset')
            ->as('details')
            ->withPivot('deleted_at')
            ->withTimestamps();
    }

    public function state()
    {
        return $this->belongsTo(Status::class, 'status');
    }
}
