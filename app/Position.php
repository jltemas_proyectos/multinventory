<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Position extends Model
{

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'positions';

    protected $fillable = ['name', 'description', 'status'];

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function employees()
    {
        return $this->hasMany(Employee::class);
    }
}
