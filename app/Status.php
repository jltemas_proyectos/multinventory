<?php

namespace App;

use App\Article;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Status extends Model
{

    use SoftDeletes;
    
    protected $dates = ['deleted_at'];
    
    protected $table="status";
    
    protected $fillable = ['name','description'];

    public function articles()
    {
        return $this->hasMany(Article::class,'status');
    }

    public function roles()
    {
        return $this->hasMany(Rol::class, 'status');
    }

    public function departments()
    {
        return $this->hasMany(Department::class, 'status');
    }

    public function deliveries()
    {
        return $this->hasMany(DeliveryCertificate::class, 'status');
    }

    public function employees()
    {
        return $this->hasMany(Employee::class, 'status');
    }

    public function fixeds()
    {
        return $this->hasMany(FixedAsset::class, 'status');
    }

    public function users()
    {
        return $this->hasMany(User::class, 'status');
    }

}
