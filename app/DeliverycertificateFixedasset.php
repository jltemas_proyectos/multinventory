<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeliverycertificateFixedasset extends Pivot
{
    use SoftDeletes;

    protected $table = 'delivery_certificate_fixed_asset';
    
    protected $dates = ['deleted_at'];

    // protected $fillable = ['quantity','observation','deleted_at'];
}
