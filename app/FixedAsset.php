<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FixedAsset extends Model
{

    protected $table = 'fixed_assets';

    protected $fillable = ['code', 'storage_cellar_id', 'status'];

    public function delivery()
    {
        return $this->belongsToMany(DeliveryCertificate::class, 'delivery_certificate_fixed_asset');
    }

    public function articles()
    {
        return $this->hasMany(Article::class, 'fixedasset_code');
    }

    public function storageCellar()
    {
        return $this->belongsTo(StorageCellar::class);
    }

    public function employees()
    {
        return $this->belongsToMany(Employee::class, 'employee_fixed_asset')
            ->as('details')
            ->withPivot('deleted_at')
            ->withTimestamps();
    }

    public function deliveries()
    {
        return $this->belongsToMany(DeliveryCertificate::class);
    }

    public function state()
    {
        return $this->belongsTo(Status::class, 'status');
    }
}
