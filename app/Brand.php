<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Brand extends Model
{

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'brands';

    protected $fillable = ['name', 'description', 'status'];

    public function articles()
    {
        return $this->hasMany(Article::class);
    }
}
