<?php

namespace App;

use App\{Article,FixedAsset};
use Illuminate\Database\Eloquent\Model;

class FixedAssetArticle extends Model
{
    protected $table = 'fixed_asset_article';

    protected $fillable = ['fixed_asset_id', 'article_id', 'quantity','created_by'];

}
