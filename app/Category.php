<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{

    use SoftDeletes;

    protected $table = 'categories';

    protected $dates = ['deleted_at'];

    protected $fillable = ['name', 'description', 'image', 'status'];

    public function scopeCategories($query)
    {
        return $query->select('name', 'description', 'image');
    }

    public function articles()
    {
        return $this->hasMany(Article::class);
    }
}
