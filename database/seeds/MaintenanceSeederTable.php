<?php

use App\Maintenance;
use Illuminate\Database\Seeder;

class MaintenanceSeederTable extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $maintance = new Maintenance();
    $maintance->type = 'Preventivo';
    $maintance->code = 1144859213;
    $maintance->date = date("Y/m/d");
    $maintance->observations = 'todo esta bien';
    $maintance->save();

    $maintance = new Maintenance();
    $maintance->type = 'Correctivo';
    $maintance->code = 1144859214;
    $maintance->date = date("Y/m/d");
    $maintance->observations = 'todo esta bien';
    $maintance->save();

    $maintance = new Maintenance();
    $maintance->type = 'Preventivo';
    $maintance->code = 1144859215;
    $maintance->date = date("Y/m/d");
    $maintance->observations = 'todo esta bien';
    $maintance->save();

    $maintance = new Maintenance();
    $maintance->type = 'Correctivo';
    $maintance->code = 1144859216;
    $maintance->date = date("Y/m/d");
    $maintance->observations = 'todo esta bien';
    $maintance->save();
  }
}
