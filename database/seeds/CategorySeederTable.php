<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategorySeederTable extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    factory(Category::class)->create();

    // $category = new Category();
    // $category->name = 'Laptop';
    // $category->description = 'Equipos de computo portatiles';
    // $category->image = 'images/home/laptop.png';
    // $category->save();

    // $category = new Category();
    // $category->name = 'Impresoras';
    // $category->description = 'Equipos de computa para el hot';
    // $category->image = 'images/home/prints.png';
    // $category->save();

    // $category = new Category();
    // $category->name = 'Redes';
    // $category->description = 'Equipos de computa para el hot';
    // $category->image = 'images/home/network.png';
    // $category->save();

    // $category = new Category();
    // $category->name = 'Perifericos';
    // $category->description = 'Equipos de computa para el hot';
    // $category->image = 'images/home/perifericos.png';
    // $category->save();

    // $category = new Category();
    // $category->name = 'Servidor';
    // $category->description = 'Equipos de computa para el hot';
    // $category->image = 'images/home/servers.png';
    // $category->save();

    // $category = new Category();
    // $category->name = 'Desktop';
    // $category->description = 'Equipos de escritorio de la compañia multipartes';
    // $category->image = 'images/home/pc_desktop.png';
    // $category->save();
  }
}
