<?php

use App\Brand;
use Illuminate\Database\Seeder;

class BrandSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Brand::class, 5)->create();
    }
}
