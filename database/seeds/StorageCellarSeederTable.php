<?php

use App\StorageCellar;
use Illuminate\Database\Seeder;

class StorageCellarSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $storage_cellar = new StorageCellar;
        $storage_cellar->name = 'Departamento De Sistemas';
        $storage_cellar->description = 'Aquí se encuentran los activo de backup para prestamos';
        $storage_cellar->save();

        $storage_cellar = new StorageCellar;
        $storage_cellar->name = 'Bodega De Sistemas';
        $storage_cellar->description = 'Aquí se encuentran los activo que están obsoletos, fuera de servicio y dañados';
        $storage_cellar->save();
    }
}
