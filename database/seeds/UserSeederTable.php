<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeederTable extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    factory(User::class)->create([
      'user' => 'jhonry',
      'email' => 'info@gmail.com',
      'password' => bcrypt(12345)
    ]);

    factory(User::class, 4)->create();
  }
}
