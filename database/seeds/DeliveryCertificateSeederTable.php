<?php

use App\DeliveryCertificate;
use Illuminate\Database\Seeder;

class DeliveryCertificateSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $delivery = new DeliveryCertificate();
        $delivery->code = 0;
        $delivery->date = '1988/08/25';
        $delivery->department = 'sistemas';
        $delivery->user_id = 1;
        $delivery->employee_id = 1;
        $delivery->status = '1';
        $delivery->save();
    }
}
