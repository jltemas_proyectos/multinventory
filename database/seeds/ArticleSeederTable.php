<?php

use App\Article;
use Illuminate\Database\Seeder;

class ArticleSeederTable extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $article = new Article();
    $article->name = 'Teclado';
    $article->model = 'ZFSMh25D';
    $article->serial = 'AAAAAAAA';
    $article->quantity = 1;
    $article->price = 25000;
    $article->category_id = 1;
    $article->brand = 'Dell';
    // $article->fixedasset_code = 1;
    $article->description = 'teclado inalambrico';
    $article->save();

    $article = new Article();
    $article->name = 'Teclado';
    $article->model = 'ZFSMh25D';
    $article->serial = 'BBBBBBBB';
    $article->quantity = 1;
    $article->price = 25000;
    $article->category_id = 1;
    $article->brand = 'Hp';
    // $article->fixedasset_code = 1;
    $article->description = 'teclado inalambrico';
    $article->save();

    $article = new Article();
    $article->name = 'Mouse';
    $article->model = 'XREM';
    $article->serial = 'CCCCCCCC';
    $article->quantity = 1;
    $article->price = 25000;
    $article->category_id = 4;
    $article->brand = 'Dell';
    // $article->fixedasset_code = 1;
    $article->description = 'Mouser inalambrico';
    $article->save();

    $article = new Article();
    $article->name = 'Mouse';
    $article->model = 'XREM';
    $article->serial = 'DDDDDDDD';
    $article->quantity = 1;
    $article->price = 25000;
    $article->category_id = 4;
    $article->brand = 'Hp';
    // $article->fixedasset_code = 1;
    $article->description = 'Mouser inalambrico';
    $article->save();

    $article = new Article();
    $article->name = 'Monitor';
    $article->model = 'Mh25D';
    $article->serial = 'EEEEEEEE';
    $article->quantity = 1;
    $article->price = 25000;
    $article->category_id = 4;
    $article->brand = 'Dell';
    // $article->fixedasset_code = 1;
    $article->description = 'monitor de 24"';
    $article->save();

    $article = new Article();
    $article->name = 'Monitor';
    $article->model = 'Mh25D';
    $article->serial = 'FFFFFFFF';
    $article->quantity = 1;
    $article->price = 25000;
    $article->category_id = 4;
    $article->brand = 'Hp';
    // $article->fixedasset_code = 1;
    $article->description = 'monitor de 24"';
    $article->save();

    $article = new Article();
    $article->name = 'Pc DEll';
    $article->model = 'ESM5';
    $article->serial = 'GGGGGGGG';
    $article->quantity = 1;
    $article->price = 1000000;
    $article->category_id = 6;
    $article->brand = 'Dell';
    // $article->fixedasset_code = 1;
    $article->description = 'monitor de 24"';
    $article->save();

    $article = new Article();
    $article->name = 'Pc Hp';
    $article->model = 'ESM5';
    $article->serial = 'HHHHHHHH';
    $article->quantity = 1;
    $article->price = 1000000;
    $article->category_id = 6;
    $article->brand = 'Hp';
    // $article->fixedasset_code = 1;
    $article->description = 'monitor de 24"';
    $article->save();

    $article = new Article();
    $article->name = 'PC Lenovo';
    $article->model = 'DY8';
    $article->serial = 'IIIIIIII';
    $article->quantity = 1;
    $article->price = 25000;
    $article->category_id = 6;
    $article->brand = 'Lenovo';
    // $article->fixedasset_code = 1;
    $article->description = 'teclado inalambrico';
    $article->save();

    $article = new Article();
    $article->name = 'PC Dell';
    $article->model = 'DY8';
    $article->serial = 'JJJJJJJJ';
    $article->quantity = 1;
    $article->price = 25000;
    $article->category_id = 6;
    $article->brand = 'Dell';
    // $article->fixedasset_code = 1;
    $article->description = 'teclado inalambrico';
    $article->save();

    $article = new Article();
    $article->name = 'PC Hp';
    $article->model = '5RM';
    $article->serial = 'KKKKKKKK';
    $article->quantity = 1;
    $article->price = 25000;
    $article->category_id = 6;
    $article->brand = 'Hp';
    // $article->fixedasset_code = 1;
    $article->description = 'teclado inalambrico';
    $article->save();

    $article = new Article();
    $article->name = 'PC Hp';
    $article->model = '5RM';
    $article->serial = 'LLLLLLLL';
    $article->quantity = 1;
    $article->price = 25000;
    $article->category_id = 6;
    $article->brand = 'Hp';
    // $article->fixedasset_code = 1;
    $article->description = 'teclado inalambrico';
    $article->save();

    $article = new Article();
    $article->name = 'Router';
    $article->model = '25D';
    $article->serial = 'MMMMMMMM';
    $article->quantity = 1;
    $article->price = 25000;
    $article->category_id = 3;
    $article->brand = 'Cisco';
    // $article->fixedasset_code = 1;
    $article->description = 'Mouser inalambrico';
    $article->save();

    $article = new Article();
    $article->name = 'Router';
    $article->model = '25D';
    $article->serial = 'NNNNNNNN';
    $article->quantity = 1;
    $article->price = 25000;
    $article->category_id = 3;
    $article->brand = 'Linksys';
    // $article->fixedasset_code = 1;
    $article->description = 'Mouser inalambrico';
    $article->save();

    $article = new Article();
    $article->name = 'Switch';
    $article->model = 'M25D';
    $article->serial = 'OOOOOOOO';
    $article->quantity = 1;
    $article->price = 25000;
    $article->category_id = 3;
    $article->brand = 'Cisco';
    // $article->fixedasset_code = 1;
    $article->description = 'monitor de 24"';
    $article->save();

    $article = new Article();
    $article->name = 'Switch';
    $article->model = 'M25D';
    $article->serial = 'PPPPPPPP';
    $article->quantity = 1;
    $article->price = 25000;
    $article->category_id = 3;
    $article->brand = 'Linksys';
    // $article->fixedasset_code = 1;
    $article->description = 'monitor de 24"';
    $article->save();

    $article = new Article();
    $article->name = 'Servidor Dell';
    $article->model = 'TOR';
    $article->serial = 'QQQQQQQQ';
    $article->quantity = 1;
    $article->price = 1000000;
    $article->category_id = 5;
    $article->brand = 'Dell';
    // $article->fixedasset_code = 1;
    $article->description = 'monitor de 24"';
    $article->save();

    $article = new Article();
    $article->name = 'Servidor Hp';
    $article->model = 'TOR';
    $article->serial = 'RRRRRRRR';
    $article->quantity = 1;
    $article->price = 1000000;
    $article->category_id = 5;
    $article->brand = 'Hp';
    // $article->fixedasset_code = 1;
    $article->description = 'monitor de 24"';
    $article->save();

    $article = new Article();
    $article->name = 'Servidor Lenovo';
    $article->model = 'SION4';
    $article->serial = 'SSSSSSSS';
    $article->quantity = 1;
    $article->price = 1000000;
    $article->category_id = 5;
    $article->brand = 'Lenovo';
    // $article->fixedasset_code = 1;
    $article->description = 'monitor de 24"';
    $article->save();

    $article = new Article();
    $article->name = 'Servidor Lenovo';
    $article->model = 'SION4';
    $article->serial = 'TTTTTTTT';
    $article->quantity = 1;
    $article->price = 1000000;
    $article->category_id = 5;
    $article->brand = 'Lenovo';
    // $article->fixedasset_code = 1;
    $article->description = 'monitor de 24"';
    $article->save();

    $article = new Article();
    $article->name = 'Portatil Compaq';
    $article->model = 'S43';
    $article->serial = 'UUUUUUUU';
    $article->quantity = 1;
    $article->price = 1000000;
    $article->category_id = 1;
    $article->brand = 'Compaq';
    // $article->fixedasset_code = 1;
    $article->description = 'monitor de 24"';
    $article->save();

    $article = new Article();
    $article->name = 'Portatil Dell';
    $article->model = 'S43';
    $article->serial = 'VVVVVVVV';
    $article->quantity = 1;
    $article->price = 1000000;
    $article->category_id = 1;
    $article->brand = 'DEll';
    // $article->fixedasset_code = 1;
    $article->description = 'monitor de 24"';
    $article->save();

    $article = new Article();
    $article->name = 'Portatil Sony';
    $article->model = 'M4R';
    $article->serial = 'WWWWWWWW';
    $article->quantity = 1;
    $article->price = 1000000;
    $article->category_id = 1;
    $article->brand = 'Sony';
    // $article->fixedasset_code = 1;
    $article->description = 'monitor de 24"';
    $article->save();

    $article = new Article();
    $article->name = 'Portatil Mac';
    $article->model = 'M4R';
    $article->serial = 'XXXXXXXX';
    $article->quantity = 1;
    $article->price = 1000000;
    $article->category_id = 1;
    $article->brand = 'Apple';
    // $article->fixedasset_code = 1;
    $article->description = 'monitor de 24"';
    $article->save();

    $article = new Article();
    $article->name = 'Impresora HP';
    $article->model = '45FR';
    $article->serial = 'YYYYYYYY';
    $article->quantity = 1;
    $article->price = 1000000;
    $article->category_id = 2;
    $article->brand = 'Hp';
    // $article->fixedasset_code = 1;
    $article->description = 'monitor de 24"';
    $article->save();

    $article = new Article();
    $article->name = 'Impresora Epson';
    $article->model = '45FR';
    $article->serial = 'ZZZZZZZZ';
    $article->quantity = 1;
    $article->price = 1000000;
    $article->category_id = 2;
    $article->brand = 'Epson';
    // $article->fixedasset_code = 1;
    $article->description = 'monitor de 24"';
    $article->save();

    $article = new Article();
    $article->name = 'Impresora Kyosera';
    $article->model = '2020D';
    $article->serial = '1A1A1A1A';
    $article->quantity = 1;
    $article->price = 1000000;
    $article->category_id = 2;
    $article->brand = 'Kyosera';
    // $article->fixedasset_code = 1;
    $article->description = 'monitor de 24"';
    $article->save();

    $article = new Article();
    $article->name = 'Impresora Kyosera';
    $article->model = '2020D';
    $article->serial = 'ZASF66FKF';
    $article->quantity = 1;
    $article->price = 1000000;
    $article->category_id = 2;
    $article->brand = 'Kyosera';
    // $article->fixedasset_code = 1;
    $article->description = 'monitor de 24"';
    $article->save();
  }
}
