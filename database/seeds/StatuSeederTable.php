<?php

use App\Status;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatuSeederTable extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $status = new Status();
    $status->name = 'Asignado';
    $status->description = 'Indica que un Articulo, Usuario, Activo Fijo o Acta de entrega esta asignado';
    $status->save();
    
    $status = new Status();
    $status->name = 'No Asignado';
    $status->description = 'Indica que un Articulo, Activo Fijo o Acta de entrega No a sido asignado';
    $status->save();

    $status = new Status();
    $status->name = 'Asignar Articulos';
    $status->description = 'Indica que un Activo Fijo esta disponible para que sele asignen Articulos';
    $status->save();

    $status = new Status();
    $status->name = 'Disponible';
    $status->description = 'Indica que un Articulo y Usuario esta disponible para ser asiganado';
    $status->save();

    $status = new Status();
    $status->name = 'Mantenimiento';
    $status->description = 'Indica que un Articulo se encuentra en mantenimiento';
    $status->save();

    $status = new Status();
    $status->name = 'Activo';
    $status->description = 'Indica que un Empleado o Esuario esta (Activo) dentro del sistema';
    $status->save();

    $status = new Status();
    $status->name = 'Herrajes';
    $status->description = 'Indica que un Articulo, Fue dado de baja por que su ciclo de vida ya finalizo';
    $status->save();

    $status = new Status();
    $status->name = 'Prestamo';
    $status->description = 'Indica que un Articulo, Fue dado como prestamo momentanio por fallas o daños en algunos de los perifericos del usuario';
    $status->save();

    $status = new Status();
    $status->name = 'Reparación';
    $status->description = 'Indica que un Articulo, Fue enviado a reparacion y se encuentra fuera de la compañia por tal motivo';
    $status->save();

    $status = new Status();
    $status->name = 'Fallas';
    $status->description = 'Indica que un Articulo, Fue cambio por fallas reportadas por el usuario';
    $status->save();

    $status = new Status();
    $status->name = 'Dañado';
    $status->description = 'Indica que un Articulo, Fue cambio por Obsolencencia';
    $status->save();

    $status = new Status();
    $status->name = 'Inconformidad';
    $status->description = 'Indica que un Articulo, Fue cambio por inconformidad del usuario';
    $status->save();
  }
}
