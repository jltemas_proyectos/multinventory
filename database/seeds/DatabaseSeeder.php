
<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $this->truncateTable([
      'status',
      'roles',
      'users',
      'departments',
      'positions',
      'brands',
      'categories',
      'users',
      'employees',
      /* 
      'storage_cellars',
      'fixed_assets',
      'delivery_Certificates',
      'articles',
      'maintenances' */
    ]);
    $this->call(StatuSeederTable::class);
    $this->call(RoleSeederTable::class);
    $this->call(UserSeederTable::class);
    $this->call(BrandSeederTable::class);
    $this->call(PositionSeederTable::class);
    $this->call(DepartmentSeederTable::class);
    $this->call(CategorySeederTable::class);
    $this->call(EmployeeSeederTable::class);

    /* 
    $this->call(StorageCellarSeederTable::class);
    $this->call(FixedAssetSeederTable::class);
    $this->call(DeliveryCertificateSeederTable::class);
    $this->call(ArticleSeederTable::class);
    $this->call(MaintenanceSeederTable::class); */
  }

  protected function truncateTable(array $tables)
  {
    // Deshabilita la revisión de Llave Foranea de la BD
    DB::statement('SET FOREIGN_KEY_CHECKS = 0');
    // Trunca cada Table
    foreach ($tables as $table) {
      DB::table($table)->truncate();
    }
    // Habilita la revisión de Llave Foranea de la BD
    DB::statement('SET FOREIGN_KEY_CHECKS = 1');
  }
}
