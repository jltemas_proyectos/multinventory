<?php

use App\Department;
use Illuminate\Database\Seeder;

class DepartmentSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Department::class,5)->create();

        // $department = new Department();
        // $department->name = "Sistemas";
        // $department->description = 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis, ut possimus ullam qui dolorum vel itaque dignissimos distinctio molestias minima veniam quaerat quisquam consequatur ab rerum et laborum id neque!';
        // $department->save();

        // $department = new Department();
        // $department->name = "Compras";
        // $department->description = 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis, ut possimus ullam qui dolorum vel itaque dignissimos distinctio molestias minima veniam quaerat quisquam consequatur ab rerum et laborum id neque!';
        // $department->save();

        // $department = new Department();
        // $department->name = "Ventas";
        // $department->description = 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis, ut possimus ullam qui dolorum vel itaque dignissimos distinctio molestias minima veniam quaerat quisquam consequatur ab rerum et laborum id neque!';
        // $department->save();

        // $department = new Department();
        // $department->name = "Contabilidad";
        // $department->description = 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis, ut possimus ullam qui dolorum vel itaque dignissimos distinctio molestias minima veniam quaerat quisquam consequatur ab rerum et laborum id neque!';
        // $department->save();
    }
}
