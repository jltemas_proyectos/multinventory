<?php

use App\Employee;
use Illuminate\Database\Seeder;

class EmployeeSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $employee = new Employee();
        $employee->cedula = '1130658445';
        $employee->first_name = "Carlos";
        $employee->second_name = "Eder";
        $employee->first_lastname = "Diaz";
        $employee->second_lastname = "Bueno";
        $employee->phone = "6872000 ext 195";
        $employee->user_id = 1;
        $employee->position_id = "1";
        $employee->department_id = "1";
        $employee->save();

        $employee = new Employee();
        $employee->cedula = '882233221';
        $employee->first_name = "Jhon";
        $employee->second_name = "Iber";
        $employee->first_lastname = "Medina";
        $employee->second_lastname = "Bueno";
        $employee->phone = "6872000 ext 195";
        $employee->user_id = 2;
        $employee->position_id = "2";
        $employee->department_id = "1";
        $employee->save();

        $employee = new Employee();
        $employee->cedula = '1046233221';
        $employee->first_name = "Laura";
        $employee->second_name = "Yised";
        $employee->first_lastname = "Vivas";
        $employee->second_lastname = "Ortiz";
        $employee->phone = "6872000 ext 195";
        $employee->user_id = 3;
        $employee->position_id = "3";
        $employee->department_id = "2";
        $employee->save();

        $employee = new Employee();
        $employee->cedula = '3346233245';
        $employee->first_name = "Tahiry";
        $employee->second_name = "Andrea";
        $employee->first_lastname = "Medina";
        $employee->second_lastname = "Gallego";
        $employee->phone = "6872000 ext 195";
        $employee->user_id = 4;
        $employee->position_id = "4";
        $employee->department_id = "2";
        $employee->save();
    }
}
