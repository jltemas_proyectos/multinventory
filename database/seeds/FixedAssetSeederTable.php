<?php

use App\FixedAsset;
use Illuminate\Database\Seeder;

class FixedAssetSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fixed = new FixedAsset();
        $fixed->code = 0;
        $fixed->storage_cellar_id = 1;
        $fixed->save();
      
        $fixed = new FixedAsset();
        $fixed->code = 1;
        $fixed->storage_cellar_id = 1;
        $fixed->save();
      
        $fixed = new FixedAsset();
        $fixed->code = 2;
        $fixed->storage_cellar_id = 2;
        $fixed->save();
      
        $fixed = new FixedAsset();
        $fixed->code = 3;
        $fixed->storage_cellar_id = 1;
        $fixed->save();
      
        $fixed = new FixedAsset();
        $fixed->code = 4;
        $fixed->storage_cellar_id = 2;
        $fixed->save();
      
        $fixed = new FixedAsset();
        $fixed->code = 5;
        $fixed->storage_cellar_id = 2;
        $fixed->save();
      
        $fixed = new FixedAsset();
        $fixed->code = 6;
        $fixed->storage_cellar_id = 1;
        $fixed->save();
      
        $fixed = new FixedAsset();
        $fixed->code = 7;
        $fixed->storage_cellar_id = 2;
        $fixed->save();
    }
}
