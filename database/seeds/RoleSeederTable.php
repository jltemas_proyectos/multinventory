<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleSeederTable extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {

    $rol = new Role();
    $rol->name = "Administrador";
    $rol->description = "Administra todo el sistema de información";
    $rol->status = '1';
    $rol->save();

    $rol = new Role();
    $rol->name = "Cliente";
    $rol->description = "Usuario del sistema de información";
    $rol->status = '1';
    $rol->save();
  }
}
