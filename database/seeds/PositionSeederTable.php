<?php

use App\Position;
use Illuminate\Database\Seeder;

class PositionSeederTable extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {

    factory(Position::class, 6)->create();
  }
}
