<?php

use Faker\Generator as Faker;

$factory->define(App\Category::class, function (Faker $faker) {
  return [
    'name' => $faker->word(1),
    'description' => $faker->paragraph(1),
    'image' => $faker->imageUrl(600, 400), 
  ];
});
