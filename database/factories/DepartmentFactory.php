<?php

use App\Department;
use App\Status;
use Faker\Generator as Faker;

$factory->define(Department::class, function (Faker $faker) {
  return [
    'name' => $faker->word(1), 
    'description'=>$faker->paragraph(1), 
    'status'=> 6
  ];
});
