<?php

use Faker\Generator as Faker;

$factory->define(App\Position::class, function (Faker $faker) {
    return [
        'name' => $faker->jobTitle, 
        'description'=>$faker->paragraph(1), 
        'status'=> 6
    ];
});
