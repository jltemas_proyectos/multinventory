<?php

use App\{Department, Employee, Position};
use Faker\Generator as Faker;

$factory->define(Employee::class, function (Faker $faker) {
  return [
    'first_name' => $faker->firstName(),
    'second_name' => $faker->name(),
    'first_lastname' => $faker->lastName,
    'second_lastname' => $faker->lastName,
    'cedula' => $faker->tollFreePhoneNumber,
    'phone' => $faker->phoneNumber,
    'department_id' => rand(1, Department::count()),
    'user_id' => 1,
    'position_id' => rand(1, Position::count()),
    'image' => $faker->imageUrl(600, 400,), 'status'
  ];
});
