<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFixedAssetMaintenanceDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fixed_asset_maintenance', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('fixed_asset_id');
            $table->unsignedInteger('maintenance_id');
            $table->text('observation');
            $table->timestamps();

            $table->foreign('fixed_asset_id')->references('id')->on('fixed_assets');
            $table->foreign('maintenance_id')->references('id')->on('maintenances');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fixed_asset_maintenance');
    }
}
