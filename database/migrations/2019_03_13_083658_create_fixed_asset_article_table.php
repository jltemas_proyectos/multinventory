<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFixedAssetArticleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('fixed_asset_article', function (Blueprint $table) {
        $table->increments('id');
        $table->unsignedInteger('fixed_asset_id');
        $table->unsignedInteger('article_id');
        $table->integer('quantity')->default('0');
        $table->unsignedInteger('created_by');
        $table->timestamps();
        
        $table->foreign('created_by')->references('id')->on('users');
        $table->foreign('article_id')->references('id')->on('articles');
        $table->foreign('fixed_asset_id')->references('id')->on('fixed_assets');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fixed_asset_article');
    }
}
