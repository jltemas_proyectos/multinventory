<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryCertificatesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('delivery_certificates', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('code')->unique();
      $table->date('date');
      $table->unsignedInteger('employee_id');
      $table->string('department');
      $table->unsignedInteger('user_id');
      $table->text('observation')->nullable(true);
      $table->unsignedInteger('status')->default('2');
      $table->softDeletes();
      $table->timestamps();

      $table->foreign('user_id')->references('id')->on('users');
      $table->foreign('employee_id')->references('id')->on('employees');
      $table->foreign('status')->references('id')->on('status');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('delivery_certificates');
  }
}
