<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('articles', function (Blueprint $table) {
      $table->increments('id');
      $table->string('name', 30);
      $table->string('brand');
      $table->string('model', 30);
      $table->string('serial', 20);
      $table->integer('quantity');
      $table->integer('price');
      $table->string('image')->default('images/articles/devices.png');
      $table->text('description');
      $table->unsignedInteger('status')->default('2');
      $table->unsignedInteger('category_id');
      $table->unsignedInteger('fixedasset_code')->default('1');
      $table->softDeletes();
      $table->timestamps();

      $table->foreign('category_id')->references('id')->on('categories');
      $table->foreign('fixedasset_code')->references('id')->on('fixed_assets');
      $table->foreign('status')->references('id')->on('status');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('articles');
  }
}
