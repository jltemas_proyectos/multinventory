<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryCertificateFixedAssetDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_certificate_fixed_asset', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('delivery_certificate_id');
            $table->unsignedInteger('fixed_asset_id');
            $table->softDeletes();
            $table->timestamps();
            
            $table->foreign('fixed_asset_id')->references('id')->on('fixed_assets');
            $table->foreign('delivery_certificate_id')->references('id')->on('delivery_certificates');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deliverycertificate_fixedasset');
    }
}
