// $(function () {

//   // Scritp de la vista de la edit-assign-fixed-asset

//   /**
//    * @const addBtn description (Selecciona el boton AddArticle para agregar articulos al Activo Fijo)
//    * 
//    */
//   const addBtn = document.getElementById('addArticle')


//   /** 
//   * @var code description (Selecciona el Fixed Asset Code a asignar)
//   */
//   var code = document.getElementById('code'),

//     /**
//      * @var storageCellar description (Selecciona el Storage Cellar en la que va a estar el Activo Fijo)
//      */
//     storageCellar = document.getElementById('storage_cellar_id'),

//     /** 
//      * @var container description (Selecciona la tabla en donde se visualizan los articulos del Activo Fijo a modificar)
//      */
//     container = document.getElementById('table_detail'),

//     /**
//      * @var  totalEditAsign description (Selecciona el campo Total de la Tabla que muestra el Total de los articulos del Activo Fijo)
//      */
//     totalEditAsign = document.getElementById('total_edit_asign'),

//     /**
//      * @var total description (Vella en contro Total de las sumatoria de la columna Price)
//      */
//     total = parseInt(totalEditAsign.value),

//     /**
//      * @var selectHidden description (Selecciona el Select de los articulos que van a agregarse)
//      */
//     selectHidden = document.getElementById('article'),

//     /**
//      * [btnSave Selecciona el boton de envio de formulario]
//      *
//      * @return  {[type]}  [return description]
//      */
//     btnSave = document.querySelector('[type="submit"]');


//     btnSave.addEventListener('click', e =>{
//         // if (code.value && storageCellar) {
//         //   e.preventDefault()
//         //   console.log(`Code: ${code.value} Storage Cellar: ${storageCellar.value}`)
//         // }      
//     })


//   /**
//     * Crea elementos del DOM
//     */
//   function createElement(tag) {
//     return document.createElement(tag)
//   }

//   /**
//    * Crea filas en la tabla
//    */
//   function createRow() {

//     let tbody = document.getElementById('content-table'),
//       fragment = document.createDocumentFragment(),
//       btnDel = document.getElementById('col-btn-delete'),
//       row = createElement('tr'),
//       col = createElement('td'),
//       select = selectHidden.cloneNode(true),
//       col2 = createColumn('brands', 'brands[]'),
//       col3 = createColumn('models', 'models[]'),
//       col4 = createColumn('quantity', 'quantities[]'),
//       col5 = createColumn('serial', 'serials[]'),
//       col6 = createColumn('fixed', 'fixeds[]', code.value),
//       col7 = createColumn('price', 'prices[]'),
//       col8 = btnDel.cloneNode(true)
//     select.style.visibility = 'visible'
//     col.appendChild(select)

//     fragment.appendChild(col)
//     fragment.appendChild(col2)
//     fragment.appendChild(col3)
//     fragment.appendChild(col4)
//     fragment.appendChild(col5)
//     fragment.appendChild(col6)
//     fragment.appendChild(col7)
//     fragment.appendChild(col8)
//     row.appendChild(fragment)
//     tbody.appendChild(row)

//   }

//   /*
//     * Crea columnas en la tabla 
//     */
//   function createColumn(id, name, value) {
//     let col = createElement('td'),
//       input = createElement('input')
//     input.setAttribute('type', 'text')
//     input.id = id
//     input.setAttribute('name', name)
//     if (value) { input.setAttribute('value', (value) ? value : '') } else { '' }
//     input.setAttribute('required', '')
//     input.classList.add('form-control', 'col-lg-1')
//     col.appendChild(input)
//     return col
//   }

//   /**
//    * Elimina articulos del Activo Fijo y reculcula el valor toal del mismo
//    */
//   container.addEventListener('click', e => {

//     if (e.target.classList.contains('icon_close_alt2')) {
//       if (e.target.parentElement.parentElement.previousElementSibling.firstChild.value) {
//         total = total - parseInt(e.target.parentElement.parentElement.previousElementSibling.firstChild.value)
//         e.target.parentElement.parentElement.parentElement.remove()
//         totalEditAsign.value = total
//       } else {
//         e.target.parentElement.parentElement.parentElement.remove()
//       }
//     }

//     if (e.target.classList.contains('btn-danger')) {
//       if (e.target.parentElement.previousElementSibling.firstChild.value) {
//         total = total - parseInt(e.target.parentElement.previousElementSibling.firstChild.value)
//         e.target.parentElement.parentElement.remove()
//         totalEditAsign.value = total
//       } else {
//         e.target.parentElement.parentElement.remove()
//       }
//     }
//   })

//   /**
//    * Cambia el articulo seleccionado y recalcula el valor Total del Activo Fijo
//    */
//   container.addEventListener("change", e => {
//     if (e.target.classList.contains("articles")) {

//       let article_id = e.target.value,
//         brand = e.target.parentElement.nextElementSibling.firstElementChild,
//         model = e.target.parentElement.nextElementSibling.nextElementSibling.firstElementChild,
//         quantity = e.target.parentElement.nextElementSibling.nextElementSibling.nextElementSibling.firstElementChild,
//         serial = e.target.parentElement.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.firstElementChild,
//         price = e.target.parentElement.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.firstElementChild,
//         article_price = e.target.parentElement.parentElement.lastElementChild.previousElementSibling.firstElementChild,
//         token = document.getElementsByName("_token");

//       //Ajax Jquery

//       let dato = article_id,
//         url = `/fixed-assets`,
//         _token = token[0].value;

//       $.ajax({
//         type: "POST",
//         headers: { 'X-CSRF-TOKEN': _token },
//         url: url,
//         data: {
//           article_id: dato,
//           code: code.value,
//           storage_cellar: storageCellar.value
//         },
//         dataType: "json",
//         success: function (response) {

//           // console.log(response)
//           brand.setAttribute('value', response.brand)
//           model.setAttribute('value', response.model)
//           quantity.setAttribute('value', response.quantity)
//           serial.setAttribute('value', response.serial)
//           price.setAttribute('value', response.price)

//           total = total + parseInt(article_price.value)
//           totalEditAsign.value = total
//         }
//       });

//     }
//   })

//   addBtn.addEventListener('click', e => {
//     createRow()
//   })

// })

{
  import {code} from './myscript.js'
  
}