$(function () {

  // Scritp de la vista de la edit-assign-fixed-asset

  /**
   * @const btnAddAF description (Selecciona el boton Add Fixed Asset para agregar un Activo Fijo con todos sus Articulos)
   * 
   */
  const btnAddAF = document.getElementById("btnAddFA"),
    code = document.getElementById('code')


  /** 
   * @var container description (Selecciona la tabla en donde se visualizan los articulos del Activo Fijo a modificar)
   */
  container = document.getElementById("tbody"),

    /**
     * @var  totalEditDelivery description (Selecciona el campo Total de la Tabla que muestra el Total de los articulos del Activo Fijo)
     */
    totalEditDelivery = document.getElementById('total_edit_delivery'),

    /**
     * @var total description (Vella en contro Total de las sumatoria de la columna Price)
     */
    total = parseInt(totalEditDelivery.value),

    /**
     * [count Contador para uamentar los ids de las filas nuevas que se van creando de uno en uno]
     *
     * @return  {[numbre]}  [return number]
     */
    count = 1,

    /**
     * [token description]
     *
     * @return  {[type]}  [return description]
     */
    token = document.getElementsByName("_token");

  /**
   * @var selectHidden description (Selecciona el Select de los articulos que van a agregarse)
   */
  selectFixedAssetHidden = document.getElementById('selectFixedasset')

  /**
    * Crea elementos del DOM
    */
  function createElement(tag) {
    return document.createElement(tag)
  }

  /**
   * Crea filas en la tabla
   */
  function createRow() {

    let tbody = document.getElementById('tbody'),
      fragment = document.createDocumentFragment(),
      btnDel = document.getElementsByClassName('btn-danger')[0],
      row = createElement('tr'),
      col = createElement('td'),
      col8 = createElement('td'),
      select = selectFixedAssetHidden.cloneNode(true),
      col2 = createColumn('article', 'articles[]'),
      col3 = createColumn('brands', 'brands[]'),
      col4 = createColumn('models', 'models[]'),
      col5 = createColumn('serial', 'serials[]'),
      col6 = createColumn('quantity', 'quantity[]'),
      col7 = createColumn('price', 'prices[]')
    col8.appendChild(btnDel.cloneNode(true))

    select.style.visibility = 'visible'
    select.setAttribute('name', 'fixedassets[]')
    col.appendChild(select)



    fragment.appendChild(col)
    fragment.appendChild(col2)
    fragment.appendChild(col3)
    fragment.appendChild(col4)
    fragment.appendChild(col5)
    fragment.appendChild(col6)
    fragment.appendChild(col7)
    fragment.appendChild(col8)
    row.appendChild(fragment)
    tbody.appendChild(row)

  }

  /**
   * [createColumn Crea columnas dentro de una table]
   *
   * @param   {[String]}  id     [id Indica el valor del atributo id de la columna que se esta creando]
   * @param   {[String]}  name   [name Indica el valor del tributo name de la columna que se esta creando]
   * @param   {[String | Number]}  value  [value Indica el valor del atributo value de la columna que se esta creando ]
   *
   * @return  {[object]}         [return una columna de una table con los valores de atributos id, name, value]
   */
  function createColumn(id, name, value) {
    let col = createElement('td'),
      input = createElement('input')
    input.setAttribute('type', 'text')
    input.id = id
    input.setAttribute('name', name)
    if (value) { input.setAttribute('value', (value) ? value : '') } else { '' }
    input.setAttribute('required', '')
    input.classList.add('form-control', 'col-lg-1')
    col.appendChild(input)
    return col
  }

  /**
   * [createRowArticle description]
   *
   * @param   {[object]}  response  [response este objeto contiena la respuesta ajax de todos los articulos que tiene un FixedAsset]
   *
   */
  function createRowArticle(response) {
    let template = document.createDocumentFragment(),
      row = document.createElement('tr'),
      col2 = createColumn(`article${count}`, 'articles[]'),
      col3 = createColumn(`brands${count}`, 'brands[]'),
      col4 = createColumn(`model${count}`, 'models[]'),
      col5 = createColumn(`serial${count}`, 'serials[]'),
      col6 = createColumn(`quantity${count}`, 'quantitys[]'),
      col7 = createColumn(`price${count}`, 'prices[]'),
      col8 = document.createElement('td'),
      btnDelete = document.createElement('buttom'),
      i = document.createElement('i')

    btnDelete.classList.add('btn', 'btn-danger')
    i.classList.add('icon_close_alt2')
    btnDelete.appendChild(i)
    col8.appendChild(btnDelete)

    col2.firstElementChild.value = response.name
    col3.firstElementChild.value = response.brand
    col4.firstElementChild.value = response.model
    col5.firstElementChild.value = response.serial
    col6.firstElementChild.value = response.quantity
    col7.firstElementChild.value = response.price

    template.appendChild(col2)
    template.appendChild(col3)
    template.appendChild(col4)
    template.appendChild(col5)
    template.appendChild(col6)
    template.appendChild(col7)
    template.appendChild(col8)

    row.appendChild(template)
    tbody.appendChild(row)
    count++

    total += response.price
  }

  /**
   * [ajaxDelete Esta funcion eliminia un articulo de un FixedAsset]
   *
   * @param   {[string]}  url    [url Direccion de donde se va hacer la peticion Ajax]
   * @param   {[string]}  token  [token Token de seguridad de Laravel para hacer el envio de la Data]
   * @param   {[object]}  data   [data objeto con datos de la petion Ajax]
   *
   * @return  {[object]}         [return return la confirmacion de la accion realizada]
   */
  function ajaxDelete(url, data) {

    $.ajax({
      type: "POST",
      headers: { 'X-CSRF-TOKEN': token[0].value },
      url: url,
      data: data,
      dataType: "JSON",
      success: function (response) {
        console.log(response)
      }
    });
  }

  /**
   * [ajaxDeleteIcon Elimina el articulo cuando se hace clic directamente en el Icon de Eliminar]
   *
   * @param   {[object]}  e  [e objeto Evento]
   *
   */
  function ajaxDeleteIcon(e) {
    e.stopPropagation()
    let reason = e.target.classList.item(1),
      fixedasset = e.target.parentElement.parentElement.parentElement.previousElementSibling.previousElementSibling.previousElementSibling.previousElementSibling.previousElementSibling.previousElementSibling.previousElementSibling.firstElementChild.dataset.fixed,
      article = e.target.parentElement.parentElement.parentElement.previousElementSibling.previousElementSibling.previousElementSibling.previousElementSibling.previousElementSibling.previousElementSibling.firstElementChild.dataset.article,
      colPrice = parseInt(e.target.parentElement.parentElement.parentElement.previousElementSibling.firstElementChild.value),
      tr = e.target.parentElement.parentElement.parentElement.parentElement,
      url = '/delivery-certificates/ajax/delete',
      data = {
        deliveryC: code.value,
        article: article,
        reason: reason,
        fixedasset: fixedasset
      }

    ajaxDelete(url, data)

    if (colPrice) {
      total -= colPrice
      tr.remove()
      totalEditDelivery.value = total
    } else {
      tr.remove()
    }
  }

  /**
   * [ajaxDeleteBtn Elemina un articulo cuando se hace directamente clic sobre el btn]
   *
   * @param   {[object]}  e  [e Objeto Evento]
   *
   */
  function ajaxDeleteBtn(e) {
    e.stopPropagation()
    let reason = e.target.classList.item(1),
      colPrice = parseInt(e.target.parentElement.parentElement.previousElementSibling.firstElementChild.value),
      article = e.target.parentElement.parentElement.previousElementSibling.previousElementSibling.previousElementSibling.previousElementSibling.previousElementSibling.previousElementSibling.firstElementChild.dataset.article,
      tr = e.target.parentElement.parentElement.parentElement,
      url = '/delivery-certificates/ajax/delete',
      data = {
        article: article,
        reason: reason
      }

    ajaxDelete(url, data)

    if (colPrice) {
      total -= colPrice
      tr.remove()
      totalEditDelivery.value = total
    } else {
      tr.remove()
    }
  }

  /**
   * Elimina articulo del Activo Fijo y reculcula el valor total del acta
   */
  container.addEventListener('click', e => {

    if (e.target.dataset.icon) {
      ajaxDeleteIcon(e)
    } else if (e.target.dataset.btn) {
      ajaxDeleteBtn(e)
    }

  })

  /**
   * Cambia el articulo seleccionado y recalcula el valor Total del Activo Fijo
   */
  container.addEventListener("change", e => {
    if (e.target.classList.contains("articles")) {

      let selectFixed = e.target.parentElement,
        article_id = e.target.value,
        article = e.target.parentElement.nextElementSibling.firstElementChild,
        brand = e.target.parentElement.nextElementSibling.nextElementSibling.firstElementChild,
        model = e.target.parentElement.nextElementSibling.nextElementSibling.nextElementSibling.firstElementChild,
        serial = e.target.parentElement.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.firstElementChild,
        quantity = e.target.parentElement.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.firstElementChild,
        price = e.target.parentElement.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.firstElementChild


      //Ajax Jquery

      let dato = article_id,
        url = `/delivery-certificates/ajax/edit`,
        _token = token[0].value

      $.ajax({
        type: "POST",
        headers: { 'X-CSRF-TOKEN': _token },
        url: url,
        data: {
          fixedasset: dato,
        },
        dataType: "JSON",
        success: function (response) {
          let length = response.length;
          for (let i = 0; i < length; i++) {
            if (i === 0) {
              selectFixed.setAttribute('rowspan', length)
              article.setAttribute('value', response[i].name)
              brand.setAttribute('value', response[i].brand)
              model.setAttribute('value', response[i].model)
              serial.setAttribute('value', response[i].serial)
              quantity.setAttribute('value', response[i].quantity)
              price.setAttribute('value', response[i].price)
              total += response[i].price
              continue
            } else {
              createRowArticle(response[i])
            }

          }
          totalEditDelivery.value = total
        }
      })

    }
  })

  btnAddAF.addEventListener('click', e => {
    createRow()
  })



})

