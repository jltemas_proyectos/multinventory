//Definiciones
const logoutLink = document.getElementById("logout-link"),
  listEmployee = document.getElementById("list-employees");

// Funciones
const logout = (e) => {
  e.preventDefault();
  const logoutForm = document.getElementById("logout-form");
  logoutForm.submit();
};

/**
 * 
 * @param {*} e Event element
 * 
 * allow delete a element from table list
 */
const deleteElementList = (e) => {
  e.preventDefault();
  const employee = e.target,
    formDeleteElement = document.getElementById("form-delete-element"),
    inputId = document.querySelector('.input-id');

  if (employee.dataset.employeeId) {
    const id = employee.dataset.employeeId,
      tr = employee.parentElement.parentElement.parentElement;

    formDeleteElement.action = `${window.location.href}/${id}`;
    inputId.value = id;
    formDeleteElement.submit();
    tr.remove()
  }
  // const btnDelete = e.target,
  //   formDeleteElement = document.querySelector('.form-delete-element');
  console.log(e.target);

  // if (
  //   btnDelete.classList.contains("btn-danger") ||
  //   btnDelete.classList.contains("form-delete-element") ||
  //   btnDelete.classList.contains("icon_close_alt2")
  // ) {
  //   console.log(btnDelete);
  //   formDeleteElement.submit();
  // }
  // alert("aquí");
};

// Eventos
logoutLink.addEventListener("click", logout);

listEmployee.addEventListener("click", deleteElementList);
