$(function () {

  //Start FixedAssets

  // Definición de Contantes y Variables
  const container = document.getElementById("content-table"),
    addBtn = document.getElementById("addArticle");

  // Variables y constantes
  // (let articles) contiene todos los articulos disponibles para asignar de la base de datos
  let articles = JSON.parse(localStorage.getItem('articles')),

    // (let total) Vella en contro Total de las sumatoria de la columna Price
    total = 0,

    // (let colTotal) Indica el valor de columna donde se muestra el Total de los articulos
    colTotal = document.getElementById('total'),

    // (let totalEditAsign) Indica el valor total del formulario edit-asign-fixed-asset
    totalEditAsign = document.getElementById('total-edit-asign'),

    // (let code) Indica el codigo del activo fijo a asignar
    code = document.getElementById('code'),

    // (let storageCellar) indica la bodega en la que va a estar el activo fijo
    storageCellar = document.getElementById('storage_cellar_id'),

    // [selectedCode Indica el continido del select "Fixed Asset Code"]
    selectedCode = '',

    mainSelect = Array.from(document.querySelectorAll('.articles'))
  // mainSelect = document.getElementById('article')

  // (let fixedStatic) Hace referencia al campo activo de cada articulo relacionado en la tabla
  fixedStatic = document.getElementById('fixedContent'),

    //[articlcesSelected Guardara todos los articulos seleccionados por el usuario]
    articlesSelected = [];

  // Funciones

  /**
   * verifica si un objeto esta vacio
   */
  function isEmpty(obj) {
    for (let key in obj) {
      if (obj.hasOwnProperty(key))
        return false
    }
    return true
  }

  /**
   * Crea elementos en el DOM
   */
  function createElement(tag) {
    return document.createElement(tag)
  }

  /*
  * Crea columnas en la tabla con etiquetas td e input
  */
  function createColumn(id, name, value, type) {
    let col = createElement('td'),
      input = createElement('input')
    input.setAttribute('type', type)
    input.id = id
    input.setAttribute('name', name)
    input.setAttribute('required', '')
    if (value) {
      input.setAttribute('value', value)
      input.classList.add('js-fixedHidden')
    } else {
      input.classList.add('form-control', 'col-sm-1')
    }
    col.appendChild(input)
    return col
  }

  /**
   * [queryDB hace consulta en la BD ]
   *
   * @param   {[String]}  method    [method Hace referencia el metodo HTTP de envio]
   * @param   {[String]}  url       [url Hace referencia a la dirección donde se van a consultar los datos]
   * @param   {[String]}  data      [data Hace referencia a los datos que se van a enviar en la cabecera de la petición]
   * @param   {[String]}  dataType  [dataType Hace referencia al tipo de respuesta generada por el servidor]
   *
   */
  function queryDB(method, url, data = '', dataType = 'json') {
    $.ajax({
      type: method,
      url: url,
      data: data,
      dataType: dataType,
      success: function (response) {
        localStorage.clear()
        localStorage.setItem('data', JSON.stringify(response))
        localStorage.setItem('articles', JSON.stringify(response))
      }
    })
  }

  /**
   * [selectCode Permite validar el que este seleccionado el codigo FixedAsset a asignar de cada articulo]
   *
   * @param   {[HTMLElement]}  selectElement  [code elemento select a evaluar datos y contenido]
   *
   */
  function selectElementF(selectElement) {
    if (selectElement.value && selectElement.selectedIndex !== 0) {
      if (selectElement.parentElement.classList.contains('has-error') || selectElement.selectedIndex !== 0) {

        if (selectElement.id === 'code') {

          selectElement.parentElement.classList.remove('has-error')
          fixedStatic.value = size(selectElement)

          let fixedStaticHidden = hiddenFixeds()
          for (const element of fixedStaticHidden) {
            element.value = selectElement.value
          }

          let fixeds = Array.from(document.querySelectorAll('.js-fixed'))
          for (const element of fixeds) {
            element.value = size(code)
          }
        }

        if (selectElement.id === 'storage_cellar_id') {
          selectElement.parentElement.classList.remove('has-error')
        }
      }

    } else {
      if (selectElement.id === 'code') {
        selectElement.parentElement.classList.add('has-error')
        fixedStatic.value = ''
        let fixedStaticHidden = hiddenFixeds()
        for (const element of fixedStaticHidden) {
          element.value = ''
        }
      }
      if (selectElement.id === 'storage_cellar_id') {
        selectElement.parentElement.classList.add('has-error')
      }
    }

  }
  /**
   * [calcLength calcula la longitud del valor de un select]
   *
   * @param   {[type]}  param  [param description]
   *
   * @return  {[type]}         [return description]
   */
  function size(selectElement) {
    let value;
    switch (selectElement.value.length) {
      case 1:
        value = `000${selectElement.value}`
        break;

      default:
        break;
    }
    return value
  }

  /**
   * [buttonDelete Crea los botones de eliminar de la aplicacíon]
   *
   * @return  {[HTMLElement]}  [return button]
   */
  function buttonDelete() {
    let deleteRow = createElement('button'),
      i = createElement('i')

    deleteRow.classList.add('btn', 'btn-danger', 'remove-row')
    deleteRow.id = 'remove-row'
    deleteRow.setAttribute('type', 'button')
    i.classList.add('icon_close_alt2')
    deleteRow.appendChild(i)

    return deleteRow
  }

  /**
   * [hiddenFixeds optiene todos los input ocultos del cogido de AF]
   *
   * @return  {[Array]}  [return Array con todos los campos inputs oculto]
   */
  function hiddenFixeds() {
    return Array.from(document.querySelectorAll('.js-fixedHidden'))
  }

  /**
   * [createSelectArticles Crea los select que contienen los articulos]
   *
   * @return  {[object]}        [return select con todos los articulos]
   */
  function createSelectArticles() {
    let articles = JSON.parse(localStorage.getItem('articles')),
      select = createElement('select'),
      options = document.createDocumentFragment(),
      optionDefault = createElement('option')

    articles.forEach(article => {
      let option = createElement('option')
      if (!options.hasChildNodes()) {
        options.appendChild(optionDefault)
        option.value = article.id
        option.textContent = `${article.name} - ${article.serial}`
        options.appendChild(option)
      } else {
        option.value = article.id
        option.textContent = `${article.name} - ${article.serial}`
        options.appendChild(option)
      }
    })

    select.classList.add('form-control', 'col-sm-3', 'articles')
    select.setAttribute('required', '')
    select.setAttribute('name', 'articles[]')
    select.appendChild(options)

    return select

  }

  /**
   * [createRow crea cada articulo en la table articles]
   */
  function createRow() {
    let template = document.createDocumentFragment(),
      tbody = document.getElementById('content-table'),
      row = createElement('tr'),
      deleteRow = buttonDelete(),
      select = createSelectArticles(),
      fixed = code.value,
      col1 = createElement('td'),
      col2 = createColumn(`brands${count}`, "brands[]", '', 'text'),
      col3 = createColumn(`model${count}`, "models[]", '', 'text'),
      col4 = createColumn(`quantity${count}`, "quantities[]", '', 'text'),
      col5 = createColumn(`serial${count}`, "serials[]", '', 'text'),
      col6 = createColumn(`fixed${count}`, "fixeds[]", fixed, 'hidden'),
      col7 = createColumn(`price${count}`, "prices[]", '', 'text'),
      col8 = createElement('td'),
      fixedContent = createElement('input', '', fixed, 'text');

    fixedContent.classList.add('form-control', 'js-fixed')
    selectedCode = Array.from(code)
    fixedContent.setAttribute('value', selectedCode[code.selectedIndex].textContent)
    col6.appendChild(fixedContent)

    select.id = `article${count}`
    col8.appendChild(deleteRow)

    col1.appendChild(select)
    row.classList.add('row-js')
    row.appendChild(col1)
    row.appendChild(col2)
    row.appendChild(col3)
    row.appendChild(col4)
    row.appendChild(col5)
    row.appendChild(col6)
    row.appendChild(col7)
    row.appendChild(col8)
    template.appendChild(row)
    tbody.appendChild(template)
    count++
  }

  /**
   * [verifyArticlesSelected Verifica que el nuevo select a crear no contenga uno de los articulos seleccionados con anterioridad]
   *
   */
  function verifyArticlesSelected() {
    // Selecciona todos los select que contengan la clases (articles)
    let selectedArticles = Array.from(document.querySelectorAll('.articles')),
      // Obtiene de localStorage el contenido del indice (articles)
      articlesLs = JSON.parse(localStorage.getItem('articles'))

    selectedArticles.forEach(select => {
      articlesLs.forEach((article, index) => {
        if (select.value == article.id) {
          articlesLs.splice(index, 1)
          localStorage.setItem('articles', JSON.stringify(articlesLs))
        }
      })
    })

    createRow()
  }

  function deleteArticleRow(findArticle) {

    const data = JSON.parse(localStorage.getItem('data'))
    let articlesOld = JSON.parse(localStorage.getItem('articles')),
      exist;

    articlesOld.forEach(article => {
      if (article.id === findArticle) {
        exist = true
      } else {
        exist = false
      }
    });

    data.forEach(article => {
      if (exist) {
        if (article.id === findArticle) {
          articlesOld.push(article)
          localStorage.setItem('articles', JSON.stringify(articlesOld.sort((art1, art2) => art1.id - art2.id)))
        }
      } else {
        localStorage.setItem('article', JSON.stringify(articlesOld))
      }
    })
  }

  // Eventos

  addEventListener('DOMContentLoaded', () => {
    queryDB(`GET`, `/fixed-assets`, '', 'json')
  })

  // Este metodo escuche el evento "change" y establece el valor del compo activo fijo 
  // que se muestra al ingresar al formulario de 

  code.addEventListener('change', e => {
    e.stopPropagation()
    selectElementF(e.target)
  })

  /**
   * Valida que el campo "Storage Cella" tengo un valor
   */
  storageCellar.addEventListener('change', e => {
    e.stopPropagation()
    selectElementF(storageCellar)
  })

  /**
   * Agrega fila a la tabla Articulos en la vista "create-fixed-asset"
   */
  let count = 1

  addBtn.addEventListener("click", () => {

    let lastRow = container.lastElementChild.firstElementChild.firstElementChild

    if (code.selectedIndex !== 0 && storageCellar.selectedIndex !== 0 && lastRow.selectedIndex !== 0) {
      verifyArticlesSelected()
    } else if ((!code.value && code.selectedIndex === 0) && (!storageCellar.value && storageCellar.selectedIndex === 0)) {
      code.parentElement.classList.add('has-error')
      storageCellar.parentElement.classList.add('has-error')
      alert(`Seleccione un valor para "Fixed Asset Code" y para "Storage Cellar" Primero!`)
    } else if (!code.value) {
      code.parentElement.classList.add('has-error')
      alert(`Seleccione un valor para "Fixed Asset Code" Primero!`)
    } else if (!storageCellar.value) {
      storageCellar.parentElement.classList.add('has-error')
      alert(`Seleccione un valor para "Storage Cellar" Primero!`)
    } else if (!lastRow.value) {
      lastRow.parentElement.classList.add('has-error')
      alert(`Seleccione un valor para "Article" Primero!`)
    }
  });

  //  Cactuta el evento change en todos los desendientes del Tbody
  container.addEventListener("change", e => {
    if (e.target.classList.contains("has-error") || e.target.classList.contains("articles") && (code.value && code.selectElement !== 0) && (storageCellar.value && storageCellar.selectedIndex !== 0)) {

      e.target.parentElement.classList.remove("has-error")
      let article_id = e.target.value,
        brand = e.target.parentElement.nextElementSibling.firstElementChild,
        model = e.target.parentElement.nextElementSibling.nextElementSibling.firstElementChild,
        quantity = e.target.parentElement.nextElementSibling.nextElementSibling.nextElementSibling.firstElementChild,
        serial = e.target.parentElement.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.firstElementChild,
        price = e.target.parentElement.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.firstElementChild,
        article_price = e.target.parentElement.parentElement.lastElementChild.previousElementSibling.firstElementChild;
      // colDelete = e.target.parentElement.parentElement.lastElementChild,
      // btnDeleteRow = buttonDelete()

      // if (!colDelete.hasChildNodes()) {
      //   colDelete.appendChild(btnDeleteRow)
      // }

      for (const article of JSON.parse(localStorage.getItem('articles'))) {
        if (article.id == article_id) {
          brand.setAttribute('value', article.brand)
          model.setAttribute('value', article.model)
          quantity.setAttribute('value', article.quantity)
          serial.setAttribute('value', article.serial)
          price.setAttribute('value', article.price)
          break
        }
      }

      if (!isEmpty(article_price.value)) {
        total = total + parseInt(article_price.value)
      }
      colTotal.textContent = total

    } else {
      if (!code.value && storageCellar.value) {
        e.target.selectedIndex = 0
        code.parentElement.classList.add('has-error')
        alert(`Establesca el valor para "Fixed Asset Code" para continuar!`)
      }
      if (!storageCellar.value && code.value) {
        e.target.selectedIndex = 0
        storageCellar.parentElement.classList.add('has-error')
        alert(`Establesca el valor para "Storage Cellar" para continuar!`)
      }

      if (!code.value && !storageCellar.value) {
        e.target.selectedIndex = 0
        code.parentElement.classList.add('has-error')
        storageCellar.parentElement.classList.add('has-error')
        alert(`Establesca el valor para "Fixed Asset Code" y para "Storage Cellar" para continuar!`)
      }
    }
  })

  //  Cactuta el evento click en todos los desendientes del Tbody
  container.addEventListener("click", e => {
    e.stopPropagation();
    let btnDelete = e.target.classList.contains("remove-row"),
      iconDelete = e.target.classList.contains("icon_close_alt2"),
      selectValueBtn = parseInt(e.target.parentElement.parentElement.firstElementChild.firstElementChild.value),
      selectValueIcon = parseInt(e.target.parentElement.parentElement.parentElement.firstElementChild.firstElementChild.value);

    /**
     * El limina las filas de los articulos Agregados y resta al total el valor de la "price"
     * de la fila eliminada
     */
    if (btnDelete) {
      article_price = e.target.parentElement.previousElementSibling.firstElementChild
      if (isEmpty(article_price)) {
        total = total + 0
      }
      total = total - article_price.value
      colTotal.textContent = total
      e.target.parentElement.parentElement.remove()

      deleteArticleRow(selectValueBtn)
    } else if (iconDelete) {
      article_price = e.target.parentElement.parentElement.previousElementSibling.firstElementChild
      if (isEmpty(article_price)) {
        total = total - 0
      }
      total = total - article_price.value
      colTotal.textContent = total
      e.target.parentElement.parentElement.parentElement.remove();
      deleteArticleRow(selectValueIcon)
    }
  });
  //End FixedAssets
})




