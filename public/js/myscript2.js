$(function () {

  /**
    * verifica si un objeto esta vacio
    */
  function isEmpty(obj) {
    for (let key in obj) {
      if (obj.hasOwnProperty(key))
        return false;
    }
    return true;
  }


  // Crea Filas que se le agregaran a la tabla con boton 

  function createRow() {
    let template = document.createDocumentFragment(),
      row = document.createElement('tr'),
      option = document.getElementById('fixedasset'),
      col1 = document.createElement('td'),
      col2 = createColumn(`article${count}`, 'articles[]'),
      col3 = createColumn(`brands${count}`, 'brands[]'),
      col4 = createColumn(`model${count}`, 'models[]'),
      col5 = createColumn(`serial${count}`, 'serials[]'),
      col6 = createColumn(`quantity${count}`, 'quantitys[]'),
      col7 = createColumn(`price${count}`, 'prices[]'),
      col8 = document.createElement('td'),
      btnDelete = document.createElement('buttom'),
      i = document.createElement('i')

    selectFix = option.cloneNode(true)
    selectFix.id = `fixedassets${count}`
    btnDelete.classList.add('btn', 'btn-danger')
    i.classList.add('icon_close_alt2')
    btnDelete.appendChild(i)
    col8.appendChild(btnDelete)
    col1.appendChild(selectFix)

    console.log(selectFix)

    template.appendChild(col1)
    template.appendChild(col2)
    template.appendChild(col3)
    template.appendChild(col4)
    template.appendChild(col5)
    template.appendChild(col6)
    template.appendChild(col7)
    template.appendChild(col8)

    row.appendChild(template)
    tbody.appendChild(row)
    count++

  }


  // Crea columnas en la tabla 

  function createColumn(id, name, value) {
    let col = document.createElement('td'),
      input = document.createElement('input')
    input.setAttribute('type', 'text')
    input.setAttribute('readonly', '')
    input.id = id
    input.setAttribute('name', name)
    if (value) { input.setAttribute('value', (value) ? value : '') } else { '' }
    input.setAttribute('required', '')
    input.classList.add('form-control', 'col-lg-1')
    col.appendChild(input)
    return col
  }

  // Optiene el Select Departamento
  let token = document.getElementsByName('_token'),
    selectEmployees = document.getElementById('employee_id'),
    option = document.createElement('option'),
    id = document.getElementById('cedula'),
    addArt = document.getElementById('addArticle'),
    section = document.getElementById('conten-form'),
    colTotal = document.getElementById('priceTotal'),
    count = 1,
    total = 0

  option.setAttribute('value', '')

  // Crea las filas dentro de la tabla cada vez que se hace clic en el boton AddArticle
  addArt.addEventListener('click', () => {
    createRow()
  })


  function createRowArticle(response) {
    let template = document.createDocumentFragment(),
      row = document.createElement('tr'),
      col2 = createColumn(`article${count}`, 'articles[]'),
      col3 = createColumn(`brands${count}`, 'brands[]'),
      col4 = createColumn(`model${count}`, 'models[]'),
      col5 = createColumn(`serial${count}`, 'serials[]'),
      col6 = createColumn(`quantity${count}`, 'quantitys[]'),
      col7 = createColumn(`price${count}`, 'prices[]'),
      col8 = document.createElement('td'),
      btnDelete = document.createElement('buttom'),
      i = document.createElement('i')

    btnDelete.classList.add('btn', 'btn-danger')
    i.classList.add('icon_close_alt2')
    btnDelete.appendChild(i)
    col8.appendChild(btnDelete)

    col2.firstElementChild.value = response.name
    col3.firstElementChild.value = response.brand
    col4.firstElementChild.value = response.model
    col5.firstElementChild.value = response.serial
    col6.firstElementChild.value = response.quantity
    col7.firstElementChild.value = response.price

    template.appendChild(col2)
    template.appendChild(col3)
    template.appendChild(col4)
    template.appendChild(col5)
    template.appendChild(col6)
    template.appendChild(col7)
    template.appendChild(col8)

    row.appendChild(template)
    tbody.appendChild(row)
    count++
  }

  //Despliega los articulos que pertenecen a un activo fijo
  section.addEventListener('change', e => {

    if (e.target.classList.contains('fixedasset')) {

      let data = e.target.value,
        url = `/delivery-certificates`,
        length = 0,
        colFixed = e.target.parentElement,
        colArticle = e.target.parentNode.nextElementSibling.firstElementChild,
        colBrand = e.target.parentNode.nextElementSibling.nextElementSibling.firstElementChild,
        colModel = e.target.parentNode.nextElementSibling.nextElementSibling.nextElementSibling.firstElementChild,
        colSerial = e.target.parentNode.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.firstElementChild,
        colQuantity = e.target.parentNode.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.firstElementChild,
        colPrice = e.target.parentNode.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.firstElementChild

      $.ajax({
        type: "POST",
        headers: { 'X-CSRF-TOKEN': token[0].value },
        url: url,
        data: { fixedasset: data },
        dataType: "json",
        success: function (response) {
          length = response.length
          for (let i = 0; i < length; i++) {
            if (i === 0) {
              colFixed.setAttribute('rowspan', length)
              colArticle.value = response[i].name;
              colBrand.value = response[i].brand
              colModel.value = response[i].model
              colSerial.value = response[i].serial
              colQuantity.value = response[i].quantity
              colPrice.value = response[i].price

              total = total + parseInt(colPrice.value)
              continue
            }
            createRowArticle(response[i])
            total = total + parseInt(colPrice.value)
          }
          colTotal.textContent = total
          console.log(colPrice.value)
          console.log(total)
        }
      })


    }

    if (e.target.id === 'department_id') {

      e.stopImmediatePropagation()
      let url = '/departments',
        department = e.target.value;

      if (selectEmployees.childElementCount > 1) {
        selectEmployees.textContent = ''
        selectEmployees.appendChild(option)
      }

      $.ajax({
        type: "POST",
        headers: { 'X-CSRF-TOKEN': token[0].value },
        url: url,
        data: { department: department },
        dataType: "json",
        success: function (response) {

          let template = document.createDocumentFragment()

          for (item of response) {
            let optionEmployee = document.createElement('option')
            optionEmployee.setAttribute('value', item.id)
            optionEmployee.textContent = `${item.first_name} ${item.first_lastname}`
            template.appendChild(optionEmployee)
          }

          selectEmployees.appendChild(template)
        }
      });
    }

    if (e.target.id === 'employee_id') {
      let url = '/employees/ajax',
        employee = e.target.value;

      $.ajax({
        type: "POST",
        headers: { 'X-CSRF-TOKEN': token[0].value },
        url: url,
        data: { employee: employee },
        dataType: "json",
        success: function (response) {
          id.value = response[0].cedula
          console.log(employee)
          console.log(response)
        }
      });
    }
  })

  //Elimina Las filas de los articulos
  tbody.addEventListener('click', e => {

    let icon = e.target.classList.contains('icon_close_alt2'),
      btn = e.target.classList.contains('btn-danger')

    if (icon) {
      total = total -
        e.target.parentElement.parentElement.parentElement.remove()
    } else if (btn) {
      e.target.parentElement.parentElement.remove()
    }

  })

})


