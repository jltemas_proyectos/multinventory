<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

@include('block.head')

<body>
  <!-- container section start -->
  <section id="container" class="">
    <!--header start-->

    @include('block.header')

    <!--header end-->

    <!--sidebar start-->
    {{-- @include('block.aside') --}}
    @include('template.blocks.aside')
    <!--sidebar end-->

    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <!-- page start-->
        @yield('main-content')
        <!-- page end-->
      </section>
    </section>
    <!--main content end-->
  </section>
  <!-- container section end -->
  <!-- javascripts -->
  @include('block.scripts')
</body>

</html>