<header class="header dark-bg">
  <div class="toggle-nav">
    <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom">
      <i class="icon_menu"></i>
    </div>
  </div>
  {{--logo start--}}
  <a href="{{route('dashboard')}}" class="logo">Mult<span class="lite">Inventory</span></a> {{--logo end--}}
  <div class="nav search-row" id="top_menu">
    {{-- search form start --}}
    {{-- <ul class="nav top-menu">
      <li>
        <form class="navbar-form">
          <input class="form-control" placeholder="Search" type="text">
        </form>
      </li>
    </ul> --}}
    {{-- search form end --}}
  </div>
  <div class="top-nav notification-row">
    {{-- notificatoin dropdown start--}}
    <ul class="nav pull-right top-menu">
      <li class="dropdown">
        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
          <span class="profile-ava">
            <img alt="" src="{{asset('img/avatar-mini2.jpg')}}">
          </span>
        <span class="username">{{ Auth::user()->user_name}}</span>
          <b class="caret"></b>
        </a>
        <ul class="dropdown-menu extended logout">
          <li class="log-arrow-up"></li>
          <li class="eborder-top">
          <a href="{{route('users.show', Auth::user()->id)}}"><i class="icon_profile"></i> My Profile</a>
          </li>
          <li>
            <a href="{{route('logout')}}" onclick="event.preventDefault();
            document.getElementById('logout-form').submit();"><i class="icon_key_alt"></i> Log Out</a>
            <form id="logout-form" action="{{route('logout')}}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
          </li>
        </ul>
      </li>
      {{-- user login dropdown end --}}
    </ul>
    {{-- notificatoin dropdown end--}}
  </div>
</header>