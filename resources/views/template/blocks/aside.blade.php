<aside>
  <div id="sidebar" class="nav-collapse">
    {{-- sidebar menu start--}}
    <ul class="sidebar-menu">
      <li class="active">
        <a class="" href="{{ route('dashboard') }}">
          <i class="fa fa-tachometer"></i>
          <span>Dashboard</span>
        </a>
      </li>
      <li class="sub-menu">
        <a href="javascript:;" class="">
          <i class="fa fa-users" aria-hidden="true"></i>
          <span>Employees</span>
          <span class="menu-arrow arrow_carrot-right"></span>
        </a>
        <ul class="sub">
          <li><a class="" href="{{ route('employees.create') }}">Create Employee</a></li>
          <li><a class="" href="{{ route('employees.index') }}">List Employees</a></li>
        </ul>
      </li>
      <li class="sub-menu">
        <a href="javascript:;" class="">
          <i class="fa fa-sitemap" aria-hidden="true"></i>
          <span>Departments</span>
          <span class="menu-arrow arrow_carrot-right"></span>
        </a>
        <ul class="sub">
          <li><a class="" href="{{ route('departments.create') }}">Create Department</a></li>
          <li><a class="" href="{{ route('departments.index') }}">List Departments</a></li>
        </ul>
      </li>
      <li class="sub-menu">
        <a href="javascript:;" class="">
          <i class="fa fa-handshake-o" aria-hidden="true"></i>
          <span>Positions</span>
          <span class="menu-arrow arrow_carrot-right"></span>
        </a>
        <ul class="sub">
          <li><a class="" href="{{ route('positions.create') }}">Create Position</a></li>
          <li><a class="" href="{{ route('positions.index') }}">Positions List</a></li>
        </ul>
      </li>
      <li class="sub-menu">
        <a href="javascript:;" class="">
          <i class="fa fa-desktop" aria-hidden="true"></i>
          <span>Articles</span>
          <span class="menu-arrow arrow_carrot-right"></span>
        </a>
        <ul class="sub">
          <li><a class="" href="{{ route('articles.create') }}">Crear Article</a></li>
          <li><a class="" href="{{ route('articles.index') }}">List Articles</a></li>
        </ul>
      </li>
      <li class="sub-menu">
        <a href="javascript:;" class="">
          <i class="fa fa-tachometer" aria-hidden="true"></i>
          <span>Categories</span>
          <span class="menu-arrow arrow_carrot-right"></span>
        </a>
        <ul class="sub">
          <li><a class="" href="{{ route('categories.create') }}">Create Category</a></li>
          <li><a class="" href="{{ route('categories.index') }}">List Categories</a></li>
        </ul>
      </li>
      <li class="sub-menu">
        <a href="javascript:;" class="">
          <i class="fa fa-linode" aria-hidden="true"></i>
          <span>Brands</span>
          <span class="menu-arrow arrow_carrot-right"></span>
        </a>
        <ul class="sub">
          <li><a class="" href="{{ route('brands.create') }}">Create Brand</a></li>
          <li><a class="" href="{{ route('brands.index') }}">List Brands</a></li>
        </ul>
      </li>
      <li class="sub-menu">
        <a href="javascript:;" class="">
          <i class="icon_genius"></i>
          <span>Fixed Assets</span>
          <span class="menu-arrow arrow_carrot-right"></span>
        </a>
        <ul class="sub">
          <li><a class="" href="{{ route('fixed-assets.create') }}">Create Fixed Asset</a></li>
          <li><a class="" href="{{ route('fixed-assets.assign-fixed-asset') }}">Assign Fixed Asset</a></li>
          <li><a class="" href="{{ route('fixed-assets.index') }}">List Fixed Assets</a></li>
        </ul>
      </li>
      <li class="sub-menu">
        <a href="javascript:;" class="" title="Delivery Certificate">
          <i class="icon_documents_alt"></i>
          <span>Delivery Certif</span>
          <span class="menu-arrow arrow_carrot-right"></span>
        </a>
        <ul class="sub">
          <li><a class="" href="{{ route('delivery-certificates.create') }}" title="Create Delivery Certificate">Create
              Delivery Certif</a></li>
          <li><a class="" href="{{ route('delivery-certificates.index') }}">List Orders</a></li>
        </ul>
      </li>
      <li class="sub-menu">
        <a href="javascript:;" class="">
          <i class="icon_tools"></i>
          <span>Maintenances</span>
          <span class="menu-arrow arrow_carrot-right"></span>
        </a>
        <ul class="sub">
          <li><a class="" href="{{ route('maintenances.create') }}">Create Maintenance</a></li>
          <li><a class="" href="{{ route('maintenances.index') }}">Listar Ordernes</a></li>
        </ul>
      </li>
      <li class="sub-menu">
        <a href="javascript:;" class="" title="Delivery Certificate">
          <i class="fa fa-archive" aria-hidden="true"></i>
          <span>Storage Cellar</span>
          <span class="menu-arrow arrow_carrot-right"></span>
        </a>
        <ul class="sub">
          <li><a class="" href="{{ route('storage-cellars.create') }}" title="Create Delivery Certificate">Create
              Delivery Certif</a></li>
          <li><a class="" href="{{ route('storage-cellars.index') }}">List Orders</a></li>
        </ul>
      </li>
      <li class="sub-menu">
        <a href="javascript:;" class="">
          <i class="icon_group"></i>
          <span>Users</span>
          <span class="menu-arrow arrow_carrot-right"></span>
        </a>
        <ul class="sub">
          <li><a class="" href="{{ route('users.create') }}">Create User</a></li>
          <li><a class="" href="{{ route('users.index') }}">List Users</a></li>
        </ul>
      </li>
      <li class="sub-menu">
        <a href="javascript:;" class="">
          <i class="fa fa-address-book-o" aria-hidden="true"></i>
          <span>Roles</span>
          <span class="menu-arrow arrow_carrot-right"></span>
        </a>
        <ul class="sub">
          <li><a class="" href="{{ route('roles.create') }}">Create Relo</a></li>
          <li><a class="" href="{{ route('roles.index') }}">List Relos</a></li>
        </ul>
      </li>
      <li class="sub-menu">
        <a href="javascript:;" class="">
          <i class="fa fa-list-alt" aria-hidden="true"></i>
          <span>Status</span>
          <span class="menu-arrow arrow_carrot-right"></span>
        </a>
        <ul class="sub">
          <li><a class="" href="{{ route('status.create') }}">Create Status</a></li>
          <li><a class="" href="{{ route('status.index') }}">List Status</a></li>
        </ul>
      </li>
    </ul>
    {{-- sidebar menu end--}}
  </div>
</aside>