<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    {{--head start--}}
    @include('template.blocks.head')
    {{--end head--}}
<body class="login-img3-body">
    <section class="container">
        @if (session()->has('message'))
            <div class="alert alert-info">{{session('message')}}</div>
        @endif
        @yield('form-login')
    </section>
</body>
</html>