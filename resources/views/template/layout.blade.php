<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
{{--head start--}}
@include('template.blocks.head')
{{--end head--}}
<body>
 {{-- container section start --}}
 <section id="container">
   {{-- header start --}}
   {{-- @include('template.blocks.header') --}}
   @include('block.header')
   {{-- header end --}}

   {{--sidebar start--}}
   {{-- @include('template.blocks.aside') --}}
   @include('block.aside')
   {{--sidebar end--}}

   {{--main content start--}}
   <section id="main-content">
     {{-- <div id="container"> --}}
       @yield('main-content')
     {{-- </div> --}}
      @yield('content')
    </section>
   {{--main content end--}}
</section>
{{-- container section end --}}
{{-- javascripts start--}}
@include('template.blocks.scripts')
@yield('js')
{{-- javascripts end--}}
</body>
</html>
