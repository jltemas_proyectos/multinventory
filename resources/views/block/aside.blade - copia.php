<aside>
  <div id="sidebar" class="nav-collapse ">
    {{-- sidebar menu start--}}
    <ul class="sidebar-menu">
      <li class="">
        <a class="" href="">
          <i class="fa fa-dashboard"></i>
          <span>Inicio</span>
        </a>
      </li>
      <li class="sub-menu">
        <a class="" href="javascript:;">
          <i class="icon_flowchart"></i>
          <span>Áreas</span>
          <span class="menu-arrow arrow_carrot-right"></span>
        </a>
        <ul class="sub">
          <li><a class="" href=""><i class="fa fa-cubes"></i>Plasticos</a></li>
          <li><a class="" href="login.html"><i class="fa fa-bus"></i>Automotor</a></li>
          <li><a class="" href="contact.html"><i class="fa fa-motorcycle"></i>Motos</a></li>
          <li><a class="active" href="blank.html"><i class="fa fa-linode"></i>Troquelados</a></li>
          <li><a class="" href="404.html"><i class="fa fa-cogs"></i>Rodachines</a></li>
          <li><a class="" href="404.html"><i class="fa fa-object-group"></i>Metolizados</a></li>
          <li><a class="" href="404.html"><i class="fa fa-flask"></i>Acabados</a></li>
          <li><a class="" href="404.html"><i class="fa fa-industry"></i>Vulcanizados</a></li>
        </ul>
      </li>
      <li class="sub-menu">
        <a href="javascript:;" class="">
          <i class="icon_documents"></i>
          <span>Ordenes</span>
          <span class="menu-arrow arrow_carrot-right"></span>
        </a>
        <ul class="sub">
          <ul class="sub">
            <li><a class="" href="profile.html"><i class="fa fa-cubes"></i>Plasticos</a></li>
            <li><a class="" href="login.html"><i class="fa fa-bus"></i>Automotor</a></li>
            <li><a class="" href="contact.html"><i class="fa fa-motorcycle"></i>Motos</a></li>
            <li><a class="active" href="blank.html"><i class="fa fa-linode"></i>Troquelados</a></li>
            <li><a class="" href="404.html"><i class="fa fa-cogs"></i>Rodachines</a></li>
            <li><a class="" href="404.html"><i class="fa fa-object-group"></i>Metolizados</a></li>
            <li><a class="" href="404.html"><i class="fa fa-flask"></i>Acabados</a></li>
            <li><a class="" href="404.html"><i class="fa fa-industry"></i>Vulcanizados</a></li>
          </ul>
        </ul>
      </li>
      <li class="sub-menu ">
        <a href="javascript:;" class="">
          <i class="icon_tools"></i>
          <span>Parada Maquinas</span>
          <span class="menu-arrow arrow_carrot-right"></span>
        </a>
        <ul class="sub">
          <ul class="sub">
            <li><a class="" href="profile.html"><i class="fa fa-cubes"></i>Plasticos</a></li>
            <li><a class="" href="login.html"><i class="fa fa-bus"></i>Automotor</a></li>
            <li><a class="" href="contact.html"><i class="fa fa-motorcycle"></i>Motos</a></li>
            <li><a class="active" href="blank.html"><i class="fa fa-linode"></i>Troquelados</a></li>
            <li><a class="" href="404.html"><i class="fa fa-cogs"></i>Rodachines</a></li>
            <li><a class="" href="404.html"><i class="fa fa-object-group"></i>Metolizados</a></li>
            <li><a class="" href="404.html"><i class="fa fa-flask"></i>Acabados</a></li>
            <li><a class="" href="404.html"><i class="fa fa-industry"></i>Vulcanizados</a></li>
          </ul>
        </ul>
      </li>
      <li>
        <a class="" href="chart-chartjs.html">
          <i class="icon_piechart"></i>
          <span>Informes</span>
        </a>
      </li>
    </ul>
    {{-- sidebar menu end--}}
  </div>
</aside>