<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
  {{-- <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal"> --}}
  {{-- CSRF Token --}}
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>{{ config('app.name', 'Multinventory') }}</title>
  {{-- Favicon --}}
  <link rel="shortcut icon" href="{{ asset('img/favicon.png')}}">
  {{-- Bootstrap CSS --}}
  <link href="{{ asset('css/app.css')}}" rel="stylesheet">

  {{--external css start--}}

  {{-- font icon --}}
  <link href="{{ asset('css/elegant-icons-style.css')}}" rel="stylesheet">
  <link href="{{ asset('css/font-awesome.min.css')}}" rel="stylesheet">

  {{--external css end--}}

  {{-- Custom styles --}}
  <link href="{{ asset('css/style.css')}}" rel="stylesheet">
  <link href="{{ asset('css/style-responsive.css')}}" rel="stylesheet">
  @yield('custom_css')
  {{-- My Styles --}}
  <link rel="stylesheet" href="{{ asset('css/mystyle.css')}}">

  {{-- HTML5 shim and Respond.js IE8 support of HTML5 --}}
  {{--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <script src="js/lte-ie7.js"></script>
      <![endif]--}}
</head>