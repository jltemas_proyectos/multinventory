<!DOCTYPE html>
<html lang="en">
@include('template.blocks.head')
  <body>
   <div class="page-404">
    <p class="text-404">404</p>
    <h2>Aww Snap!</h2>
    <p>Something went wrong or that page doesn’t exist yet. <br><a href="{{ route('home') }}">Return Home</a></p>
   </div>
  </body>
</html>
