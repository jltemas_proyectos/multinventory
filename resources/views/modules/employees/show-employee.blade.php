@extends('template')
@section('main-content')
{{--overview start--}}
<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header"><i class="fa fa-users" aria-hidden="true"></i>Employee Profile</h3>
    <ol class="breadcrumb">
      <li><i class="fa fa-home" aria-hidden="true"></i><a href="{{ route('dashboard') }}">Dashboard</a></li>
      <li><i class="fa fa-users" aria-hidden="true"></i><a href="{{ route('employees.index') }}">Employees</a></li>
      <li><i class="fa fa-user" aria-hidden="true"></i>Show Employee</li>
    </ol>
  </div>
</div>
{{--overview end--}}
{{--main content start--}}
<div class="row">
  <div class="col-lg-12">
    {{-- profile-widget start--}}
    <div class="row">
      <div class="col-lg-12">
        <div class="profile-widget profile-widget-info">
          <div class="panel-body">
            <div class="col-lg-2 col-sm-2">
              <h4>{{ $employee->first_name.' '.$employee->first_lastname }}</h4>
              <div class="follow-ava">
                <img src='{{ asset("storage/$employee->image")}}'
                  alt='{{ "$employee->first_name $employee->first_lastname"}}' class="img-rounded img-responsive">
                </div>
                <h6>{{ $employee->position->name }}</h6>
            </div>
            <div class="col-lg-4 col-sm-4 follow-info">
              <p class="show-employee"><i class="fa fa-sitemap"> {{ $employee->department->name }}</i></p>
              <p class="show-employee"><i class="fa fa-handshake-o"> {{ $employee->position->name }}</i></p>
              <p class="show-employee"><i class="fa fa-envelope"> {{ $employee->user->email }}</i></p>
              <p class="show-employee"><i class="fa fa-phone"> {{ $employee->phone }}</i></p>
            </div>
            <div class="col-lg-2 col-sm-6 follow-info weather-category">
              <ul>
                <li class="active">
                  <span class="block sub-title-show-article">Fixed Assets</span>
                  <i class="block icon_genius fa-2x"></i>
                  <span>{{$employee->fixedassets->count() ? $employee->fixedassets->count() : 'No Asignado'}}</span>
                </li>
              </ul>
            </div>
            <div class="col-lg-2 col-sm-6 follow-info weather-category">
              <ul>
                <li class="active">
                  <span class="block sub-title-show-article">Delivery Certificate</span>
                  <i class="block icon_documents_alt fa-2x"></i>
                  <span>{{$employee->deliveries->count() ? $employee->deliveries->count() : 'No Asignado'}}</span>
                </li>
              </ul>
            </div>
            <div class="col-lg-2 col-sm-6 follow-info weather-category">
              <ul>
                <li class="active">
                  <span class="block sub-title-show-article">User</span>
                  <i class="block fa fa-user-circle fa-2x"></i>
                  {{ $employee->user->user_name }}
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    {{-- profile-widget end--}}
    {{-- page start --}}
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          {{--Start Tab headers menu --}}
          <header class="panel-heading tab-bg-info" id="none-border">
            <ul class="nav nav-tabs">
              @if ($errors->all() || session()->has('status'))
              <li>
                <a data-toggle="tab" href="#delivery-certificate">
                  <i class="icon-home"></i>
                  Delivery Certificate
                </a>
              </li>
              <li>
                <a data-toggle="tab" href="#profile">
                  <i class="icon-user"></i>
                  Profile
                </a>
              </li>
              <li class="active">
                <a data-toggle="tab" href="#edit-profile">
                  <i class="icon-envelope"></i>
                  Edit Profile
                </a>
              </li>
              @else
              <li class="active">
                <a data-toggle="tab" href="#delivery-certificate">
                  <i class="icon-home"></i>
                  Delivery Certificate
                </a>
              </li>
              <li>
                <a data-toggle="tab" href="#profile">
                  <i class="icon-user"></i>
                  Profile
                </a>
              </li>
              <li>
                <a data-toggle="tab" href="#edit-profile">
                  <i class="icon-envelope"></i>
                  Edit Profile
                </a>
              </li>
              @endif
            </ul>
          </header>
          {{--End Tab headers menu --}}
          {{-- Start Tab Body --}}
          <div class="panel-body">
            <div class="tab-content">
              @if ($errors->all() || session()->has('status'))
              {{-- Start Tab delivery-certificate --}}
              <div id="delivery-certificate" class="tab-pane ">
                <div class="profile-activity">
                  @foreach ($employee->deliveries as $delivery)
                  <div class="act-time">
                    <div class="activity-body act-in">
                      <div class="text">
                        <i class="activity-img icon_documents_alt fa-2x"></i>
                        <p class="attribution">
                          <a href="{{ route('delivery-certificates.show',$delivery->id) }}">{{ $delivery->code}}</a>
                          {{ $delivery->created_at }}
                        </p>
                        <p><span>Delivered By:
                          </span>{{ $delivery->user->employee->first_name.' '.$delivery->user->employee->first_lastname }}
                        </p>
                      </div>
                    </div>
                  </div>
                  @endforeach
                </div>
              </div>
              {{-- End Tab delivery-certificate --}}
              {{-- Start Tab profile --}}
              <div id="profile" class="tab-pane">
                <section class="panel">
                  <div class="bio-graph-heading">
                    <p>Multipartes De Colombia S.A.S</p>
                    <p>Department De {{ $employee->department->name }}</p>
                  </div>
                  <div class="panel-body bio-graph-info">
                    <h1>Employee Info</h1>
                    <div class="row">
                      <div class="col-sm-2">
                        <div class="form-group">
                          <p><span> Names </span>: {{ $employee->first_name .' '.$employee->second_name.' ' }} </p>
                        </div>
                        <div class="form-group">
                          <p><span>Last Names </span>:
                            {{ $employee->first_lastname .' '.$employee->second_lastname.' ' }}</p>
                        </div>
                        <div class="form-group">
                          <p><span>Cedula </span>: {{ $employee->cedula }}</p>
                        </div>
                        <div class="form-group">
                          <p><span>User name </span>: {{ $employee->user->user_name }}</p>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <p><span>Department </span>: {{ $employee->department->name }}</p>
                        </div>
                        <div class="form-group">
                          <p><span>Position</span>: {{ $employee->position->name }}</p>
                        </div>
                        <div class="form-group">
                          <p><span>Email </span>: {{ $employee->user->email }}</p>
                        </div>
                        <div class="form-group">
                          <p><span>Phone </span>: {{ $employee->phone }}</p>
                        </div>

                      </div>
                    </div>
                  </div>
                </section>
              </div>
              {{-- End Tab profile --}}
              {{-- Start Tab edit-profile --}}
              <div id="edit-profile" class="tab-pane active">
                <section class="panel">
                  <div class="panel-body bio-graph-info">
                    <h1>Profile Info </h1>
                    <div class="form-horizontal">
                      <form class="form-validate form-horizontal" id="register_form" method="POST"
                        action="{{ route('employees.update', $employee->id) }}" enctype="multipart/form-data">
                        @if (session()->has('status'))
                        <div class="alert alert-success col-lg-offset-2">
                          {{ session('status') }}
                        </div>
                        @endif
                        <div class="form-group">
                          <label for="image" class="control-label col-lg-2">Image <span
                              class="required">*</span></label>
                          <div class="col-lg-4">
                            @if ($errors->has('image'))
                            <div class="alert alert-danger" role="alert">
                              {{ $errors->first('image') }}
                            </div>
                            @endif
                            <input class="control-label" id="image" name="image" type="file"
                              value="{{ old('image') ?? $employee->image}} " enctype="multipar/form-data" />
                          </div>
                          {{--  Esta clase col-lg-offset-2 de bootstrap es la que le esta dando el espaciado a la imagen  --}}
                          <div class="col-lg-3 col-lg-offset-2">
                            <img src='{{ asset("storage/$employee->image")}}'
                              alt='{{ "$employee->first_name $employee->first_lastname"}}'
                              class="img-rounded img-responsive">
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="first_name" class="control-label col-lg-2">First name <span
                              class="required">*</span></label>
                          <div class="col-lg-4">
                            @if ($errors->has('first_name'))
                            <div class="alert alert-danger">
                              {{ $errors->first('first_name') }}
                            </div>
                            @endif
                            <input class="form-control " id="first_name" name="first_name" type="text"
                              value="{{ old('first_name') ?? $employee->first_name}}" />
                          </div>
                          <label for="second_name" class="control-label col-lg-2">Second name</label>
                          <div class="col-lg-4">
                            @if ($errors->has('second_name'))
                            <div class="alert alert-danger">
                              {{ $errors->first('second_name') }}
                            </div>
                            @endif
                            <input class="form-control " id="second_name" name="second_name" type="text"
                              value="{{ old('second_name') ?? $employee->second_name}}"
                              aria-describedby="basic-addon1" />
                          </div>
                        </div>
                        <div class="form-group ">
                          <label for="first_lastname" class="control-label col-lg-2">First lastname <span
                              class="required">*</span></label>
                          <div class="col-lg-4">
                            @if ($errors->has('first_lastname'))
                            <div class="alert alert-danger">
                              {{ $errors->first('first_lastname') }}
                            </div>
                            @endif
                            <input class="form-control " id="first_lastname" name="first_lastname" type="text"
                              value="{{ old('first_lastname') ?? $employee->first_lastname}}" />
                          </div>
                          <label for="second_lastname" class="control-label col-lg-2">Second lastname <span
                              class="required">*</span></label>
                          <div class="col-lg-4">
                            @if ($errors->has('second_lastname'))
                            <div class="alert alert-danger">
                              {{ $errors->first('second_lastname') }}
                            </div>
                            @endif
                            <input class="form-control " id="second_lastname" name="second_lastname" type="text"
                              value="{{ old('second_lastname') ?? $employee->second_lastname }}" />
                          </div>
                        </div>
                        <div class="form-group ">
                          <label for="cedula" class="control-label col-lg-2">Cedula <span
                              class="required">*</span></label>
                          <div class="col-lg-4">
                            @if ($errors->has('cedula'))
                            <div class="alert alert-danger">
                              {{ $errors->first('cedula') }}
                            </div>
                            @endif
                            <input class="form-control " id="cedula" name="cedula" type="text"
                              value="{{ old('cedula') ?? $employee->cedula }}" />
                          </div>
                          <label for="phone" class="control-label col-lg-2">Phone <span
                              class="required">*</span></label>
                          <div class="col-lg-4">
                            @if ($errors->has('phone'))
                            <div class="alert alert-danger">
                              {{ $errors->first('phone') }}
                            </div>
                            @endif
                            <input class="form-control " id="phone" name="phone" type="text"
                              value="{{ old('phone') ?? $employee->phone }}" />
                          </div>
                        </div>
                        <div class="form-group ">
                          <label for="department_id" class="control-label col-lg-2">Department <span
                              class="required">*</span></label>
                          <div class="col-lg-4">
                            @if ($errors->has('department_id'))
                            <div class="alert alert-danger">
                              {{ $errors->first('department_id') }}
                            </div>
                            @endif
                            <select class="form-control" id="department_id" name="department_id">
                              @foreach ($departments as $department)
                              @if ($employee->department->id === $department->id)
                              <option value="{{ $department->id }}" selected>{{ $department->name }}</option>
                              @continue
                              @endif
                              <option value="{{ $department->id }}">{{ $department->name }}</option>
                              @endforeach
                            </select>
                          </div>
                          <label for="position_id" class="control-label col-lg-2">Position <span
                              class="required">*</span></label>
                          <div class="col-lg-4">
                            @if ($errors->has('position_id'))
                            <div class="alert alert-danger">
                              {{ $errors->first('position_id') }}
                            </div>
                            @endif
                            <select class="form-control " id="position_id" name="position_id">
                              @foreach ($positions as $position)
                              @if ($employee->position->id === $position->id)
                              <option value="{{ $position->id }}" selected>{{ $position->name }}</option>
                              @continue
                              @endif
                              <option value="{{ $position->id }}">{{ $position->name }}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="user_name" class="control-label col-lg-2">User name<span
                              class="required">*</span></label>
                          <div class="col-lg-4">
                            @if ($errors->has('user_name'))
                            <div class="alert alert-danger">
                              {{ $errors->first('user_name') }}
                            </div>
                            @endif
                            <input class="form-control " id="user_name" name="user_name" type="text"
                              value="{{ old('user_name') ?? $employee->user->user_name}}" readonly />
                          </div>
                          <label for="email" class="control-label col-lg-2">Email</label>
                          <div class="col-lg-4">
                            @if ($errors->has('email'))
                            <div class="alert alert-danger">
                              {{ $errors->first('email') }}
                            </div>
                            @endif
                            <div class="input-group input-group-md">
                              <span class="input-group-addon">@</span>
                              <input class="form-control " id="email" name="email" type="email"
                                value="{{ old('email') ?? $employee->user->email}}" aria-describedby="basic-addon1"
                                readonly />
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-lg-offset-2 col-lg-10">
                            <button class="btn btn-primary" type="submit">Update</button>
                            <a class="btn btn-danger" href="{{ route('employees.index') }}">Cancel</a>
                          </div>
                        </div>
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                      </form>
                    </div>
                  </div>
                </section>
              </div>
              {{-- End Tab edit-profile --}}
              @else
              {{-- Start Tab delivery-certificate --}}
              <div id="delivery-certificate" class="tab-pane active">
                <div class="profile-activity">
                  @foreach ($employee->deliveries as $delivery)
                  <div class="act-time">
                    <div class="activity-body act-in">
                      <div class="text">
                        <i class="activity-img icon_documents_alt fa-2x"></i>
                        <p class="attribution">
                          <a href="{{ route('delivery-certificates.show',$delivery->id) }}">{{ $delivery->code}}</a>
                          {{ $delivery->created_at }}
                        </p>
                        <p><span>Delivered By:
                          </span>{{ $delivery->user->employee->first_name.' '.$delivery->user->employee->first_lastname }}
                        </p>
                      </div>
                    </div>
                  </div>
                  @endforeach
                </div>
              </div>
              {{-- End Tab delivery-certificate --}}
              {{-- Start Tab profile --}}
              <div id="profile" class="tab-pane">
                <section class="panel">
                  <div class="bio-graph-heading">
                    <p>Multipartes De Colombia S.A.S</p>
                    <p>Department De {{ $employee->department->name }}</p>
                  </div>
                  <div class="panel-body bio-graph-info">
                    <h1>Employee Info</h1>
                    <div class="row">
                      <div class="col-sm-2">
                        <div class="form-group">
                          <p><span> Names </span>: {{ $employee->first_name .' '.$employee->second_name.' ' }} </p>
                        </div>
                        <div class="form-group">
                          <p><span>Last Names </span>:
                            {{ $employee->first_lastname .' '.$employee->second_lastname.' ' }}</p>
                        </div>
                        <div class="form-group">
                          <p><span>Cedula </span>: {{ $employee->cedula }}</p>
                        </div>
                        <div class="form-group">
                          <p><span>User name </span>: {{ $employee->user->user_name }}</p>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <p><span>Department </span>: {{ $employee->department->name }}</p>
                        </div>
                        <div class="form-group">
                          <p><span>Position</span>: {{ $employee->position->name }}</p>
                        </div>
                        <div class="form-group">
                          <p><span>Email </span>: {{ $employee->user->email }}</p>
                        </div>
                        <div class="form-group">
                          <p><span>Phone </span>: {{ $employee->phone }}</p>
                        </div>

                      </div>
                    </div>
                  </div>
                </section>
              </div>
              {{-- End Tab profile --}}
              {{-- Start Tab edit-profile --}}
              <div id="edit-profile" class="tab-pane">
                <section class="panel">
                  <div class="panel-body bio-graph-info">
                    <div class="row">
                      <h1 class>Profile Info</h1>
                    </div>
                    <div class="form">
                      <form class="form-validate form-horizontal" id="register_form" method="POST"
                        action="{{ route('employees.update', $employee->id) }}" enctype="multipart/form-data">
                        @if (session()->has('status'))
                        <div class="alert alert-success col-lg-offset-2">
                          {{ session('status') }}
                        </div>
                        @endif
                        <div class="form-group">
                          <label for="image" class="control-label col-lg-2">Image <span
                              class="required">*</span></label>
                          <div class="col-lg-4">
                            @if ($errors->has('image'))
                            <div class="alert alert-danger" role="alert">
                              {{ $errors->first('image') }}
                            </div>
                            @endif
                            <input class="control-label" id="image" name="image" type="file"
                              value="{{ old('image') ?? $employee->image}} " enctype="multipar/form-data" />
                          </div>
                          {{--  Esta clase col-lg-offset-2 de bootstrap es la que le esta dando el espaciado a la imagen  --}}
                          <div class="col-lg-3 col-lg-offset-2">
                            <img src='{{ asset("storage/$employee->image")}}'
                              alt='{{ "$employee->first_name $employee->first_lastname"}}'
                              class="img-rounded img-responsive">
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="first_name" class="control-label col-lg-2">First name <span
                              class="required">*</span></label>
                          <div class="col-lg-4">
                            @if ($errors->has('first_name'))
                            <div class="alert alert-danger">
                              {{ $errors->first('first_name') }}
                            </div>
                            @endif
                            <input class="form-control " id="first_name" name="first_name" type="text"
                              value="{{ old('first_name') ?? $employee->first_name}}" />
                          </div>
                          <label for="second_name" class="control-label col-lg-2">Second name</label>
                          <div class="col-lg-4">
                            @if ($errors->has('second_name'))
                            <div class="alert alert-danger">
                              {{ $errors->first('second_name') }}
                            </div>
                            @endif
                            <input class="form-control " id="second_name" name="second_name" type="text"
                              value="{{ old('second_name') ?? $employee->second_name}}"
                              aria-describedby="basic-addon1" />
                          </div>
                        </div>
                        <div class="form-group ">
                          <label for="first_lastname" class="control-label col-lg-2">First lastname <span
                              class="required">*</span></label>
                          <div class="col-lg-4">
                            @if ($errors->has('first_lastname'))
                            <div class="alert alert-danger">
                              {{ $errors->first('first_lastname') }}
                            </div>
                            @endif
                            <input class="form-control " id="first_lastname" name="first_lastname" type="text"
                              value="{{ old('first_lastname') ?? $employee->first_lastname}}" />
                          </div>
                          <label for="second_lastname" class="control-label col-lg-2">Second lastname <span
                              class="required">*</span></label>
                          <div class="col-lg-4">
                            @if ($errors->has('second_lastname'))
                            <div class="alert alert-danger">
                              {{ $errors->first('second_lastname') }}
                            </div>
                            @endif
                            <input class="form-control " id="second_lastname" name="second_lastname" type="text"
                              value="{{ old('second_lastname') ?? $employee->second_lastname }}" />
                          </div>
                        </div>
                        <div class="form-group ">
                          <label for="cedula" class="control-label col-lg-2">Cedula <span
                              class="required">*</span></label>
                          <div class="col-lg-4">
                            @if ($errors->has('cedula'))
                            <div class="alert alert-danger">
                              {{ $errors->first('cedula') }}
                            </div>
                            @endif
                            <input class="form-control " id="cedula" name="cedula" type="text"
                              value="{{ old('cedula') ?? $employee->cedula }}" />
                          </div>
                          <label for="phone" class="control-label col-lg-2">Phone <span
                              class="required">*</span></label>
                          <div class="col-lg-4">
                            @if ($errors->has('phone'))
                            <div class="alert alert-danger">
                              {{ $errors->first('phone') }}
                            </div>
                            @endif
                            <input class="form-control " id="phone" name="phone" type="text"
                              value="{{ old('phone') ?? $employee->phone }}" />
                          </div>
                        </div>
                        <div class="form-group ">
                          <label for="department_id" class="control-label col-lg-2">Department <span
                              class="required">*</span></label>
                          <div class="col-lg-4">
                            @if ($errors->has('department_id'))
                            <div class="alert alert-danger">
                              {{ $errors->first('department_id') }}
                            </div>
                            @endif
                            <select class="form-control" id="department_id" name="department_id">
                              @foreach ($departments as $department)
                              @if ($employee->department->id === $department->id)
                              <option value="{{ $department->id }}" selected>{{ $department->name }}</option>
                              @continue
                              @endif
                              <option value="{{ $department->id }}">{{ $department->name }}</option>
                              @endforeach
                            </select>
                          </div>
                          <label for="position_id" class="control-label col-lg-2">Position <span
                              class="required">*</span></label>
                          <div class="col-lg-4">
                            @if ($errors->has('position_id'))
                            <div class="alert alert-danger">
                              {{ $errors->first('position_id') }}
                            </div>
                            @endif
                            <select class="form-control " id="position_id" name="position_id">
                              @foreach ($positions as $position)
                              @if ($employee->position->id === $position->id)
                              <option value="{{ $position->id }}" selected>{{ $position->name }}</option>
                              @continue
                              @endif
                              <option value="{{ $position->id }}">{{ $position->name }}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="user_name" class="control-label col-lg-2">User name<span
                              class="required">*</span></label>
                          <div class="col-lg-4">
                            @if ($errors->has('user_name'))
                            <div class="alert alert-danger">
                              {{ $errors->first('user_name') }}
                            </div>
                            @endif
                            <input class="form-control " id="user_name" name="user_name" type="text"
                              value="{{ old('user_name') ?? $employee->user->user_name}}" readonly />
                          </div>
                          <label for="email" class="control-label col-lg-2">Email</label>
                          <div class="col-lg-4">
                            @if ($errors->has('email'))
                            <div class="alert alert-danger">
                              {{ $errors->first('email') }}
                            </div>
                            @endif
                            <div class="input-group input-group-md">
                              <span class="input-group-addon">@</span>
                              <input class="form-control " id="email" name="email" type="email"
                                value="{{ old('email') ?? $employee->user->email}}" aria-describedby="basic-addon1"
                                readonly />
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-lg-offset-2 col-lg-10">
                            <button class="btn btn-primary" type="submit">Update</button>
                            <a class="btn btn-danger" href="{{ route('employees.index') }}">Cancel</a>
                          </div>
                        </div>
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                      </form>
                    </div>
                  </div>
                </section>
              </div>
              {{-- End Tab edit-profile --}}
              @endif
            </div>
          </div>
          {{-- End Tab Body --}}
        </section>
      </div>
    </div>
    {{-- page end --}}
  </div>
</div>
{{--main content end--}}
@endsection