@extends('template')
@section('main-content')
{{--overview start--}}
<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header"><i class="fa fa-users"></i>Employees Module</h3>
    <ol class="breadcrumb">
      <li><i class="fa fa-home"></i><a href="{{ route('dashboard') }}">Dashboard</a></li>
      <li><i class="fa fa-users"></i><a href="{{ route('employees.index') }}">Employees</a></li>
      <li><i class="fa fa-user"></i>Update Employee</li>
    </ol>
  </div>
</div>
{{--overview end--}}
{{--main content start--}}
<div class="row">
  <div class="col-lg-12">
    <section class="panel">
      <header class="panel-heading">
        <h3>Employee Update Form</h3>
      </header>
      <div class="panel-body">
        <div class="form">
          <form class="form-validate form-horizontal " id="register_form" method="POST"
            action="{{ route('employees.update', $employee->id)}}" enctype="multipart/form-data">
            @if (session('status'))
            <div class="alert alert-success col-lg-offset-2" role="alert">
              {{ session('status') }}
            </div>
            @endif
            <div class="form-group">
              <label for="image" class="control-label col-lg-2">Image <span class="required">*</span></label>
              <div class="col-lg-4">
                @if ($errors->has('image'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first('image') }}
                </div>
                @endif
                <input class="input-file" id="image" name="image" type="file"
                  value="{{ old('image') ?? $employee->image}} " enctype="multipar/form-data" />
              </div>
              {{--  Esta clase col-lg-offset-2 de bootstrap es la que le esta dando el espaciado a la imagen  --}}
              <div class="col-lg-3 col-lg-offset-2">
                <img src='{{ url("storage/$employee->image") }}'
                  alt='{{"$employee->first_name $employee->first_lastname"}}' class="img-rounded img-responsive">
              </div>
            </div>
            <div class="form-group">
              <label for="first_name" class="control-label col-lg-2">First name <span class="required">*</span></label>
              <div class="col-lg-4">
                @if ($errors->has('first_name'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first('first_name') }}
                </div>
                @endif
                <input class="form-control" id="first_name" name="first_name" type="text"
                  value="{{ old('first_name') ?? $employee->first_name }}" required />
              </div>
              <label for="second_name" class="control-label col-lg-2">Second name</label>
              <div class="col-lg-4">
                @if ($errors->has('second_name'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first('second_name') }}
                </div>
                @endif
                <input class="form-control" id="second_name" name="second_name" type="text"
                  value="{{ old('second_name') ?? $employee->second_name}}" />
              </div>
            </div>
            <div class="form-group ">
              <label for="first_lastname" class="control-label col-lg-2">First lastname <span
                  class="required">*</span></label>
              <div class="col-lg-4">
                @if ($errors->has('first_lastname'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first('first_lastname') }}
                </div>
                @endif
                <input class="form-control" id="first_lastname" name="first_lastname" type="text"
                  value="{{ old('first_lastname') ?? $employee->first_lastname }}" required />
              </div>
              <label for="second_lastname" class="control-label col-lg-2">Second lastname <span
                  class="required">*</span></label>
              <div class="col-lg-4">
                @if ($errors->has('second_lastname'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first('second_lastname') }}
                </div>
                @endif
                <input class="form-control" id="second_lastname" name="second_lastname" type="text"
                  value="{{ old('second_lastname') ?? $employee->second_lastname }}" required />
              </div>
            </div>
            <div class="form-group ">
              <label for="cedula" class="control-label col-lg-2">Cedula <span class="required">*</span></label>
              <div class="col-lg-4">
                @if ($errors->has('cedula'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first('cedula') }}
                </div>
                @endif
                <input class="form-control" id="cedula" name="cedula" type="text"
                  value="{{ old('cedula') ?? $employee->cedula }}" required />
              </div>
              <label for="phone" class="control-label col-lg-2">Phone <span class="required">*</span></label>
              <div class="col-lg-4">
                @if ($errors->has('phone'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first('phone') }}
                </div>
                @endif
                <input class="form-control" id="phone" name="phone" type="text"
                  value="{{ old('phone') ?? $employee->phone }}" required />
              </div>
            </div>
            <div class="form-group ">
              <label for="department_id" class="control-label col-lg-2">Department <span
                  class="required">*</span></label>
              <div class="col-lg-4">
                @if ($errors->has('department_id'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first('department_id') }}
                </div>
                @endif
                <select name="department_id" id="department_id" class="form-control" required>
                  <option></option>
                  @foreach ($departments as $department)
                  @if ($department->name === $employee->department->name)
                  <option value="{{ $department->id }}" selected>{{ $department->name }}</option>
                  @continue
                  @endif
                  <option value="{{ $department->id }}">{{ $department->name }}</option>
                  @endforeach
                </select>
              </div>
              <label for="position_id" class="control-label col-lg-2">Position <span class="required">*</span></label>
              <div class="col-lg-4">
                @if ($errors->has('position_id'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first('position_id') }}
                </div>
                @endif
                <select name="position_id" id="position_id" class="form-control" required>
                  <option></option>
                  @foreach ($positions as $position)
                  @if ($position->name === $employee->position->name)
                  <option value="{{ $position->id }}" selected>{{ $position->name }}</option>
                  @continue
                  @endif
                  <option value="{{ $position->id }}">{{ $position->name }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group ">
              <label for="user_name" class="control-label col-lg-2">User name</label>
              <div class="col-lg-4">
                @if ($errors->has('user_name'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first('user_name') }}
                </div>
                @endif
                <input class="form-control" id="user_name" name="user_name" type="text"
                  value="{{ old('user_name') ?? $employee->user->user_name }}" readonly required />
              </div>
              <label for="email" class="control-label col-lg-2">email</label>
              <div class="col-lg-4">
                @if ($errors->has('email'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first('email') }}
                </div>
                @endif
                <div class="input-group input-group-md">
                  <span class="input-group-addon">@</span>
                  <input class="form-control" id="email" name="email" type="email"
                    value="{{ old('email') ?? $employee->user->email}}" readonly />
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="col-lg-offset-2 col-lg-10">
                <button class="btn btn-primary" type="submit">Save</button>
                <a class="btn btn-danger" href="{{ route('employees.index') }}">Cancel</a>
              </div>
            </div>
            {{ csrf_field() }}
            {{ method_field('PUT') }}
          </form>
        </div>
      </div>
    </section>
  </div>
</div>
{{--main content end--}}
@endsection