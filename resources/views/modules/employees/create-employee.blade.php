@extends('template')
@section('main-content')
{{--overview start--}}
<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header"><i class="fa fa-users"></i>Employees Module</h3>
    <ol class="breadcrumb">
      <li><i class="fa fa-tachometer"></i><a href="{{ route('dashboard') }}">Dashboard</a></li>
      <li><i class="fa fa-users"></i><a href="{{ route('employees.index') }}">Employees</a></li>
      <li><i class="fa fa-user"></i>Create Employee</li>
    </ol>
  </div>
</div>
{{--overview end--}}
{{--main content start--}}
<div class="row">
  <div class="col-lg-12">
    <section class="panel">
      <header class="panel-heading">
        <h3>Employee Registration Form</h3>
      </header>
      <div class="panel-body">
        <div class="form">
          <form class="form-validate form-horizontal" id="register_form" method="POST"
            action="{{ route('employees.store')}}" enctype="multipart/form-data">
            @if (session('status'))
            <div class="alert alert-success col-lg-offset-2" role="alert">
              {{ session('status') }}
            </div>
            @endif
            <div class="form-group">
              <label for="image" class="control-label col-lg-2">Image <span class="required">*</span></label>
              <div class="col-lg-4">
                @if ($errors->has('image'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first('image') }}
                </div>
                @endif
                <input class="input-file" id="image" name="image" type="file" required />
              </div>
            </div>
            <div class="form-group">
              <label for="first_name" class="control-label col-lg-2">First name <span class="required">*</span></label>
              <div class="col-lg-4">
                @if ($errors->has('first_name'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first('first_name') }}
                </div>
                @endif
                <input class="form-control" id="first_name" name="first_name" type="text"
                  value="{{ old('first_name') }}" />
              </div>
              <label for="second_name" class="control-label col-lg-2">Second name</label>
              <div class="col-lg-4">
                @if ($errors->has('second_name'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first('second_name') }}
                </div>
                @endif
                <input class="form-control" id="second_name" name="second_name" type="text"
                  value="{{ old('second_name') }}" />
              </div>
            </div>
            <div class="form-group ">
              <label for="first_lastname" class="control-label col-lg-2">First lastname <span
                  class="required">*</span></label>
              <div class="col-lg-4">
                @if ($errors->has('first_lastname'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first('first_lastname') }}
                </div>
                @endif
                <input class="form-control" id="first_lastname" name="first_lastname" type="text"
                  value="{{ old('first_lastname') }}" required />
              </div>
              <label for="second_lastname" class="control-label col-lg-2">Second lastname <span
                  class="required">*</span></label>
              <div class="col-lg-4">
                @if ($errors->has('second_lastname'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first('second_lastname') }}
                </div>
                @endif
                <input class="form-control" id="second_lastname" name="second_lastname" type="text"
                  value="{{ old('second_lastname') }}" />
              </div>
            </div>
            <div class="form-group ">
              <label for="cedula" class="control-label col-lg-2">Cedula <span class="required">*</span></label>
              <div class="col-lg-4">
                @if ($errors->has('cedula'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first('cedula') }}
                </div>
                @endif
                <input class="form-control" id="cedula" name="cedula" type="text" value="{{ old('cedula') }}"
                  required />
              </div>
              <label for="phone" class="control-label col-lg-2">Phone <span class="required">*</span></label>
              <div class="col-lg-4">
                @if ($errors->has('phone'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first('phone') }}
                </div>
                @endif
                <input class="form-control" id="phone" name="phone" type="text" value="{{ old('phone') }}" required />
              </div>
            </div>
            <div class="form-group ">
              <label for="department_id" class="control-label col-lg-2">Department <span
                  class="required">*</span></label>
              <div class="col-lg-4">
                @if ($errors->has('department_id'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first('department_id') }}
                </div>
                @endif
                <select name="department_id" id="department_id" class="form-control" required>
                  <option></option>
                  @foreach ($departments as $department)
                  <option value="{{ $department->id ?? old('department_id')}}">
                    {{ $department->name ?? old('department_id') }}</option>
                  @endforeach
                </select>
              </div>
              <label for="position_id" class="control-label col-lg-2">Position <span class="required">*</span></label>
              <div class="col-lg-4">
                @if ($errors->has('position_id'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first('position_id') }}
                </div>
                @endif
                <select name="position_id" id="position_id" class="form-control" required>
                  <option></option>
                  @foreach ($positions as $position)
                  <option value="{{ $position->id }}">{{ $position->name }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group ">
              <label for="user_name" class="control-label col-lg-2">User name <span class="required">*</span></label>
              <div class="col-lg-4">
                @if ($errors->has('user_name'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first('user_name') }}
                </div>
                @endif
                <input class="form-control" id="user_name" name="user_name" type="text" value="{{ old('user_name') }}"
                  required />
              </div>
              <label for="email" class="control-label col-lg-2">email <span class="required">*</span></label>
              <div class="col-lg-4">
                @if ($errors->has('email'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first('email') }}
                </div>
                @endif
                <div class="input-group input-group-md">
                  <span class="input-group-addon">@</span>
                  <input class="form-control" id="email" name="email" type="email" value="{{ old('email') }}"
                    required />
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="col-lg-offset-2 col-lg-10">
                <button class="btn btn-primary" type="submit">Save</button>
                <a class="btn btn-danger" href="{{ route('employees.index') }}">Cancel</a>
              </div>
            </div>
            {{ csrf_field() }}
          </form>
        </div>
      </div>
    </section>
  </div>
</div>
{{--main content end--}}
@endsection