@extends('template')
@section('main-content')
    {{-- overview start --}}
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa-users"></i>Employees Module</h3>
            <ol class="breadcrumb">
                <li><i class="fa fa-home"></i><a href="{{ route('dashboard') }}">Dashboard</a></li>
                <li><i class="fa fa-users"></i>Employees</li>
            </ol>
        </div>
    </div>
    {{-- overview end --}}
    {{-- main content start --}}
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                      @if (session('status'))
                          <div class="alert alert-danger col-lg-offset-0" role="alert">
                              {{ session('status') }}
                          </div>
                      @endif
                        <div class="table-responsive">
                            <table class="table table-striped table-advance table-hover" id="list-employees">
                                <caption class="page-header-table">Employees</caption>
                                <tbody class="form-horizontal">
                                    <tr class="form-group">
                                        <th class="col-lg-1 col-sm-1 center">N°</th>
                                        <th class="col-lg-1 col-sm-1 center"># Cedula</th>
                                        <th class="col-lg-2 col-sm-2 center"><i class="fa fa-user"
                                                aria-hidden="trues"></i> Full Name</th>
                                        <th class="col-lg-2 col-sm-2 center"><i class="fa fa-sitemap"></i> Department</th>
                                        <th class="col-lg-2 col-sm-1 center"><i class="icon_genius"></i> Fixed Asset</th>
                                        <th class="col-lg-2 col-sm-2 center"><i class="icon_genius"></i> Deliveries
                                            Certificates</th>
                                        <th class="col-lg-2 col-sm-2 center"><i class="icon_cogs"></i> Action</th>
                                    </tr>
                                    @foreach ($employees as $employee)
                                        @if (!$employee->deleted_at)
                                            <tr class="form-group">
                                                <td class="col-lg-1 col-sm-1 center">{{ $count }}</td>
                                                <td class="col-lg-1 col-sm-1 center">{{ $employee->cedula }}</td>
                                                <td class="col-lg-2 col-sm-2 center">
                                                    {{ "$employee->first_name $employee->second_name $employee->first_lastname $employee->second_lastname" }}
                                                </td>
                                                <td class="col-lg-2 col-sm-2 center">{{ $employee->department->name }}
                                                </td>
                                                <td class="col-lg-2 col-sm-2 center">
                                                    @foreach ($employee->fixedassets as $fixedasset)
                                                        @switch(strlen($fixedasset->code))
                                                            @case(1)
                                                                <a
                                                                    href="{{ route('fixed-assets.show', $fixedasset->id) }}">{{ $fixedasset->code ? "000$fixedasset->code," : 0 }}</a>
                                                            @break

                                                            @case(2)
                                                                <a
                                                                    href="{{ route('fixed-assets.show', $fixedasset->id) }}">{{ $fixedasset->code ? "00$fixedasset->code," : 0 }}</a>
                                                            @break

                                                            @case(3)
                                                                <a
                                                                    href="{{ route('fixed-assets.show', $fixedasset->id) }}">{{ $fixedasset->code ? "0$fixedasset->code," : 0 }}</a>
                                                            @break

                                                            @case(4)
                                                                <a
                                                                    href="{{ route('fixed-assets.show', $fixedasset->id) }}">{{ $fixedasset->code ? "$fixedasset->code," : 0 }}</a>
                                                            @break
                                                        @endswitch
                                                    @endforeach
                                                </td>
                                                <td class="col-lg-2 col-sm-2 center">
                                                    @foreach ($employee->deliveries as $delivery)
                                                        @if ($delivery->code !== 0)
                                                            @switch(strlen($delivery->code))
                                                                @case(1)
                                                                    <a
                                                                        href="{{ route('delivery-certificates.show', $delivery->id) }}">{{ $delivery->code ? "000$delivery->code," : 0 }}</a>
                                                                @break

                                                                @case(2)
                                                                    <a
                                                                        href="{{ route('delivery-certificates.show', $delivery->id) }}">{{ $delivery->code ? "00$delivery->code," : 0 }}</a>
                                                                @break

                                                                @case(3)
                                                                    <a
                                                                        href="{{ route('delivery-certificates.show', $delivery->id) }}">{{ $delivery->code ? "0$delivery->code," : 0 }}</a>
                                                                @break

                                                                @case(3)
                                                                    <a
                                                                        href="{{ route('delivery-certificates.show', $delivery->id) }}">{{ $delivery->code ? "$delivery->code," : 0 }}</a>
                                                                @break
                                                            @endswitch
                                                        @endif
                                                    @endforeach
                                                </td>
                                                <td class="col-lg-2 col-sm-2 center">
                                                    <div class="btn-group btn-group-xs" role="group">
                                                        <a class="btn btn-primary"
                                                            href="{{ route('employees.edit', $employee->id) }}">
                                                            <i class="icon_plus_alt2"></i>
                                                        </a>
                                                        <a class="btn btn-success"
                                                            href="{{ route('employees.show', $employee->id) }}">
                                                            <i class="icon_check_alt2"></i>
                                                        </a>
                                                        <a class="btn btn-danger" data-employee-id="{{ $employee->id }}">
                                                            <i class="icon_close_alt2"></i>
                                                        </a>
                                                    </div>
                                                </td>
                                            </tr>
                                            @php $count++ @endphp
                                        @endif
                                    @endforeach
                                </tbody>
                                <form id="form-delete-element" method="POST">
                                    <input type="hidden" name="id" value="" class='input-id'>
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                </form>
                            </table>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
    {{-- main content end --}}
    <form id="form-delete-element" method="POST">
        <input type="hidden" name="id" value="" id='id-element-delete'>
        <input type="hidden" name="_token" value="Qas8offg6LmvkGK4y4FnkEkHBDOP9ujDP1d74kuU">
    </form>
@endsection
