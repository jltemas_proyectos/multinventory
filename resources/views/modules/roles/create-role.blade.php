@extends('template')
@section('main-content')
{{--overview start--}}
<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header"><i class="fa fa-address-book-o"></i>Roles Module</h3>
    <ol class="breadcrumb">
      <li><i class="fa fa-home"></i><a href="{{ route('dashboard') }}">Dashboard</a></li>
      <li><i class="fa fa-address-book-o"></i><a href="{{ route('roles.index') }}">Roles</a></li>
      <li><i class="fa fa-cog"></i>Create Role</li>
    </ol>
  </div>
</div>
{{--overview end--}}
{{--main content start--}}
<div class="row">
  <div class="panel-body panel-body-j">
    <section class="panel">
      <header class="panel-heading">
        <h3>Role Registration Form</h3>
      </header>
      <div class="panel-body">
        <div class="form">
          <form class="form-validate form-horizontal " id="register_form" method="POST"
            action="{{ route('roles.store') }}">
            @if (session()->has('status'))
            <div class="alert alert-success col-lg-offset-2" role="alert">
              {{ session('status') }}
            </div>
            @endif
            <div class="form-group ">
              <label for="name" class="control-label col-lg-2">Name <span class="required">*</span></label>
              <div class="col-lg-10">
                @if ($errors->has('name'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first('name') }}
                </div>
                @endif
                <input class=" form-control" id="name" name="name" type="text" value="{{ old('name') }}" />
              </div>
            </div>
            <div class="form-group ">
              <label for="description" class="control-label col-lg-2">Description <span
                  class="required">*</span></label>
              <div class="col-lg-10">
                @if ($errors->has('description'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first('description') }}
                </div>
                @endif
                <textarea name="description" id="description" class="form-control">{{ old('description') }}</textarea>
              </div>
            </div>
            <div class="form-group">
              <div class="col-lg-offset-2 col-lg-10">
                <button class="btn btn-primary" type="submit">Save</button>
                <a class="btn btn-danger" href="{{ route('roles.index') }}">Cancel</a>
              </div>
            </div>
            {{ csrf_field() }}
          </form>
        </div>
      </div>
    </section>
  </div>
</div>
{{--main content end--}}
@endsection