@extends('template')
@section('main-content')
{{--overview start--}}
<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header"><i class="fa fa-archive"></i>Storage Cellar Module</h3>
    <ol class="breadcrumb">
      <li><i class="fa fa-home"></i><a href="{{ route('dashboard') }}">Dashboard</a></li>
      <li><i class="fa fa-archive"></i>Storage Cellars</li>
    </ol>
  </div>
</div>
{{--overview end--}}
{{--main content start--}}
<div class="row">
  <div class="col-lg-12">
    <div class="panel">
      <div class="panel-body">
        <div class="row">
          <div class="col-lg-12">
            <section class="panel">
              <div class="table-responsive">
                <table class="table table-striped table-advance table-hover">
                  <caption class="page-header-table">Storage Cellars</caption>
                  <tbody class="form-horizontal">
                    <tr class="form-group">
                      <th class="col-lg-1 col-sm-1 center"> N°</th>
                      <th class="col-lg-3 col-sm-2"><i class="fa fa-archive" aria-hidden="trues"></i> Name</th>
                      <th class="col-lg-5 col-sm-6"><i class="fa fa-file-text-o"></i> Description</th>
                      <th class="col-lg-3 col-sm-3"><i class="icon_cogs"></i> Action</th>
                    </tr>
                    @foreach($storage_cellars as $storage_cellar)
                    @if (!$storage_cellar->deleted_at)
                    <tr class="form-group">
                      <td class="col-lg-1 col-sm-1 center">{{ $count }}</td>
                      <td class="col-lg-3 col-sm-2">{{ $storage_cellar->name }}</td>
                      <td class="col-lg-5 col-sm-3 col-sm-3">{{ $storage_cellar->description }}</td>
                      <td class="col-sm-3">
                        <div class="btn-group">
                          <a class="btn btn-primary" href="{{ route('storage-cellars.edit', $storage_cellar->id) }}"><i
                              class="icon_plus_alt2"></i></a>
                          <form class="form-delete" method="POST"
                            action="{{ route('storage-cellars.destroy', $storage_cellar->id) }}">
                            <button class="btn btn-danger form-delete-button" type="submit"><i
                                class="icon_close_alt2"></i></button>
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                          </form>
                        </div>
                      </td>
                    </tr>
                    @php $count++ @endphp
                    @endif
                    @endforeach
                  </tbody>
                </table>
              </div>
            </section>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
{{--main content end--}}
@endsection