@extends('template')
@section('main-content')
{{--overview start--}}
<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header"><i class="icon_group"></i>User Profile</h3>
    <ol class="breadcrumb">
      <li><i class="fa fa-home"></i><a href="{{ route('dashboard') }}">Dashboard</a></li>
      <li><i class="icon_group"></i><a href="{{ route('users.index') }}">Users</a></li>
      <li><i class="fa fa-user"></i>Profile {{ $user->employee->first_name .' '. $user->employee->first_lastname }}</li>
    </ol>
  </div>
</div>
{{--overview end--}}
{{--main content start--}}
<div class="row">
  <div class="col-lg-12">
    {{-- profile-widget start--}}
    <div class="row">
      <div class="col-lg-12">
        <div class="profile-widget profile-widget-info">
          <div class="panel-body">
            <div class="col-lg-2 col-sm-2">
              <h4>{{ $user->employee->first_name .' '. $user->employee->first_lastname }}</h4>
              <div class="follow-ava">
                <img src="{{ asset('img/avatar-mini2.jpg')}}" alt="">
              </div>
              <h6>{{ $user->rol->name }}</h6>
            </div>
            <div class="col-lg-4 col-sm-4 follow-info">
              <p class="show-user"><i class="fa fa-sitemap"> {{ $user->employee->department->name }}</i></p>
              <p class="show-user"><i class="fa fa-handshake-o"> {{ $user->employee->position->name }}</i></p>
              <p class="show-user"><i class="fa fa-envelope"> {{ $user->email }}</i></p>
              <p class="show-user"><i class="fa fa-phone"> {{ $user->employee->phone }}</i></p>
            </div>
            <div class="col-lg-2 col-sm-6 follow-info weather-category">
              <ul>
                <li class="active">
                  <span class="block sub-title-show-article">Department</span>
                  <i class="block fa fa-sitemap fa-2x"></i>
                  {{ $user->employee->department->name }}
                </li>
              </ul>
            </div>
            <div class="col-lg-2 col-sm-6 follow-info weather-category">
              <ul>
                <li class="active">
                  <span class="block sub-title-show-article">Position</span>
                  <i class="block fa fa-handshake-o fa-2x"></i>
                  {{ $user->employee->position->name }}
                </li>
              </ul>
            </div>
            <div class="col-lg-2 col-sm-6 follow-info weather-category">
              <ul>
                <li class="active">
                  <span class="block sub-title-show-article">User</span>
                  <i class="block fa fa-user-circle fa-2x"></i>
                  {{ $user->user_name }}
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    {{-- profile-widget end--}}
    {{-- page start --}}
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading tab-bg-info" id="none-border">
            <ul class="nav nav-tabs">
              @if (count($errors->all()) > 0)
              <li>
                <a data-toggle="tab" href="#delivery-certificate">
                  <i class="icon-home"></i>
                  Delivery Certificate
                </a>
              </li>
              <li>
                <a data-toggle="tab" href="#profile">
                  <i class="icon-user"></i>
                  Profile
                </a>
              </li>
              <li class="active">
                <a data-toggle="tab" href="#edit-profile">
                  <i class="icon-envelope"></i>
                  Edit Profile
                </a>
              </li>
              @else
              <li class="active">
                <a data-toggle="tab" href="#delivery-certificate">
                  <i class="icon-home"></i>
                  Delivery Certificate
                </a>
              </li>
              <li>
                <a data-toggle="tab" href="#profile">
                  <i class="icon-user"></i>
                  Profile
                </a>
              </li>
              <li class="">
                <a data-toggle="tab" href="#edit-profile">
                  <i class="icon-envelope"></i>
                  Edit Profile
                </a>
              </li>
              @endif
            </ul>
          </header>
          <div class="panel-body">
            <div class="tab-content">
              {{-- delivery-certificate start --}}
              @if (count($errors->all()) > 0)
              <div id="delivery-certificate" class="tab-pane">
                <div class="profile-activity">
                  <div class="act-time">
                    <div class="activity-body act-in">
                      <span class="arrow"></span>
                      <div class="text">
                        <a href="#" class="activity-img"><img class="avatar" src="img/chat-avatar.jpg" alt=""></a>
                        <p class="attribution"><a href="#">Jonatanh Doe</a> at 4:25pm, 30th Octmber 2014</p>
                        <p>It is a long established fact that a reader will be distracted layout</p>
                      </div>
                    </div>
                  </div>
                  <div class="act-time">
                    <div class="activity-body act-in">
                      <span class="arrow"></span>
                      <div class="text">
                        <a href="#" class="activity-img"><img class="avatar" src="img/chat-avatar.jpg" alt=""></a>
                        <p class="attribution"><a href="#">Jhon Loves </a> at 5:25am, 30th Octmber 2014</p>
                        <p>Knowledge speaks, but wisdom listens.</p>
                      </div>
                    </div>
                  </div>
                  <div class="act-time">
                    <div class="activity-body act-in">
                      <span class="arrow"></span>
                      <div class="text">
                        <a href="#" class="activity-img"><img class="avatar" src="img/chat-avatar.jpg" alt=""></a>
                        <p class="attribution"><a href="#">Jimy Smith</a> at 5:25am, 30th Octmber 2014</p>
                        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis
                          egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec
                          eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat
                          eleifend leo.</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              @else
              <div id="delivery-certificate" class="tab-pane active">
                <div class="profile-activity">
                  <div class="act-time">
                    <div class="activity-body act-in">
                      <span class="arrow"></span>
                      <div class="text">
                        <a href="#" class="activity-img"><img class="avatar" src="img/chat-avatar.jpg" alt=""></a>
                        <p class="attribution"><a href="#">Jonatanh Doe</a> at 4:25pm, 30th Octmber 2014</p>
                        <p>It is a long established fact that a reader will be distracted layout</p>
                      </div>
                    </div>
                  </div>
                  <div class="act-time">
                    <div class="activity-body act-in">
                      <span class="arrow"></span>
                      <div class="text">
                        <a href="#" class="activity-img"><img class="avatar" src="img/chat-avatar.jpg" alt=""></a>
                        <p class="attribution"><a href="#">Jhon Loves </a> at 5:25am, 30th Octmber 2014</p>
                        <p>Knowledge speaks, but wisdom listens.</p>
                      </div>
                    </div>
                  </div>
                  <div class="act-time">
                    <div class="activity-body act-in">
                      <span class="arrow"></span>
                      <div class="text">
                        <a href="#" class="activity-img"><img class="avatar" src="img/chat-avatar.jpg" alt=""></a>
                        <p class="attribution"><a href="#">Jimy Smith</a> at 5:25am, 30th Octmber 2014</p>
                        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis
                          egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec
                          eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat
                          eleifend leo.</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              @endif
              {{-- delivery-certificate end --}}
              {{-- profile start--}}
              <div id="profile" class="tab-pane">
                <section class="panel">
                  <div class="bio-graph-heading">
                    <p>Multipartes De Colombia S.A.S</p>
                    <p>Department De Sistemas</p>
                  </div>
                  <div class="panel-body bio-graph-info">
                    <h1>User Info</h1>
                    <div class="row">
                      <div class="bio-row">
                        <p><span> Names </span>:
                          {{ $user->employee->first_name .' '.$user->employee->second_name.' ' }} </p>
                      </div>
                      <div class="bio-row">
                        <p><span>Department </span>: {{ $user->employee->department->name }}</p>
                      </div>
                      <div class="bio-row">
                        <p><span>Last Names </span>:
                          {{ $user->employee->first_lastname .' '.$user->employee->second_lastname.' ' }}</p>
                      </div>
                      <div class="bio-row">
                        <p><span>Position</span>: {{ $user->employee->position->name }}</p>
                      </div>
                      <div class="bio-row">
                        <p><span>Cedula </span>: {{ $user->employee->cedula }}</p>
                      </div>
                      <div class="bio-row">
                        <p><span>Email </span>: {{ $user->email }}</p>
                      </div>
                      <div class="bio-row">
                        <p><span>User name </span>: {{ $user->user_name }}</p>
                      </div>
                      <div class="bio-row">
                        <p><span>Phone </span>: {{ $user->employee->phone }}</p>
                      </div>
                    </div>
                  </div>
                </section>
              </div>
              {{--profile start--}}
              {{--edit-profile start--}}
              @if (count($errors->all()) > 0)
              <div id="edit-profile" class="tab-pane active">
                <section class="panel">
                  <div class="panel-body bio-graph-info">
                    <h1> Profile Info</h1>
                    <form class="form-validate form-horizontal " id="register_form" method="POST"
                      action="{{ route('users.update', $user->id) }}">
                      @if (session()->has('status'))
                      <div class="alert alert-success col-lg-offset-2">
                        {{ session('status') }}
                      </div>
                      @endif
                      <div class="form-group">
                        <label for="first_name" class="control-label col-lg-2">First name <span
                            class="required">*</span></label>
                        <div class="col-lg-4">
                          @if ($errors->has('first_name'))
                          <div class="alert alert-danger">
                            {{ $errors->first('first_name') }}
                          </div>
                          @endif
                          <input class="form-control " id="first_name" name="first_name" type="text"
                            value="{{ old('first_name') ?? $user->employee->first_name}}" />
                        </div>
                        <label for="second_name" class="control-label col-lg-2">Second name <span
                            class="required">*</span></label>
                        <div class="col-lg-4">
                          @if ($errors->has('second_name'))
                          <div class="alert alert-danger">
                            {{ $errors->first('second_name') }}
                          </div>
                          @endif
                          <input class="form-control " id="second_name" name="second_name" type="text"
                            value="{{ old('second_name') ?? $user->employee->second_name}}"
                            aria-describedby="basic-addon1" />
                        </div>
                      </div>
                      <div class="form-group ">
                        <label for="first_lastname" class="control-label col-lg-2">First lastname <span
                            class="required">*</span></label>
                        <div class="col-lg-4">
                          @if ($errors->has('first_lastname'))
                          <div class="alert alert-danger">
                            {{ $errors->first('first_lastname') }}
                          </div>
                          @endif
                          <input class="form-control " id="first_lastname" name="first_lastname" type="text"
                            value="{{ old('first_lastname') ?? $user->employee->first_lastname}}" />
                        </div>
                        <label for="second_lastname" class="control-label col-lg-2">Second lastname <span
                            class="required">*</span></label>
                        <div class="col-lg-4">
                          @if ($errors->has('second_lastname'))
                          <div class="alert alert-danger">
                            {{ $errors->first('second_lastname') }}
                          </div>
                          @endif
                          <input class="form-control " id="second_lastname" name="second_lastname" type="text"
                            value="{{ old('second_lastname') ?? $user->employee->second_lastname }}" />
                        </div>
                      </div>
                      <div class="form-group ">
                        <label for="cedula" class="control-label col-lg-2">Cedula <span
                            class="required">*</span></label>
                        <div class="col-lg-4">
                          @if ($errors->has('cedula'))
                          <div class="alert alert-danger">
                            {{ $errors->first('cedula') }}
                          </div>
                          @endif
                          <input class="form-control " id="cedula" name="cedula" type="text"
                            value="{{ old('cedula') ?? $user->employee->cedula }}" />
                        </div>
                        <label for="phone" class="control-label col-lg-2">Phone <span class="required">*</span></label>
                        <div class="col-lg-4">
                          @if ($errors->has('phone'))
                          <div class="alert alert-danger">
                            {{ $errors->first('phone') }}
                          </div>
                          @endif
                          <input class="form-control " id="phone" name="phone" type="text"
                            value="{{ old('phone') ?? $user->employee->phone }}" />
                        </div>
                      </div>
                      <div class="form-group ">
                        <label for="department" class="control-label col-lg-2">Department <span
                            class="required">*</span></label>
                        <div class="col-lg-4">
                          @if ($errors->has('department'))
                          <div class="alert alert-danger">
                            {{ $errors->first('department') }}
                          </div>
                          @endif
                          <select class="form-control " id="department" name="department">
                            @foreach ($departments as $department)
                            @if ($user->employee->department->id === $department->id)
                            <option value="{{ $department->id }}" selected>{{ $department->name }}</option>
                            @continue
                            @endif
                            <option value="{{ $department->id }}">{{ $department->name }}</option>
                            @endforeach
                          </select>
                        </div>
                        <label for="position" class="control-label col-lg-2">Position <span
                            class="required">*</span></label>
                        <div class="col-lg-4">
                          @if ($errors->has('position'))
                          <div class="alert alert-danger">
                            {{ $errors->first('position') }}
                          </div>
                          @endif
                          <select class="form-control " id="department" name="position">
                            @foreach ($positions as $position)
                            @if ($user->employee->position->id === $position->id)
                            <option value="{{ $position->id }}" selected>{{ $position->name }}</option>
                            @continue
                            @endif
                            <option value="{{ $position->id }}">{{ $position->name }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="user_name" class="control-label col-lg-2">User name <span
                            class="required">*</span></label>
                        <div class="col-lg-4">
                          @if ($errors->has('user_name'))
                          <div class="alert alert-danger">
                            {{ $errors->first('user_name') }}
                          </div>
                          @endif
                          <input class="form-control " id="user_name" name="user_name" type="text"
                            value="{{ old('user_name') ?? $user->user_name}}" />
                        </div>
                        <label for="email" class="control-label col-lg-2">Email <span class="required">*</span></label>
                        <div class="col-lg-4">
                          @if ($errors->has('email'))
                          <div class="alert alert-danger">
                            {{ $errors->first('email') }}
                          </div>
                          @endif
                          <div class="input-group input-group-md">
                            <span class="input-group-addon" id="basic-addon1">@</span>
                            <input class="form-control " id="email" name="email" type="email"
                              value="{{ old('email') ?? $user->email}}" aria-describedby="basic-addon1" />
                          </div>
                        </div>
                      </div>
                      <div class="form-group ">
                        <label for="password" class="control-label col-lg-2">Password <span
                            class="required">*</span></label>
                        <div class="col-lg-4">
                          @if ($errors->has('password'))
                          <div class="alert alert-danger">
                            {{ $errors->first('password') }}
                          </div>
                          @endif
                          <input class="form-control " id="password" name="password" type="password" />
                        </div>
                        <label for="confirm_password" class="control-label col-lg-2">Confirm Password <span
                            class="required">*</span></label>
                        <div class="col-lg-4">
                          @if ($errors->has('confirm_password'))
                          <div class="alert alert-danger">
                            {{ $errors->first('confirm_password') }}
                          </div>
                          @endif
                          <input class="form-control " id="confirm_password" name="confirm_password" type="password" />
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                          <button class="btn btn-primary" type="submit">Update</button>
                          <a class="btn btn-danger" href="{{ route('users.index') }}">Cancel</a>
                        </div>
                      </div>
                      {{ csrf_field() }}
                      {{ method_field('PUT') }}
                    </form>
                  </div>
                </section>
              </div>
              @else
              <div id="edit-profile" class="tab-pane">
                <section class="panel">
                  <div class="panel-body bio-graph-info">
                    <h1> Profile Info</h1>
                    <form class="form-validate form-horizontal " id="register_form" method="POST"
                      action="{{ route('users.update', $user->id) }}">
                      @if (session()->has('status'))
                      <div class="alert alert-success col-lg-offset-2">
                        {{ session('status') }}
                      </div>
                      @endif
                      <div class="form-group">
                        <label for="first_name" class="control-label col-lg-2">First name <span
                            class="required">*</span></label>
                        <div class="col-lg-4">
                          @if ($errors->has('first_name'))
                          <div class="alert alert-danger">
                            {{ $errors->first('first_name') }}
                          </div>
                          @endif
                          <input class="form-control " id="first_name" name="first_name" type="text"
                            value="{{ old('first_name') ?? $user->employee->first_name}}" />
                        </div>
                        <label for="second_name" class="control-label col-lg-2">Second name <span
                            class="required">*</span></label>
                        <div class="col-lg-4">
                          @if ($errors->has('second_name'))
                          <div class="alert alert-danger">
                            {{ $errors->first('second_name') }}
                          </div>
                          @endif
                          <input class="form-control " id="second_name" name="second_name" type="text"
                            value="{{ old('second_name') ?? $user->employee->second_name}}"
                            aria-describedby="basic-addon1" />
                        </div>
                      </div>
                      <div class="form-group ">
                        <label for="first_lastname" class="control-label col-lg-2">First lastname <span
                            class="required">*</span></label>
                        <div class="col-lg-4">
                          @if ($errors->has('first_lastname'))
                          <div class="alert alert-danger">
                            {{ $errors->first('first_lastname') }}
                          </div>
                          @endif
                          <input class="form-control " id="first_lastname" name="first_lastname" type="text"
                            value="{{ old('first_lastname') ?? $user->employee->first_lastname}}" />
                        </div>
                        <label for="second_lastname" class="control-label col-lg-2">Second lastname <span
                            class="required">*</span></label>
                        <div class="col-lg-4">
                          @if ($errors->has('second_lastname'))
                          <div class="alert alert-danger">
                            {{ $errors->first('second_lastname') }}
                          </div>
                          @endif
                          <input class="form-control " id="second_lastname" name="second_lastname" type="text"
                            value="{{ old('second_lastname') ?? $user->employee->second_lastname }}" />
                        </div>
                      </div>
                      <div class="form-group ">
                        <label for="cedula" class="control-label col-lg-2">Cedula <span
                            class="required">*</span></label>
                        <div class="col-lg-4">
                          @if ($errors->has('cedula'))
                          <div class="alert alert-danger">
                            {{ $errors->first('cedula') }}
                          </div>
                          @endif
                          <input class="form-control " id="cedula" name="cedula" type="text"
                            value="{{ old('cedula') ?? $user->employee->cedula }}" />
                        </div>
                        <label for="phone" class="control-label col-lg-2">Phone <span class="required">*</span></label>
                        <div class="col-lg-4">
                          @if ($errors->has('phone'))
                          <div class="alert alert-danger">
                            {{ $errors->first('phone') }}
                          </div>
                          @endif
                          <input class="form-control " id="phone" name="phone" type="text"
                            value="{{ old('phone') ?? $user->employee->phone }}" />
                        </div>
                      </div>
                      <div class="form-group ">
                        <label for="department" class="control-label col-lg-2">Department <span
                            class="required">*</span></label>
                        <div class="col-lg-4">
                          @if ($errors->has('department'))
                          <div class="alert alert-danger">
                            {{ $errors->first('department') }}
                          </div>
                          @endif
                          <select class="form-control " id="department" name="department">
                            @foreach ($departments as $department)
                            @if ($user->employee->department->id === $department->id)
                            <option value="{{ $department->id }}" selected>{{ $department->name }}</option>
                            @continue
                            @endif
                            <option value="{{ $department->id }}">{{ $department->name }}</option>
                            @endforeach
                          </select>
                        </div>
                        <label for="position" class="control-label col-lg-2">Position <span
                            class="required">*</span></label>
                        <div class="col-lg-4">
                          @if ($errors->has('position'))
                          <div class="alert alert-danger">
                            {{ $errors->first('position') }}
                          </div>
                          @endif
                          <select class="form-control " id="department" name="position">
                            @foreach ($positions as $position)
                            @if ($user->employee->position->id === $position->id)
                            <option value="{{ $position->id }}" selected>{{ $position->name }}</option>
                            @continue
                            @endif
                            <option value="{{ $position->id }}">{{ $position->name }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="user_name" class="control-label col-lg-2">User name <span
                            class="required">*</span></label>
                        <div class="col-lg-4">
                          @if ($errors->has('user_name'))
                          <div class="alert alert-danger">
                            {{ $errors->first('user_name') }}
                          </div>
                          @endif
                          <input class="form-control " id="user_name" name="user_name" type="text"
                            value="{{ old('user_name') ?? $user->user_name}}" />
                        </div>
                        <label for="email" class="control-label col-lg-2">Email <span class="required">*</span></label>
                        <div class="col-lg-4">
                          @if ($errors->has('email'))
                          <div class="alert alert-danger">
                            {{ $errors->first('email') }}
                          </div>
                          @endif
                          <div class="input-group input-group-md">
                            <span class="input-group-addon" id="basic-addon1">@</span>
                            <input class="form-control " id="email" name="email" type="email"
                              value="{{ old('email') ?? $user->email}}" aria-describedby="basic-addon1" />
                          </div>
                        </div>
                      </div>
                      <div class="form-group ">
                        <label for="password" class="control-label col-lg-2">Password <span
                            class="required">*</span></label>
                        <div class="col-lg-4">
                          @if ($errors->has('password'))
                          <div class="alert alert-danger">
                            {{ $errors->first('password') }}
                          </div>
                          @endif
                          <input class="form-control " id="password" name="password" type="password" />
                        </div>
                        <label for="confirm_password" class="control-label col-lg-2">Confirm Password <span
                            class="required">*</span></label>
                        <div class="col-lg-4">
                          @if ($errors->has('confirm_password'))
                          <div class="alert alert-danger">
                            {{ $errors->first('confirm_password') }}
                          </div>
                          @endif
                          <input class="form-control " id="confirm_password" name="confirm_password" type="password" />
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                          <button class="btn btn-primary" type="submit">Update</button>
                          <a class="btn btn-danger" href="{{ route('users.index') }}">Cancel</a>
                        </div>
                      </div>
                      {{ csrf_field() }}
                      {{ method_field('PUT') }}
                    </form>
                  </div>
                </section>
              </div>
              @endif
              {{--edit-profile end--}}
            </div>
          </div>
        </section>
      </div>
    </div>
    {{-- page end --}}
  </div>
</div>
{{--main content end--}}
@endsection