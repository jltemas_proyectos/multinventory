@extends('template')
@section('main-content')
{{--overview start--}}
<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header"><i class="icon_group"></i>Users Modulo</h3>
    <ol class="breadcrumb">
      <li><i class="fa fa-home"></i><a href="{{ route('dashboard') }}">Dashboard</a></li>
      <li><i class="icon_group"></i><a href="{{ route('users.index') }}">Users</a></li>
      <li><i class="fa fa-user-circle"></i>User Update</li>
    </ol>
  </div>
</div>
{{--overview end--}}
{{--main content start--}}
<div class="row">
  <div class="col-lg-12">
    <section class="panel">
      <header class="panel-heading">
        <h3>User Update Form</h3>
      </header>
      <div class="panel-body">
        <form class="form-validate form-horizontal " id="register_form" method="POST"
          action="{{ route('users.update', $user->id) }}">
          @if (session()->has('status'))
          <div class="alert alert-success col-lg-offset-2">
            {{ session('status') }}
          </div>
          @endif
          <div class="form-group">
            <label for="user_name" class="control-label col-lg-2">User name <span class="required">*</span></label>
            <div class="col-lg-10">
              @if ($errors->has('user_name'))
              <div class="alert alert-danger">
                {{ $errors->first('user_name') }}
              </div>
              @endif
              <input class="form-control " id="user_name" name="user_name" type="text"
                value="{{ old('user_name') ?? $user->user_name}}" />
            </div>
          </div>
          <div class="form-group ">
            <label for="email" class="control-label col-lg-2">Email <span class="required">*</span></label>
            <div class="col-lg-10">
              @if ($errors->has('email'))
              <div class="alert alert-danger">
                {{ $errors->first('email') }}
              </div>
              @endif
              <div class="input-group input-group-md">
                <span class="input-group-addon" id="basic-addon1">@</span>
                <input class="form-control " id="email" name="email" type="email"
                  value="{{ old('email') ?? $user->email}}" aria-describedby="basic-addon1" />
              </div>
            </div>
          </div>
          <div class="form-group ">
            <label for="password" class="control-label col-lg-2">Password <span class="required">*</span></label>
            <div class="col-lg-10">
              @if ($errors->has('password'))
              <div class="alert alert-danger">
                {{ $errors->first('password') }}
              </div>
              @endif
              <input class="form-control " id="password" name="password" type="password" />
            </div>
          </div>
          <div class="form-group ">
            <label for="confirm_password" class="control-label col-lg-2">Confirm Password <span
                class="required">*</span></label>
            <div class="col-lg-10">
              @if ($errors->has('confirm_password'))
              <div class="alert alert-danger">
                {{ $errors->first('confirm_password') }}
              </div>
              @endif
              <input class="form-control " id="confirm_password" name="confirm_password" type="password" />
            </div>
          </div>
          <div class="form-group">
            <div class="col-lg-offset-2 col-lg-10">
              <button class="btn btn-primary" type="submit">Update</button>
              <a class="btn btn-danger" href="{{ route('users.index') }}">Cancel</a>
            </div>
          </div>
          {{ csrf_field() }}
          {{ method_field('PUT') }}
        </form>
      </div>
    </section>
  </div>
</div>
{{--main content end--}}
@endsection