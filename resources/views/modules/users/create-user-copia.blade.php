@extends('template.layout')
@section('main-content')
<section class="wrapper">
  {{--overview start--}}
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header"><i class="icon_group"></i>Users Module</h3>
      <ol class="breadcrumb">
          <li><i class="fa fa-home"></i><a href="{{ route('home') }}">Home</a></li>
          <li><i class="icon_group"></i><a href="{{ route('users.index') }}">Users</a></li>
          <li><i class="fa fa-user-circle-o"></i>Create User</li>
      </ol>
    </div>
  </div>
</section>
{{--overview end--}}
{{--main content start--}}
<div class="row">
  <div class="col-lg-12">
    <div class="panel-body panel-body-j">
      <section class="panel">
        <header class="panel-heading">
          <h3>User Registration Form</h3>
        </header>
        <div class="panel-body">
          <div class="form">
            <form class="form-validate form-horizontal " id="register_form" method="get" action="">
              <div class="form-group ">
                <label for="fullname" class="control-label col-lg-2">Firs name <span class="required">*</span></label>
                <div class="col-lg-10">
                  <input class=" form-control" id="fullname" name="fullname" type="text" />
                </div>
              </div>
              <div class="form-group ">
                <label for="fullname" class="control-label col-lg-2">Second name <span class="required">*</span></label>
                <div class="col-lg-10">
                  <input class=" form-control" id="fullname" name="fullname" type="text" />
                </div>
              </div>
              <div class="form-group ">
                <label for="fullname" class="control-label col-lg-2">Firs lastname <span class="required">*</span></label>
                <div class="col-lg-10">
                  <input class=" form-control" id="fullname" name="fullname" type="text" />
                </div>
              </div>
              <div class="form-group ">
                <label for="fullname" class="control-label col-lg-2">Second lastname <span class="required">*</span></label>
                <div class="col-lg-10">
                  <input class=" form-control" id="fullname" name="fullname" type="text" />
                </div>
              </div>
              <div class="form-group ">
                <label for="address" class="control-label col-lg-2">Id <span class="required">*</span></label>
                <div class="col-lg-10">
                  <input class=" form-control" id="address" name="address" type="text" />
                </div>
              </div>
              <div class="form-group ">
                <label for="username" class="control-label col-lg-2">User name <span class="required">*</span></label>
                <div class="col-lg-10">
                  <input class="form-control " id="username" name="username" type="text" />
                </div>
              </div>
              <div class="form-group ">
                <label for="password" class="control-label col-lg-2">email <span class="required">*</span></label>
                <div class="col-lg-10">
                  <input class="form-control " id="password" name="email" type="password" />
                </div>
              </div>
              <div class="form-group ">
                <label for="confirm_password" class="control-label col-lg-2">Password <span class="required">*</span></label>
                <div class="col-lg-10">
                  <input class="form-control " id="confirm_password" name="confirm_password" type="password" />
                </div>
              </div>
              <div class="form-group ">
                <label for="confirm_password" class="control-label col-lg-2">Repeat Password <span class="required">*</span></label>
                <div class="col-lg-10">
                  <input class="form-control " id="confirm_password" name="confirm_password" type="password" />
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                  <button class="btn btn-primary" type="submit">Save</button>
                  <button class="btn btn-default" type="button">Cancel</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </section>
    </div>
  </div>
</div>
 {{--main content end--}}
@endsection