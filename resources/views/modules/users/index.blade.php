@extends('template')
@section('main-content')
{{--overview start--}}
<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header"><i class="icon_group"></i>{{$module}}</h3>
    <ol class="breadcrumb">
      <li><i class="fa fa-home"></i><a href="{{ route('dashboard') }}">Dashboard</a></li>
      <li><i class="icon_group"></i>Users</li>
    </ol>
  </div>
</div>
{{--overview end--}}
{{--main content start--}}
<div class="row">
  <div class="col-lg-12">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <div class="table-responsive">
            <table class="table table-striped table-advance table-hover">
              <caption class="page-header-table">Users</caption>
              <tbody class="form-horizontal">
                <tr class="form-group">
                  <th class="col-lg-1 center"> N°</th>
                  <th class="col-lg-2 center"><i class="fa fa-user-o"></i> User</th>
                  <th class="col-lg-3 center"><i class="fa fa-cog"></i> Roles</th>
                  <th class="col-lg-4 center"><i class="fa fa-envelope-o"></i> Email</th>
                  <th class="col-lg-2 center"><i class="icon_cogs"></i> Action</th>
                </tr>
                @foreach ($users as $key => $user)
                @if (!$user->deleted_at)
                <tr class="form-group">
                  <td class="col-lg-1 center">{{ ($key + 1) }}</td>
                  <td class="col-lg-2 center">{{ $user->user }}</td>
                  @forelse ($user->roles as $role)
                  <td class="col-lg-3 center">{{ $role->name }}</td>
                  @empty
                  <td class="col-lg-3 center">Sin role</td>
                  @endforelse
                  <td class="col-lg-4 center">{{ $user->email}}</td>
                  <td class="col-lg-2 center">
                    <div class="btn-group btn-group-xs" role="group">
                      <a class="btn btn-primary" href="{{ route('users.edit' , $user->id) }}">
                        <i class="icon_plus_alt2"></i>
                      </a>
                      <a class="btn btn-success" href="{{ route('users.show' , $user->id) }}">
                        <i class="icon_check_alt2"></i>
                      </a>
                      <a href="" class="btn btn-danger">
                        <i class="icon_close_alt2"></i>
                      </a>
                      {{-- <form class="form-delete" method="POST" action="{{ route('users.destroy', $user->id) }}">
                        <div class="btn-group" role="group">
                          <button class="btn btn-xs btn-danger form-delete-button" type="submit">
                            <i class="icon_close_alt2"></i></button>
                        </div>
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                      </form> --}}
                    </div>
                  </td>
                </tr>
                @endif
                @endforeach
              </tbody>
            </table>
          </div>
        </section>
      </div>
    </div>
  </div>
</div>
{{--main content end--}}
@endsection