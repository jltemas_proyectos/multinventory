@extends('template')
@section('main-content')
{{--overview start--}}
<div class="row">
  <div class="col-sm-12">
    <h3 class="page-header"><i class="fa fa-tachometer" aria-hidden="true"></i>Category Module</h3>
    <ol class="breadcrumb">
      <li><i class="fa fa-home"></i><a href="{{ route('dashboard') }}">Dashboard</a></li>
      <li><i class="fa fa-tachometer" aria-hidden="true"></i>Categories</li>
    </ol>
  </div>
</div>
{{--overview end--}}
{{--main content start--}}
<div class="row">
  <div class="col-sm-12">
    <div class="row">
      <div class="col-sm-12">
        <section class="panel">
          <div class="table-responsive">
            <table class="table table-striped table-advance table-hover">
              <caption class="page-header-table">Categories</caption>
              <tbody class="form-horizontal">
                <tr class="form-group">
                  <th class="col-sm-2  center"> N°</th>
                  <th class="col-sm-2 "><i class="fa fa-tachometer" aria-hidden="trues"></i> Name</th>
                  <th class="col-sm-4 "><i class="fa fa-file-text-o"></i> Description</th>
                  <th class="col-sm-4 "><i class="icon_cogs"></i> Action</th>
                </tr>
                @foreach($categories as $key => $category)
                @if (!$category->delete_at)
                <tr class="form-group btn-group-sm">
                  <td class="col-sm-2 center">{{ ($count) }}</td>
                  <td class="col-sm-2">{{ $category->name }}</td>
                  <td class="col-sm-4">{{ $category->description }}</td>
                  <td class="col-sm-4">
                    <div class="btn-group btn-group-sm">
                      <a class="btn btn-primary" href="{{ route('categories.edit', $category->id) }}"><i
                          class="icon_plus_alt2"></i></a>
                      <form class="form-delete" method="POST" action="{{ route('categories.destroy', $category->id) }}">
                        <button class="btn btn-sm btn-danger form-delete-button" type="submit"><i
                            class="icon_close_alt2"></i></button>
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                      </form>
                    </div>
                  </td>
                </tr>
                @php $count++ @endphp
                @endif
                @endforeach
              </tbody>
            </table>
          </div>
        </section>
      </div>
    </div>
  </div>
</div>
{{--main content end--}}
@endsection