@extends('template')
@section('main-content')
{{--overview start--}}
<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header"><i class="fa fa-tachometer" aria-hidden="true"></i>Category Module</h3>
    <ol class="breadcrumb">
      <li><i class="fa fa-home"></i><a href="{{ route('dashboard') }}">Dashboard</a></li>
      <li><i class="fa fa-tachometer" aria-hidden="true"></i><a href="{{ route('categories.index') }}">Categories</a>
      </li>
      <li><i class="fa fa-check"></i>Update Category</li>
    </ol>
  </div>
</div>
{{--overview end--}}
{{--main content start--}}
<div class="row">
  <div class="col-lg-12">
    <section class="panel">
      <header class="panel-heading">
        <h3>Category Update Form</h3>
      </header>
      <div class="panel-body">
        <div class="form">
          <form class="form-validate form-horizontal " id="register_form" method="POST"
            action="{{ route('categories.update', $category->id) }}">
            @if (session('status'))
            <div class="alert alert-success col-lg-offset-2" role="alert">
              {{ session('status') }}
            </div>
            @endif
            <div class="form-group">
              <div class="col-sm-4 col-sm-offset-4">
                <img src='{{asset("storage/$category->image")}}' alt="{{$category->name }}" class="img-responsive">
              </div>
            </div>
            <div class="form-group ">
              <label for="name" class="control-label col-lg-2">Name <span class="required">*</span></label>
              <div class="col-lg-4">
                @if ($errors->has('name'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first() }}
                </div>
                @endif
                <input class=" form-control" id="name" name="name" type="text"
                  value="{{ old('name') ?? $category->name }}" />
              </div>
              <label for="image" class="control-label col-lg-2">image <span class="required">*</span></label>
              <div class="col-lg-4">
                @if ($errors->has('image'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first() }}
                </div>
                @endif
                <input type="file" name="image" id="image" class="control-label">
              </div>
            </div>
            <div class="form-group ">
              <label for="description" class="control-label col-lg-2">Description <span
                  class="required">*</span></label>
              <div class="col-lg-10">
                @if ($errors->has('description'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first() }}
                </div>
                @endif
                <textarea name="description" id="description"
                  class="form-control">{{ old('description') ?? $category->description}}</textarea>
              </div>
            </div>
            <div class="form-group">
              <div class="col-lg-offset-2 col-lg-10">
                <button class="btn btn-primary" type="submit">Save</button>
                <a class="btn btn-danger" href="{{ route('categories.index') }}">Cancel</a>
              </div>
            </div>
            {{ csrf_field() }}
            {{ method_field('PUT') }}
          </form>
        </div>
      </div>
    </section>
  </div>
</div>
{{--main content end--}}
@endsection