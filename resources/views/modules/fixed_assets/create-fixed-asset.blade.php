@extends('template')
@section('main-content') {{--overview start--}}
<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header"><i class="icon_genius"></i>Fixed Assets Module</h3>
    <ol class="breadcrumb">
      <li><i class="fa fa-home"></i><a href="{{ route('dashboard') }}">Dashboard</a></li>
      <li><i class="icon_genius"></i><a href="{{ route('fixed-assets.index') }}">Fixed Assets</a></li>
      <li><i class="fa fa-book"></i>Create Fixed Asset</li>
    </ol>
  </div>
</div>
{{--overview end--}} {{--main content start--}}
<div class="row">
  <div class="col-lg-12">
    <section class="panel">
      <header class="panel-heading">
        <h3>Fixed Asset Registration Format</h3>
      </header>
      <div class="panel-body">
        <div class="form">
          <form class="form-validate form-horizontal " id="register_form" method="POST"
            action="{{ route('fixed-assets.store') }}">
            @if (session('status'))
            <div class="alert alert-success col-lg-offset-2" role="alert">
              {{ session('status') }}
            </div>
            @endif
            <div class="form-group">
              <label for="code" class="control-label col-lg-2">Code <span class="required"> *</span></label>
              <div class="col-lg-2">
                @if ($errors->has('code'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first() }}
                </div>
                @endif
                <input class="form-control" type="text" value="{{ $code }}" readonly required />
                <input class="form-control" id="code" name="code" type="hidden" value="{{ old('code') ?? $fixedasset }}"
                  readonly required />
              </div>
              <label for="name" class="control-label col-lg-2">Storage cellar <span class="required"> *</span></label>
              <div class="col-lg-6">
                @if ($errors->has('storage_cellar_id'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first() }}
                </div>
                @endif
                <select name="storage_cellar_id" id="storage_cellar_id" class="form-control" required>
                  <option value="{{null}}"></option>
                  @foreach ($storage_cellars as $storage_cellar)
                  <option value="{{$storage_cellar->id}}">{{$storage_cellar->name}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="col-lg-offset-2 col-lg-10">
                <button class="btn btn-primary" type="submit">Save</button>
                <a class="btn btn-danger" href="{{ route('brands.index') }}">Cancel</a>
              </div>
            </div>
            {{ csrf_field() }}
          </form>
        </div>
      </div>
    </section>
  </div>
</div>
{{--main content end--}}
@endsection