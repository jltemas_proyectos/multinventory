{{-- {{dd('hola')}} --}}
@extends('template')
@section('main-content')
{{--overview start--}}
<div class="row">
  <div class="col-sm-12">
    <h3 class="page-header"><i class="icon_genius"></i>Fixed Assets Module</h3>
    <ol class="breadcrumb">
      <li><i class="fa fa-home"></i><a href="{{ route('dashboard') }}">Dashboard</a></li>
      <li><i class="icon_genius"></i>Fixed Assets</li>
    </ol>
  </div>
</div>
{{--overview end--}}
{{--main content start--}}
<div class="row">
  <div class="col-sm-12">
    <div class="row">
      <div class="col-sm-12">
        <section class="panel">
          <div class="table-responsive">
            <table class="table  table-advance table-hover ">
              <caption class="page-header-table">Fixed Assets</caption>
              <tbody class="form-horizontal">
                <tr class="row">
                  <th class="col-sm-1 center"> N°</th>
                  <th class="col-sm-2 center"><i class="icon_genius"></i> Code</th>
                  <th class="col-sm-2 "><i class="fa fa-archive"></i> Storage Cellar</th>
                  <th class="col-sm-6"><i class="icon_cogs"></i> Action</th>
                </tr>
                @foreach ($fixedassets as $key => $fixedasset)
                
                @switch($fixedasset->status)
                @case(1)
                <tr class="row success">
                  <td class="col-sm-1 center">{{ $fixedasset->id - 1}}</td>
                  <td class="col-sm-1 center">{{ $codes[$key]}}</td>
                  <td class="col-sm-3 ">{{ $fixedasset->storageCellar->name }}</td>
                  <td class="col-sm-6">
                    <div class="btn-group" role="group">
                      <a class="btn btn-primary" href="{{ route('fixed-assets.edit', $fixedasset->id) }}"><i
                          class="icon_plus_alt2"></i></a>
                      <a class="btn btn-success" href="{{ route('fixed-assets.show', $fixedasset->id) }}"><i
                          class="icon_check_alt2"></i></a>
                      <form class="form-delete" method="POST"
                        action="{{ route('fixed-assets.destroy', $fixedasset->id) }}">
                        <button class="btn btn-danger form-delete-button" type="submit"><i
                            class="icon_close_alt2"></i></button>
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                      </form>
                    </div>
                  </td>
                </tr>
                @break
                @case(2)
                <tr class="row warning">
                  <td class="col-sm-1 center">{{ $fixedasset->id - 1}}</td>
                  <td class="col-sm-1 center">{{ $codes[$key]}}</td>
                  <td class="col-sm-3 ">{{ $fixedasset->storageCellar->name }}</td>
                  <td class="col-sm-3">
                    <div class="btn-group">
                      <a class="btn btn-primary" href="{{ route('fixed-assets.edit', $fixedasset->id) }}"><i
                          class="icon_plus_alt2"></i></a>
                      <a class="btn btn-success" href="{{ route('fixed-assets.show', $fixedasset->id) }}"><i
                          class="icon_check_alt2"></i></a>
                      <form class="form-delete" method="POST"
                        action="{{ route('fixed-assets.destroy', $fixedasset->id) }}">
                        <button class="btn btn-danger form-delete-button" type="submit"><i
                            class="icon_close_alt2"></i></button>
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                      </form>
                    </div>
                  </td>
                </tr>
                @break
                @default
                <tr class="row danger">
                  {{-- <td class="col-sm-1 center">{{ $fixedasset->id - 1}}</td> --}}
                  <td class="col-sm-1 center">{{ $key + 1}}</td>
                  <td class="col-sm-1 center">{{ $codes[$key]}}</td>
                  <td class="col-sm-3 ">{{ $fixedasset->storageCellar->name }}</td>
                  <td class="col-sm-3">
                    <div class="btn-group">
                      <a class="btn btn-primary" href="{{ route('fixed-assets.edit', $fixedasset->id) }}"><i
                          class="icon_plus_alt2"></i></a>
                      <a class="btn btn-success" href="{{ route('fixed-assets.show', $fixedasset->id) }}"><i
                          class="icon_check_alt2"></i></a>
                      {{-- <a class="btn btn-info" href="{{ route('fixed-assets.show', $fixedasset->id) }}"><i
                          class="fa fa-refresh"></i></a> --}}
                      <form class="form-delete" method="POST"
                        action="{{ route('fixed-assets.destroy', $fixedasset->id) }}">
                        <button class="btn btn-danger form-delete-button" type="submit"><i
                            class="icon_close_alt2"></i></button>
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                      </form>
                    </div>
                  </td>
                </tr>
                @endswitch
                
                @endforeach
              </tbody>
            </table>
            {{-- paginación --}}
            <div class="row">
              <div class="col-sm-offset-5">
                {{$fixedassets->render() }}
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
</div>
{{--main content end--}}
@endsection