@extends('template')
@section('main-content')
{{--overview start--}}
<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header"><i class="icon_genius"></i>Fixed Assets Module</h3>
    <ol class="breadcrumb">
      <li><i class="fa fa-home"></i><a href="{{ route('dashboard') }}">Dashboard</a></li>
      <li><i class="icon_genius"></i><a href="{{ route('fixed-assets.index') }}">Fixed Assets</a></li>
      <li><i class="fa fa-book"></i>Update Fixed Asset</li>
    </ol>
  </div>
</div>
{{--overview end--}}
{{--main content start--}}
<div class="row">
  <div class="col-lg-12">
    <section class="panel">
      <header class="panel-heading">
        <h3>Fixed Asset Update Format</h3>
      </header>
      <div class="panel-body">
        <div class="form">
          <form class="form-validate form-horizontal " id="register_form" method="POST"
            action="{{ route('fixed-assets.update', $fixedasset->id) }}">
            @if (session('status'))
            <div class="alert alert-success col-lg-offset-2" role="alert">
              {{ session('status') }}
            </div>
            @endif
            <div class="form-group ">
              <label for="code" class="control-label col-lg-2">Fixed Asset Code<span class="required">*</span></label>
              <div class="col-lg-2">
                @if ($errors->has('code'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first() }}
                </div>
                @endif
                <input class=" form-control" id="code" type="number" value="{{ $code }}" readonly />
                <input class=" form-control" id="code" name="code" type="hidden" value="{{ (Int) $code }}" readonly />
              </div>
              <label for="storage_cellar_id" class="control-label col-lg-2">Storage Cellar<span
                  class="required">*</span></label>
              <div class="col-lg-4">
                @if ($errors->has('storage_cellar_id'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first() }}
                </div>
                @endif
                <select class=" form-control" id="storage_cellar_id" name="storage_cellar_id">
                  <option></option>
                  @foreach ($storagecs as $storagec)
                  @if ($storagec->id === $fixedasset->storage_cellar_id)
                  <option value="{{ $storagec->id }}" selected>{{ $storagec->name }}</option>
                  @continue
                  @endif
                  <option value="{{ $storagec->id }}">{{ $storagec->name }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="col-lg-offset-2 col-lg-10">
                <button class="btn btn-primary" type="submit">Save</button>
                <a class="btn btn-danger" href="{{ route('fixed-assets.index') }}">Cancel</a>
              </div>
            </div>
            {{ csrf_field() }}
            {{ method_field('PUT') }}
          </form>
        </div>
      </div>
    </section>
  </div>
</div>
{{--main content end--}}
@endsection