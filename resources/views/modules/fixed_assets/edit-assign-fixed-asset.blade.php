@extends('template')
@section('main-content') {{--overview start--}}
<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header"><i class="icon_genius"></i>Fixed Assets Module</h3>
    <ol class="breadcrumb">
      <li><i class="fa fa-home"></i><a href="{{ route('dashboard') }}">Dashboard</a></li>
      <li><i class="icon_genius"></i><a href="{{ route('fixed-assets.index') }}">Fixed Assets</a></li>
      <li><i class="fa fa-book"></i>Fixed Asset Assign Form</li>
    </ol>
  </div>
</div>
{{--overview end--}}
{{--main content start--}}
<div class="row">
  <div class="col-lg-12">
    <section class="panel">
      <header class="panel-heading">
        <h3>Items Assignment Update Form For Fixed Assets</h3>
      </header>
      <div class="panel-body">
        <div class="form">
          <div>
            <select class="form-control col-lg-3 articles" id="article" name="articles[]" style="visibility:hidden">
              <option></option>
              @foreach ($articles as $article)
              <option value="{{ $article->id }}">{{ $article->name }}</option>
              @endforeach
            </select>
          </div>
          <form class="form-validate form-horizontal" id="register_form" method="POST"
            action="{{ route('fixed-assets.update-assign-fixed-asset', $fixedasset->code)}}">
            @if (session('status'))
            <div class="alert alert-success col-lg-offset-2" role="alert">
              {{ session('status') }}
            </div>
            @endif
            <div class="form-group">
              <label for="code" class="control-label col-lg-2">Fixed Asset Code<span class="required">
                  *</span></label>
              <div class="col-lg-4">
                <input type="text" class="form-control" required readonly value="{{$code}}">
                @if ($errors->has('code'))
                <div class="alert alert-inventory alert-danger" role="alert">
                  {{ $errors->first() }}
                </div>
                @endif
                <input type="hidden" id="code" name="code" required readonly value="{{$fixedasset->code}}">
              </div>
              <label for="storage_cellar_id" class="control-label col-lg-2">Storage Cellar<span class="required">
                  *</span></label>
              <div class="col-lg-4">
                <select class=" form-control" id="storage_cellar_id" name="storage_cellar_id" required>
                  <option></option>
                  @foreach ($storagecs as $storagec)
                  @if ($storagec->id===$fixedasset->storage_cellar_id)
                  <option value="{{$fixedasset->storage_cellar_id}}" selected>{{$fixedasset->storageCellar->name}}
                  </option>
                  @continue
                  @else
                  <option value="{{ $storagec->id }}">{{ $storagec->name }}</option>
                  @endif
                  @endforeach
                </select>
                @if ($errors->has('storage_cellar_id'))
                <div class="alert alert-inventory alert-danger" role="alert">
                  {{ $errors->first() }}
                </div>
                @endif
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12">
                <div class="table-responsive top-line bottom-line">
                  <table class="table table-bordered" id="table_detail">
                    <caption class="page-header-table">Articles</caption>
                    <thead>
                      <th>Article</th>
                      <th>Brand</th>
                      <th>Model</th>
                      <th>Quantity</th>
                      <th>Serial</th>
                      <th>Fixed Asset</th>
                      <th>Price</th>
                      <th>Action</th>
                    </thead>
                    <tbody id="content-table">
                      @foreach ($fixedasset->articles as $F_article)
                      <tr class="row-js">
                        <td class="col-lg-2">
                          <input type="text" class="form-control col-lg-1" readonly id="models"
                            value="{{$F_article->name}}" required>
                          <input type="hidden" class="form-control col-lg-1" readonly id="models" name="articles[]"
                            value="{{$F_article->id}}" required>
                        </td>
                        <td class="col-lg-2"><input type="text" class="form-control col-lg-1" readonly id="brands"
                            name="brands[]" value="{{$F_article->brand}}" required></td>
                        <td><input type="text" class="form-control col-lg-1" readonly id="models" name="models[]"
                            value="{{$F_article->model}}" required></td>
                        <td><input type="text" class="form-control col-lg-1" readonly id="quantity" name="quantities[]"
                            value="{{$F_article->quantity}}" required></td>
                        <td><input type="text" class="form-control col-lg-1" readonly id="serial" name="serials[]"
                            value="{{$F_article->serial}}" required></td>
                        <td><input type="text" class="form-control col-lg-1" readonly id="fixed" name="fixeds[]"
                            value="{{$fixedasset->code}}" required></td>
                        <td><input type="text" class="form-control col-lg-1 articles-price-js" readonly id="price"
                            name="prices[]" value="{{$F_article->price}}" required></td>
                        <td id="col-btn-delete"><a href="#" class="btn btn-danger"><i class="icon_close_alt2"></i></a>
                        </td>
                      </tr>
                      <input type="hidden" value="{{$total += $F_article->price}}">
                      @endforeach
                    </tbody>
                    <tfoot>
                      <tr>
                        <td colspan="6" class="center price">Total</td>
                        <td class="price"><input type="text" class="form-control col-lg-1" name="" id="total_edit_asign"
                            value="{{$total}}" readonly></td>
                      </tr>
                    </tfoot>
                  </table>
                  <button type="button" class="btn btn-success col-lg-offset-10" id="addArticle">Add Article</button>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="col-lg-10 margin-top">
                <button class="btn btn-primary" type="submit">Save</button>
                <a class="btn btn-danger" href="{{ route('fixed-assets.index') }}">Cancel</a>
              </div>
            </div>
            <input type="hidden" name="user" value="{{Auth::user()->id}}">
            {{ csrf_field() }}
            {{ method_field('PUT') }}
          </form>
        </div>
      </div>
    </section>
  </div>
</div>
{{--main content end--}}
@endsection
@section('js')
<script src="{{asset('js/myscript3.js')}}"></script>
@endsection