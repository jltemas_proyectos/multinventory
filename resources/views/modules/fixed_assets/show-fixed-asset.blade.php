@extends('template')
@section('main-content')
{{--overview start--}}
<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header"><i class="icon_documents_alt"></i>Fixed Asset N° {{ $code }}</h3>
    <ol class="breadcrumb">
      <li><i class="fa fa-home"></i><a href="{{ route('dashboard') }}">Dashboard</a></li>
      <li><i class="icon_documents_alt"></i><a href="{{ route('fixed-assets.index') }}">Fixed Assets</a></li>
      <li><i class="fa fa-file-text-o"></i>Fixed Asset N° {{ $code }}</li>
    </ol>
  </div>
</div>
{{--overview end--}}
{{--main content start--}}
<div class="row">
  <div class="col-lg-12">
    <div class="panel">
      <div class="panel-body ">
        <header class="row margin-bottom bottom-line">
          <div class="col-lg-2">
            <img class="img-responsive" src="{{asset('img/multi.png')}}" alt="">
          </div>
          <div class="col-lg-10">
            <div class="row">
              <div class="col-lg-7 col-lg-push-1">
                <h3 class="page-header title center">Multipartes Sas</h3>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-7 col-lg-push-1">
                <div class="form-horizontal">
                  <div class="">
                    <p class="page-header sub-title center">Fixed Asset N° <span>{{ $code }}</span></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </header>
        <div class="row">
          <div class="col-lg-6 page-header">
            <div class="form-horizontal">
              <div class="form-group">
                <span class="col-lg-4 col-sm-3">Date Create</span>
                <span class="col-sm-1">:</span>
                <p class="col-lg-5 visible-sm">{{ $fixedasset->created_at }}</p>
                <span class="col-sm-5 hidden-md hidden-sm ">{{ $fixedasset->created_at }}</span>
              </div>
            </div>
          </div>
          <div class="col-lg-6 page-header">
            <div class="form-horizontal">
              <div class="form-group">
                <span class="col-lg-4 col-sm-3">Date Update</span>
                <span class="col-sm-1">:</span>
                <p class="col-lg-5 visible-sm">{{ $fixedasset->updated_at }}</p>
                <span class="col-sm-5 hidden-md hidden-sm ">{{ $fixedasset->updated_at }}</span>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <div class="table-responsive top-line bottom-line">
              <table class="table table-bordered ">
                <caption class="page-header-table">Description</caption>
                <thead>
                  <th>Fixed Asset</th>
                  <th>Article</th>
                  <th>Brand</th>
                  <th>Serial</th>
                  <th>Quantity</th>
                  <th>Cost</th>
                </thead>
                <tbody>
                  @foreach ($articles as $article)
                  <tr>
                    <td>{{$code}}</td>
                    <td>{{$article->name}}</td>
                    <td>{{$article->brand}}</td>
                    <td>{{$article->serial}}</td>
                    <td>{{$article->quantity}}</td>
                    <td>{{$article->price}}</td>
                  </tr>
                  @php $total += $article->price @endphp
                  @endforeach
                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="5" class="center">Total</td>
                    <td>{{$total}}</td>
                  </tr>
                </tfoot>
              </table>
              <a class="btn btn-primary btn-block"
                href="{{ route('fixed-assets.edit-assign-fixed-asset', $fixedasset->id) }}">Edit</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
{{--main content end--}}
@endsection