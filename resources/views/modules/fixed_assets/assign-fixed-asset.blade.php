@extends('template')
@section('main-content') {{--overview start--}}
<div class="row">
  <div class="col-sm-12">
    <h3 class="page-header"><i class="icon_genius"></i>Fixed Assets Module</h3>
    <ol class="breadcrumb">
      <li><i class="fa fa-home"></i><a href="{{ route('dashboard') }}">Dashboard</a></li>
      <li><i class="icon_genius"></i><a href="{{ route('fixed-assets.index') }}">Fixed Assets</a></li>
      <li><i class="fa fa-book"></i>Fixed Asset Assign Form</li>
    </ol>
  </div>
</div>
{{--overview end--}} {{--main content start--}}
<div class="row">
  <div class="col-sm-12">
    <section class="panel">
      <header class="panel-heading">
        <h3>Items Assignment Form For Fixed Assets</h3>
      </header>
      <div class="panel-body">
        <div class="form">
          <div id="selectOptions" style="display:none">
            <option></option>
            @foreach ($articles as $article)
            <option value="{{ $article->id }}">{{ $article->name }}</option>
            @endforeach
          </div>
          <form class="form-validate form-horizontal" id="asign_form" method="POST"
            action="{{ route('fixed-assets.assign-fixed-asset') }}">
            @if (session('status'))
            <div class="alert alert-success col-sm-offset-2" role="alert">
              {{ session('status') }}
            </div>
            @endif
            <div class="form-group">
              <label for="code" class="control-label col-sm-2 ">Fixed Asset Code<span class="required"> *</span></label>
              <div class="col-sm-4">
                @if ($errors->has('code'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first() }}
                </div>
                @endif
                <select name="code" id="code" class="form-control" required>
                  <option value=""></option>
                  @foreach ($fixedassets as $key => $fixedasset)
                  <option value="{{$fixedasset->code}}">{{$codesFasset[$key]}}</option>
                  @endforeach
                </select>
                {{-- <input class=" form-control" id="code" name="code" type="hidden" value="{{ ++$fixedasset }}"
                readonly /> --}}
              </div>
              <label for="storage_cellar_id" class="control-label col-sm-2">Storage Cellar<span class="required">
                  *</span></label>
              <div class="col-sm-4">
                @if ($errors->has('storage_cellar_id'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first() }}
                </div>
                @endif
                <select class=" form-control" id="storage_cellar_id" name="storage_cellar_id" required>
                  <option></option>
                  @foreach ($storagecs as $storagec)
                  <option value="{{ $storagec->id }}">{{ $storagec->name }} </option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
                <div class="table-responsive top-line bottom-line">
                  <table class="table table-bordered ">
                    <caption class="page-header-table">Articles</caption>
                    <thead>
                      <th>Article</th>
                      <th>Brand</th>
                      <th>Model</th>
                      <th>Quantity</th>
                      <th>Serial</th>
                      <th>Fixed Asset</th>
                      <th>Price</th>
                      <th>Action</th>
                    </thead>
                    <tbody id="content-table">
                      <tr class="row-js">
                        <td class="col-sm-2">
                          <select class="form-control col-sm-3 articles" id="article" name="articles[]" required>
                            <option></option>
                            @foreach ($articles as $article)
                            <option value="{{ $article->id }}">{{ $article->name }} - {{ $article->serial }}</option>
                            @endforeach
                          </select>
                        </td>
                        <td class="col-sm-2"><input type="text" class="form-control col-sm-1" id="brands"
                            name="brands[]" required></td>
                        <td class="col-sm-2"><input type="text" class="form-control col-sm-1" id="models"
                            name="models[]" required></td>
                        <td><input type="text" class="form-control col-sm-1" id="quantity" name="quantities[]" required>
                        </td>
                        <td><input type="text" class="form-control col-sm-1" id="serial" name="serials[]" required></td>
                        <td>
                          <input type="text" class="form-control col-sm-1 js-fixed" id="fixedContent" value="" required>
                          <input type="hidden" class="js-fixedHidden" name="fixeds[]" required>
                        </td>
                        <td><input type="text" class="form-control col-sm-1 articles-price-js" id="price"
                            name="prices[]" required></td>
                        <td id="col-btn-delete"></td>
                      </tr>
                    </tbody>
                    <tfoot>
                      <tr>
                        <td colspan="6" class="center price">Total</td>
                        <td colspan="" class="price" id="total">0.00</td>
                      </tr>
                    </tfoot>
                  </table>
                  <button type="button" class="btn btn-success col-sm-offset-10" id="addArticle">Add Article</button>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-10 margin-top">
                <button class="btn btn-primary" type="submit">Assign</button>
                <a class="btn btn-danger" href="{{ route('fixed-assets.index') }}">Cancel</a>
              </div>
            </div>
            <input type="hidden" name="user" value="{{Auth::user()->id}}">
            {{ csrf_field() }}
          </form>
          <div id="midata"></div>
        </div>
      </div>
    </section>
  </div>
</div>
<ul id="lista"></ul>
{{--main content end--}}
@endsection
@section('js')
<script src="{{asset('js/myscript.js')}}"></script>
@endsection