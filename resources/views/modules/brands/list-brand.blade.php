@extends('template')
@section('main-content')
{{--overview start--}}
<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header"><i class="fa fa-linode" aria-hidden="true"></i>Brands Module</h3>
    <ol class="breadcrumb">
      <li><i class="fa fa-home" aria-hidden="true"></i><a href="{{ route('dashboard') }}">Dashboard</a></li>
      <li><i class="fa fa-linode" aria-hidden="true"></i>Brands</li>
    </ol>
  </div>
</div>
{{--overview end--}}
{{--main content start--}}
<div class="row">
  <div class="col-lg-12">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <div class="table-responsive">
            <table class="table table-striped table-advance table-hover">
              <caption class="page-header-table">Brands</caption>
              <tbody class="form-horizontal">
                <tr class="form-group">
                  <th class="col-lg-2 col-sm-2 center"> N°</th>
                  <th class="col-lg-8 col-sm-8 center"><i class="fa fa-linode" aria-hidden="trues"></i> Name</th>
                  <th class="col-lg-2 col-sm-2"><i class="icon_cogs"></i> Action</th>
                </tr>
                @foreach ($brands as $brand)
                @if (!$brand->deleted_at)
                <tr class="form-group btn-group-sm">
                  <td class="col-lg-1 col-sm-1 center">{{ $count }}</td>
                  <td class="col-lg-3 col-sm-2 center">{{ $brand->name }}</td>
                  <td class="col-sm-3">
                    <div class="btn-group btn-group-sm">
                      <a class="btn btn-primary" href="{{ route('brands.edit', $brand->id) }}"><i
                          class="icon_plus_alt2"></i></a>
                      <form class="form-delete" method="POST" action="{{ route('brands.destroy', $brand->id) }}">
                        <button class="btn btn-sm btn-danger form-delete-button" type="submit"><i
                            class="icon_close_alt2"></i></button>
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                      </form>
                    </div>
                  </td>
                </tr>
                @php $count++ @endphp
                @endif
                @endforeach
              </tbody>
            </table>
          </div>
        </section>
      </div>
    </div>
  </div>
</div>
{{--main content end--}}
@endsection