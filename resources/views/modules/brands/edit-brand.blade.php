@extends('template')
@section('main-content')
{{--overview start--}}
<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header"><i class="fa fa-linode"></i>Brands Module</h3>
    <ol class="breadcrumb">
      <li><i class="fa fa-home" aria-hidden="true"></i><a href="{{ route('dashboard') }}">Dashboard</a></li>
      <li><i class="fa fa-linode "></i><a href="{{ route('brands.index') }}">Brands</a></li>
      <li><i class="fa fa-cubes"></i>Update Brand</li>
    </ol>
  </div>
</div>
{{--overview end--}}
{{--main content start--}}
<div class="row">
  <div class="panel-body panel-body-j">
    <section class="panel">
      <header class="panel-heading">
        <h3>Brand Update Form</h3>
      </header>
      <div class="panel-body">
        <div class="form">
          <form class="form-validate form-horizontal " id="register_form" method="POST"
            action="{{ route('brands.update', $brand->id) }}">
            @if (session('status'))
            <div class="alert alert-success col-lg-offset-2" role="alert">
              {{ session('status') }}
            </div>
            @endif
            <div class="form-group ">
              <label for="name" class="control-label col-lg-2">Name <span class="required">*</span></label>
              <div class="col-lg-10">
                @if ($errors->has('name'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first() }}
                </div>
                @endif
                <input class=" form-control" id="name" name="name" type="text"
                  value="{{ old('name') ?? $brand->name }}" />
              </div>
            </div>
            <div class="form-group">
              <div class="col-lg-offset-2 col-lg-10">
                <button class="btn btn-primary" type="submit">Save</button>
                <a class="btn btn-danger" href="{{ route('brands.index') }}">Cancel</a>
              </div>
            </div>
            {{ method_field('PUT') }}
            {{ csrf_field() }}
          </form>
        </div>
      </div>
    </section>
  </div>
</div>
{{--main content end--}}
@endsection