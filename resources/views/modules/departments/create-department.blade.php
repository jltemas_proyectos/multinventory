@extends('template')
@section('main-content')
{{--overview start--}}
<div class="row">
  <div class="col-sm-12">
    <h3 class="page-header"><i class="fa fa-sitemap"></i>Departments Module</h3>
    <ol class="breadcrumb">
      <li><i class="fa fa-home"></i><a href="{{ route('dashboard') }}">Dashboard</a></li>
      <li><i class="fa fa-sitemap"></i><a href="{{ route('departments.index') }}">Departments</a></li>
      <li><i class="fa fa-steam"></i>Create Department</li>
    </ol>
  </div>
</div>
{{--overview end--}}
{{--main content start--}}
<div class="row">
  <div class="col-sm-12">
    <section class="panel">
      <header class="panel-heading">
        <h3>Department Registration Form</h3>
      </header>
      <div class="panel-body">
        <div class="form">
          <form class="form-validate form-horizontal " id="register_form" method="POST"
            action="{{ route('departments.store') }}">
            @if (session()->has('status'))
            <div class="alert alert-success col-sm-10 col-sm-offset-2 ">
              {{ session('status') }}
            </div>
            @endif
            <div class="form-group ">
              <label for="name" class="control-label col-sm-2">Name <span class="required">*</span></label>
              <div class="col-sm-4">
                @if ( $errors->has('name'))
                <div class="alert alert-danger" role="alert">
                  {{$errors->first('name')}}
                </div>
                @endif
                <input class=" form-control" id="name" name="name" type="text" value="{{ old('name')}}" required />
              </div>
            </div>
            <div class="form-group ">
              <label for="description" class="control-label col-sm-2">Description <span
                  class="required">*</span></label>
              <div class="col-sm-10">
                @if ( $errors->has('description'))
                <div class="alert alert-danger" role="alert">
                  {{$errors->first('description')}}
                </div>
                @endif
                <textarea name="description" id="description" class="form-control"
                  required>{{ old('description')}}</textarea>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button class="btn btn-primary" type="submit">Save</button>
                <a class="btn btn-danger" href="{{ route('departments.index') }}">Cancel</a>
              </div>
            </div>
            {{ csrf_field()}}
          </form>
        </div>
      </div>
    </section>
  </div>
</div>
{{--main content end--}}
@endsection