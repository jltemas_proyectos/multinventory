@extends('template')
@section('main-content')
{{--overview start--}}
<div class="row">
  <div class="col-sm-12">
    <h3 class="page-header"><i class="fa fa-handshake-o"></i>Positions Module</h3>
    <ol class="breadcrumb">
      <li><i class="fa fa-home"></i><a href="{{ route('dashboard') }}">Dashboard</a></li>
      <li><i class="fa fa-handshake-o"></i><a href="{{ route('positions.index') }}">Positions</a></li>
      <li><i class="fa fa-black-tie"></i>Update Positions</li>
    </ol>
  </div>
</div>
{{--overview end--}}
{{--main content start--}}
<div class="row">
  <div class="col-sm-12">
    <section class="panel">
      <header class="panel-heading">
        <h3>Positions Update Form</h3>
      </header>
      <div class="panel-body">
        <div class="form">
          <form class="form-validate form-horizontal " id="update_form" method="POST"
            action="{{ route('positions.update', $position->id) }}">
            @if (session()->has('status'))
            <div class="alert alert-success col-sm-offset-2" role="alert">
              {{ session('status') }}
            </div>
            @endif
            <div class="form-group ">
              <label for="name" class="control-label col-sm-2">Name <span class="required">*</span></label>
              <div class="col-sm-4">
                @if ($errors->has('name'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first('name') }}
                </div>
                @endif
                <input class=" form-control" id="name" name="name" type="text"
                  value="{{ old('name') ?? $position->name }}" required />
              </div>
            </div>
            <div class="form-group ">
              <label for="description" class="control-label col-sm-2">Description <span
                  class="required">*</span></label>
              <div class="col-sm-10">
                @if ($errors->has('description'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first('description') }}
                </div>
                @endif
                <textarea name="description" id="description" class="form-control"
                  required>{{ old('description') ?? $position->description  }}</textarea>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button class="btn btn-primary" type="submit">Update</button>
                <a class="btn btn-danger" href="{{ route('positions.index') }}">Cancel</a>
              </div>
            </div>
            {{ csrf_field() }}
            {{ method_field('PUT') }}
          </form>
        </div>
      </div>
    </section>
  </div>
</div>
{{--main content end--}}
@endsection