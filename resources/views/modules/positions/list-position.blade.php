@extends('template')
@section('main-content')
{{--overview start--}}
<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header"><i class="fa fa-handshake-o" aria-hidden="true"></i>Positions Module</h3>
    <ol class="breadcrumb">
      <li><i class="fa fa-home" aria-hidden="true"></i><a href="{{ route('dashboard') }}">Dashboard</a></li>
      <li><i class="fa fa-handshake-o" aria-hidden="true"></i> Positions</li>
    </ol>
  </div>
</div>
{{--overview end--}}
{{--main content start--}}
<div class="row">
  <div class="col-lg-12">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <div class="table-responsive">
            <table class="table table-striped table-advance table-hover">
              <caption class="page-header-table">Positions</caption>
              <tbody class="form-horizontal">
                <tr class="form-group">
                  <th class="col-sm-1 center"> N°</th>
                  <th class="col-sm-2"><i class="fa fa-handshake-o" aria-hidden="trues"></i> Name</th>
                  <th class="col-sm-5"><i class="fa fa-file-text-o"></i> Description</th>
                  <th class="col-sm-4"><i class="icon_cogs"></i> Action</th>
                </tr>
                @foreach ($positions as $position)
                @if (!$position->delected_at)
                <tr class="form-group">
                  <td class="col-sm-1 center">{{ ($position->id) }}</td>
                  <td class="col-sm-2">{{ $position->name }}</td>
                  <td class="col-sm-5">{{ $position->description }}</td>
                  <td class="col-sm-4">
                    <div class="btn-group btn-group-sm">
                      <a class="btn btn-primary" href="{{ route('positions.edit', $position->id) }}"><i
                          class="icon_plus_alt2"></i></a>
                      <form class="form-delete" method="POST" action="{{ route('positions.destroy', $position->id) }}">
                        <button class="btn btn-sm btn-danger form-delete-button" type="submit"><i
                            class="icon_close_alt2"></i></button>
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                      </form>
                    </div>
                  </td>
                </tr>
                @endif
                @endforeach
              </tbody>
            </table>
            {{-- Start paginación --}}
            <div class="row">
              <div class="col-sm-offset-5">
                {{$positions->render() }}
              </div>
            </div>
            {{-- Start paginación --}}
          </div>
        </section>
      </div>
    </div>
  </div>
</div>
{{--main content end--}}
@endsection