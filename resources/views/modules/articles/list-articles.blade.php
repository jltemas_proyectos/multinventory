@extends('template')
@section('main-content')
{{--overview start--}}
<div class="row">
  <div class="col-md-12">
    <h3 class="page-header"><i class="fa fa-desktop" aria-hidden="true"></i>Articles Module</h3>
    <ol class="breadcrumb">
      <li><i class="fa fa-home" aria-hidden="true"></i><a href="{{ route('dashboard') }}">Dashboard</a></li>
      <li><i class="fa fa-laptop" aria-hidden="true"></i>Articles</li>
    </ol>
  </div>
</div>
{{--overview end--}}
{{--main content start--}}
<div class="row">
  <div class="col-md-12">
    <div class="row">
      <div class="col-md-12">
        <section class="panel">
          <div class="table-responsive">
            <table class="table table-striped table-advance table-hover">
              <caption class="page-header-table">Articles</caption>
              <tbody class="form-horizontal">
                <tr class="form-group">
                  <th class="col-sm-1  center"> N°</th>
                  <th class="col-sm-2 "><i class="fa fa-desktop" aria-hidden="trues"></i> Name</th>
                  <th class="col-sm-2 center"><i class="fa fa-file-text-o"></i> Serial</th>
                  <th class="col-sm-2 center"><i class="fa fa-file-text-o"></i> Quantity</th>
                  <th class="col-sm-2 "><i class="icon_genius"></i> Fixed Asset</th>
                  <th class="col-sm-3 "><i class="icon_cogs"></i> Action</th>
                </tr>
                @foreach ($articles as $article)
                @if (!$article->deleted_at)
                <tr class="form-group">
                  <td class="col-sm-1  center">{{ $count }}</td>
                  <td class="col-sm-2 ">{{ $article->name }}</td>
                  <td class="col-sm-2 center">{{ $article->serial }}</td>
                  <td class="col-sm-2 center">{{ $article->quantity }}</td>
                  <td class="col-sm-2 ">{{ $article->fixed->code === 0 ? '' : $article->fixed->code }}</td>
                  <td class="col-sm-3">
                    <div class="btn-group btn-group-sm btn-group-xs">
                      <a class="btn btn-primary" href="{{ route('articles.edit',$article->id) }}"><i
                          class="icon_plus_alt2"></i></a>
                      <a class="btn btn-success" href="{{ route('articles.show',$article->id) }}"><i
                          class="icon_check_alt2"></i></a>
                      <form class="form-delete" method="POST" action="{{ route('articles.destroy', $article->id) }}">
                        <button class="btn btn-xs btn-sm btn-danger form-delete-button" type="submit"><i
                            class="icon_close_alt2"></i></button>
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                      </form>
                    </div>
                  </td>
                </tr>
                @endif
                @php
                $count++;
                @endphp
                @endforeach
              </tbody>
            </table>
            {{-- Start paginación --}}
            <div class="row">
              <div class="col-md-offset-5">
                {{$articles->links() }}
              </div>
            </div>
            {{-- Start paginación --}}
          </div>
        </section>
      </div>
    </div>
  </div>
</div>
{{--main content end--}}
@endsection