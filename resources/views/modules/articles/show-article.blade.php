@extends('template')
@section('main-content')
{{--overview start--}}
<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header"><i class="fa fa-desktop" aria-hidden="true"></i>Article Profile</h3>
    <ol class="breadcrumb">
      <li><i class="fa fa-home" aria-hidden="true"></i><a href="{{ route('dashboard') }}">Dashboard</a></li>
      <li><i class="fa fa-laptop" aria-hidden="true"></i><a href="{{ route('articles.index') }}">Articles</a></li>
      <li><i class="fa fa-hdd-o" aria-hidden="true"></i>Show Article</li>
    </ol>
  </div>
</div>
{{--overview end--}}
{{--main content start--}}
<div class="row">
  <div class="col-lg-12">
    {{-- profile-widget start--}}
    <div class="row">
      <div class="col-lg-12">
        <div class="profile-widget profile-widget-info">
          <div class="panel-body">
            <div class="col-lg-2 col-sm-2">
              <h4>{{ $article->name }}</h4>
              <div class="follow-ava">
                <img src='{{asset("$path/$article->image") }}' alt="{{$article->name}}"
                  class="img-rounded img-responsive">
              </div>
            </div>
            <div class="col-lg-4 col-sm-4 follow-info">
              <p><span>Marca: </span>{{ $article->brand }}</p>
              <p><span>Serial: </span>{{ $article->serial }}</p>
              <p><span>Description: </span>{{ $article->description }}</p>
            </div>
            <div class="col-lg-2 col-sm-6 follow-info weather-category">
              <ul>
                <li class="active">
                  <span class="block sub-title-show-article">Category</span>
                  <i class="block fa fa-tachometer fa-2x"></i>
                  <span>{{ $article->category->name }}</span>
                </li>
              </ul>
            </div>
            <div class="col-lg-2 col-sm-6 follow-info weather-category">
              <ul>
                <li class="active">
                  <span class="block sub-title-show-article">Fixed Asset</span>
                  <i class="block icon_genius fa-2x"></i>
                  <span>{{ $article->fixedasset_code != 1 ? $article->fixed->code : 'No Asignado' }}</span>
                </li>
              </ul>
            </div>
            <div class="col-lg-2 col-sm-6 follow-info weather-category">
              <ul>
                <li class="active">
                  <span class="block sub-title-show-article">Employee</span>
                  <i class="block fa fa-user-circle fa-2x"></i>
                  <span>{{ $article->fixed->employees->count() ? $article->fixed->employees[0]->first_name ." ".$article->fixed->employees[0]->first_lastname : "Sin Asignar"}}</span>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    {{-- profile-widget end--}}
    {{-- page start --}}
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          {{--Start Tab headers menu --}}
          <header class="panel-heading tab-bg-info" id="none-border">
            <ul class="nav nav-tabs">
              @if ($errors->all() || session()->has('status'))
              <li class="">
                <a data-toggle="tab" href="#delivery-certificate">
                  <i class="icon-home"></i>
                  <span>Delivery Certificate</span>
                </a>
              </li>
              <li class="active">
                <a data-toggle="tab" href="#edit-profile">
                  <i class="icon-envelope"></i>
                  <span>Edit Article</span>
                </a>
              </li>
              @else
              <li class="active">
                <a data-toggle="tab" href="#delivery-certificate">
                  <i class="icon-home"></i>
                  <span>Delivery Certificate</span>
                </a>
              </li>
              <li class="">
                <a data-toggle="tab" href="#edit-profile">
                  <i class="icon-envelope"></i>
                  <span>Edit Article</span>
                </a>
              </li>
              @endif
            </ul>
          </header>
          {{--End Tab headers menu --}}
          <div class="panel-body">
            <div class="tab-content">
              {{-- Start Tab Delivery-certificate --}}
              @if ($errors->all() || session()->has('status'))
              <div id="delivery-certificate" class="tab-pane ">
                <div class="profile-activity">
                  <div class="act-time">
                    @foreach ($article->fixed->delivery as $delivery)
                    <div class="activity-body act-in">
                      <div class="text">
                        {{-- <i class="activity-img icon_documents_alt fa-2x"></i> --}}
                        <p class="attribution">
                          <a href="{{ route('delivery-certificates.show',$delivery->id) }}">
                            <i class="activity-img icon_documents_alt fa-2x"></i>
                            {{ $delivery->code}}
                          </a>
                          {{ $delivery->created_at }}
                        </p>
                        <p><span>Delivered By:
                          </span>{{ $delivery->user->employee->first_name.' '.$delivery->user->employee->first_lastname }}
                        </p>
                      </div>
                    </div>
                    @endforeach
                  </div>
                </div>
              </div>
              {{-- delivery-certificate end --}}
              {{--edit-profile start--}}
              <div id="edit-profile" class="tab-pane active">
                <section class="panel">
                  <div class="panel-body bio-graph-info">
                    <h1> Article Info</h1>
                    <form class="form-validate form-horizontal " id="register_form" method="post"
                      action="{{ route('articles.update',$article->id)}}" enctype="multipart/form-data">
                      <div class="form-group">
                        <label for="image" class="control-label col-lg-2">Image <span class="required">*</span></label>
                        <div class="col-lg-4">
                          @if ($errors->has('image'))
                          <div class="alert alert-danger" role="alert">
                            {{ $errors->first('image') }}
                          </div>
                          @endif
                          <input class="control-label" id="image" name="image" type="file"
                            value="{{ old('image') ?? $article->image}}" enctype="multipar/form-data" />
                        </div>
                        {{--  Esta clase col-lg-offset-2 de bootstrap es la que le esta dando el espaciado a la imagen  --}}
                        <div class="col-lg-3 col-lg-offset-2">
                          <img src='{{ asset("storage/$article->image")}}' alt='{{ "$article->name"}}'
                            class="img-rounded img-responsive">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="control-label col-lg-2">Article name <span
                            class="required">*</span></label>
                        <div class="col-lg-4">
                          @if ($errors->has('name'))
                          <div class="alert alert-danger" role="alert">
                            {{ $errors->first() }}
                          </div>
                          @endif
                          <input class="form-control" id="name" name="name" type="text"
                            value="{{ old('name') ?? $article->name }}" required />
                        </div>
                        <label for="model" class="control-label col-lg-2">Model <span class="required">*</span></label>
                        <div class="col-lg-4">
                          @if ($errors->has('model'))
                          <div class="alert alert-danger" role="alert">
                            {{ $errors->first() }}
                          </div>
                          @endif
                          <input class="form-control" id="model" name="model" type="text"
                            value="{{ old('model') ?? $article->model }}" required />
                        </div>
                      </div>
                      <div class="form-group ">
                        <label for="serial" class="control-label col-lg-2">Serial <span
                            class="required">*</span></label>
                        <div class="col-lg-4">
                          @if ($errors->has('serial'))
                          <div class="alert alert-danger" role="alert">
                            {{ $errors->first() }}
                          </div>
                          @endif
                          <input class="form-control" id="serial" name="serial" type="text"
                            value="{{ old('serial') ?? $article->serial }}" required />
                        </div>
                        <label for="brandA" class="control-label col-lg-2">Brand <span class="required">*</span></label>
                        <div class="col-lg-4">
                          @if ($errors->has('brand'))
                          <div class="alert alert-danger" role="alert">
                            {{ $errors->first() }}
                          </div>
                          @endif
                          <input class="form-control" type="text" name="brand" id="brandA"
                            value="{{old('brand') ?? $article->brand}}" readonly>
                        </div>
                      </div>
                      <div class="form-group ">
                        <label for="category_id" class="control-label col-lg-2">Category <span
                            class="required">*</span></label>
                        <div class="col-lg-2">
                          @if ($errors->has('category_id'))
                          <div class="alert alert-danger" role="alert">
                            {{ $errors->first() }}
                          </div>
                          @endif
                          <select class="form-control m-bot15" name="category_id">
                            <option value=""></option>
                            @foreach ($categories as $category)
                            @if ($category->id === $article->category->id)
                            <option selected value="{{ $category->id }}">{{ $category->name }}</option>
                            @continue
                            @endif
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                          </select>
                        </div>
                        <label for="quantity" class="control-label col-lg-2">Quantity <span
                            class="required">*</span></label>
                        <div class="col-lg-2">
                          @if ($errors->has('quantity'))
                          <div class="alert alert-danger" role="alert">
                            {{ $errors->first() }}
                          </div>
                          @endif
                          <input class="form-control" id="quantity" name="quantity" type="text"
                            value="{{ old('quantity') ?? $article->quantity }}" required />
                        </div>
                        <label for="price" class="control-label col-lg-2">Price <span class="required">*</span></label>
                        <div class="col-lg-2">
                          @if ($errors->has('price'))
                          <div class="alert alert-danger" role="alert">
                            {{ $errors->first() }}
                          </div>
                          @endif
                          <input class="form-control" id="price" name="price" type="text"
                            value="{{ old('price') ?? $article->price }}" required />
                        </div>
                      </div>
                      <div class="form-group ">
                        <label for="description" class="control-label col-lg-2">Description <span
                            class="required">*</span></label>
                        <div class="col-lg-10">
                          @if ($errors->has('description'))
                          <div class="alert alert-danger" role="alert">
                            {{ $errors->first() }}
                          </div>
                          @endif
                          <textarea class="form-control" id="description" name="description"
                            required>{{ old('descrition') ?? $article->description }}</textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                          <button class="btn btn-primary" type="submit">Save</button>
                          <a class="btn btn-danger" href="{{ route('articles.show', $article->id) }}">Cancel</a>
                        </div>
                      </div>
                      {{method_field('PUT')}}
                      {{ csrf_field() }}
                    </form>
                  </div>
                </section>
              </div>
              {{--edit-profile end--}}
              @else
              <div id="delivery-certificate" class="tab-pane active">
                <div class="profile-activity">
                  <div class="act-time">
                    @foreach ($article->fixed->delivery as $delivery)
                    <div class="activity-body act-in">
                      <div class="text">
                        {{-- <i class="activity-img icon_documents_alt fa-2x"></i> --}}
                        <p class="attribution">
                          <a href="{{ route('delivery-certificates.show',$delivery->id) }}">
                            <i class="activity-img icon_documents_alt fa-2x"></i>
                            {{ $delivery->code}}
                          </a>
                          {{ $delivery->created_at }}
                        </p>
                        <p><span>Delivered By:
                          </span>{{ $delivery->user->employee->first_name.' '.$delivery->user->employee->first_lastname }}
                        </p>
                      </div>
                    </div>
                    @endforeach
                  </div>
                </div>
              </div>
              {{-- delivery-certificate end --}}
              {{--edit-profile start--}}
              <div id="edit-profile" class="tab-pane">
                <section class="panel">
                  <div class="panel-body bio-graph-info">
                    <h1> Article Info</h1>
                    <form class="form-validate form-horizontal " id="register_form" method="post"
                      action="{{ route('articles.update',$article->id)}}" enctype="multipart/form-data">
                      <div class="form-group">
                        <label for="image" class="control-label col-lg-2">Image <span class="required">*</span></label>
                        <div class="col-lg-4">
                          @if ($errors->has('image'))
                          <div class="alert alert-danger" role="alert">
                            {{ $errors->first('image') }}
                          </div>
                          @endif
                          <input class="control-label" id="image" name="image" type="file"
                            value="{{ old('image') ?? $article->image}}" enctype="multipar/form-data" />
                        </div>
                        {{--  Esta clase col-lg-offset-2 de bootstrap es la que le esta dando el espaciado a la imagen  --}}
                        <div class="col-lg-3 col-lg-offset-2">
                          <img src='{{ asset("storage/$article->image")}}' alt='{{ "$article->name"}}'
                            class="img-rounded img-responsive">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="control-label col-lg-2">Article name <span
                            class="required">*</span></label>
                        <div class="col-lg-4">
                          @if ($errors->has('name'))
                          <div class="alert alert-danger" role="alert">
                            {{ $errors->first() }}
                          </div>
                          @endif
                          <input class="form-control" id="name" name="name" type="text"
                            value="{{ old('name') ?? $article->name }}" required />
                        </div>
                        <label for="model" class="control-label col-lg-2">Model <span class="required">*</span></label>
                        <div class="col-lg-4">
                          @if ($errors->has('model'))
                          <div class="alert alert-danger" role="alert">
                            {{ $errors->first() }}
                          </div>
                          @endif
                          <input class="form-control" id="model" name="model" type="text"
                            value="{{ old('model') ?? $article->model }}" required />
                        </div>

                      </div>
                      <div class="form-group ">
                        <label for="serial" class="control-label col-lg-2">Serial <span
                            class="required">*</span></label>
                        <div class="col-lg-4">
                          @if ($errors->has('serial'))
                          <div class="alert alert-danger" role="alert">
                            {{ $errors->first() }}
                          </div>
                          @endif
                          <input class="form-control" id="serial" name="serial" type="text"
                            value="{{ old('serial') ?? $article->serial }}" required />
                        </div>
                        <label for="brandA" class="control-label col-lg-2">Brand <span class="required">*</span></label>
                        <div class="col-lg-4">
                          @if ($errors->has('brand'))
                          <div class="alert alert-danger" role="alert">
                            {{ $errors->first() }}
                          </div>
                          @endif
                          <input class="form-control" type="text" name="brand" id="brandA"
                            value="{{old('brand') ?? $article->brand}}" readonly>
                        </div>
                      </div>
                      <div class="form-group ">
                        <label for="category_id" class="control-label col-lg-2">Category <span
                            class="required">*</span></label>
                        <div class="col-lg-2">
                          @if ($errors->has('category_id'))
                          <div class="alert alert-danger" role="alert">
                            {{ $errors->first() }}
                          </div>
                          @endif
                          <select class="form-control m-bot15" name="category_id">
                            <option value=""></option>
                            @foreach ($categories as $category)
                            @if ($category->id === $article->category->id)
                            <option selected value="{{ $category->id }}">{{ $category->name }}</option>
                            @continue
                            @endif
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                          </select>
                        </div>
                        <label for="quantity" class="control-label col-lg-2">Quantity <span
                            class="required">*</span></label>
                        <div class="col-lg-2">
                          @if ($errors->has('quantity'))
                          <div class="alert alert-danger" role="alert">
                            {{ $errors->first() }}
                          </div>
                          @endif
                          <input class="form-control" id="quantity" name="quantity" type="text"
                            value="{{ old('quantity') ?? $article->quantity }}" required />
                        </div>
                        <label for="price" class="control-label col-lg-2">Price <span class="required">*</span></label>
                        <div class="col-lg-2">
                          @if ($errors->has('price'))
                          <div class="alert alert-danger" role="alert">
                            {{ $errors->first() }}
                          </div>
                          @endif
                          <input class="form-control" id="price" name="price" type="text"
                            value="{{ old('price') ?? $article->price }}" required />
                        </div>
                      </div>
                      <div class="form-group ">
                        <label for="description" class="control-label col-lg-2">Description <span
                            class="required">*</span></label>
                        <div class="col-lg-10">
                          @if ($errors->has('description'))
                          <div class="alert alert-danger" role="alert">
                            {{ $errors->first() }}
                          </div>
                          @endif
                          <textarea class="form-control" id="description" name="description"
                            required>{{ old('descrition') ?? $article->description }}</textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                          <button class="btn btn-primary" type="submit">Save</button>
                          <a class="btn btn-danger" href="{{ route('articles.show', $article->id) }}">Cancel</a>
                        </div>
                      </div>
                      {{method_field('PUT')}}
                      {{ csrf_field() }}
                    </form>
                  </div>
                </section>
              </div>
              {{--edit-profile end--}}
              @endif
            </div>
          </div>
        </section>
      </div>
    </div>
    {{-- page end --}}
  </div>
</div>
{{--main content end--}}
@endsection