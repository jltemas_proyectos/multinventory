@extends('template')
@section('main-content')
{{--overview start--}}
<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header"><i class="fa fa-desktop" aria-hidden="true"></i>Articles Module</h3>
    <ol class="breadcrumb">
      <li><i class="fa fa-home" aria-hidden="true"></i><a href="{{ route('dashboard') }}">Dashboard</a></li>
      <li><i class="fa fa-laptop" aria-hidden="true"></i><a href="{{ route('articles.index') }}">Articles</a></li>
      <li><i class="fa fa-hdd-o" aria-hidden="true"></i>Update Article</li>
    </ol>
  </div>
</div>
{{--overview end--}}
{{--main content start--}}
<div class="row">
  <div class="col-lg-12">
    <section class="panel">
      <header class="panel-heading">
        <h3>Article Update Form</h3>
      </header>
      <div class="panel-body">
        <div class="form">
          <form class="form-validate form-horizontal " id="register_form" method="POST"
            action="{{ route('articles.update', $article->id)}}" enctype="multipart/form-data">
            @if (session('status'))
            <div class="alert alert-success col-lg-offset-2" role="alert">
              {{ session('status') }}
            </div>
            @endif
            <div class="form-group ">
              <label for="image" class="control-label col-lg-2" id="image">Image <span class="required">*</span></label>
              <div class="col-lg-4">
                @if ($errors->has('image'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first() }}
                </div>
                @endif
                <input class="control-label" id="image" name="image" type="file" value="{{$article->image}}" />
              </div>
              <div class="col-lg-4 col-lg-offset-2">
                <img src='{{ asset("$path/$article->image") }}' class="img-rounded img-responsive">
              </div>
            </div>
            <div class="form-group">
              <label for="category_id" class="control-label col-lg-2">Category <span class="required">*</span></label>
              <div class="col-lg-4">
                @if ($errors->has('category_id'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first() }}
                </div>
                @endif
                {{ $errors->first('category_id') }}
                <select class="form-control m-bot15" name="category_id" required>
                  <option value=""></option>
                  @foreach ($categories as $category)
                  @if ($category->id === $article->category->id)
                  <option value="{{ $category->id }}" selected>{{ $category->name }}</option>
                  @endif
                  <option value="{{ $category->id }}">{{ $category->name }}</option>
                  @endforeach
                </select>
              </div>
              <label for="name" class="control-label col-lg-2">Article <span class="required">*</span></label>
              <div class="col-lg-4">
                @if ($errors->has('name'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first() }}
                </div>
                @endif
                <input class="form-control" id="name" name="name" type="text" required
                  value="{{ old('name') ?? $article->name }}" />
              </div>
            </div>
            <div class="form-group ">
              <label for="brand" class="control-label col-lg-2">Brand<span class="required">*</span></label>
              <div class="col-lg-4">
                @if ($errors->has('brand'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first() }}
                </div>
                @endif
                <input type="text" name="brand" id="brand" class="form-control"
                  value="{{old('brand') ?? $article->brand}}" required>
              </div>
              <label for="model" class="control-label col-lg-2" id="category_id">Model <span
                  class="required">*</span></label>
              <div class="col-lg-4">
                @if ($errors->has('model'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first() }}
                </div>
                @endif
                <input type="text" name="model" id="model" class="form-control"
                  value="{{old('model') ?? $article->model}}" required>
              </div>
            </div>
            <div class="form-group">
              <label for="serial" class="control-label col-lg-2">Serial <span class="required">*</span></label>
              <div class="col-lg-2">
                @if ($errors->has('serial'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first() }}
                </div>
                @endif
                <input class="form-control" id="serial" name="serial" type="text" required
                  value="{{old('serial') ?? $article->serial }}" />
              </div>
              <label for="quantity" class="control-label col-lg-2">Quantity <span class="required">*</span></label>
              <div class="col-lg-2">
                @if ($errors->has('quantity'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first() }}
                </div>
                @endif
                <input class="form-control" id="quantity" name="quantity" type="number" required
                  value="{{ old('quantity') ?? $article->quantity }}" />
              </div>
              <label for="price" class="control-label col-lg-2">Price <span class="required">*</span></label>
              <div class="col-lg-2">
                @if ($errors->has('price'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first() }}
                </div>
                @endif
                <input class="form-control" id="price" name="price" type="number" required
                  value="{{ old('price') ?? $article->price }}" />
              </div>
            </div>
            <div class="form-group ">
              <label for="description" class="control-label col-lg-2">Description <span
                  class="required">*</span></label>
              <div class="col-lg-10">
                @if ($errors->has('description'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first() }}
                </div>
                @endif
                <textarea class="form-control" id="description" name="description" required
                  rows="4">{{ old('description') ?? $article->description }}</textarea>
              </div>
            </div>
            <div class="form-group">
              <div class="col-lg-offset-2 col-lg-10">
                <button class="btn btn-primary" type="submit">Save</button>
                <a class="btn btn-danger" href="{{ route('articles.index') }}">Cancel</a>
              </div>
            </div>
            {{ csrf_field() }}
            {{ method_field('PUT') }}
          </form>
        </div>
      </div>
    </section>
  </div>
</div>
{{--main content end--}}
@endsection