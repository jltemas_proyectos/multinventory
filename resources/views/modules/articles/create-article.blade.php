@extends('template')
@section('main-content')
{{--overview start--}}
<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header"><i class="fa fa-desktop" aria-hidden="true"></i>Articles Module</h3>
    <ol class="breadcrumb">
      <li><i class="fa fa-home" aria-hidden="true"></i><a href="{{ route('dashboard') }}">Dashboard</a></li>
      <li><i class="fa fa-laptop" aria-hidden="true"></i><a href="{{ route('articles.index') }}">Articles</a></li>
      <li><i class="fa fa-hdd-o" aria-hidden="true"></i>Create Article</li>
    </ol>
  </div>
</div>
{{--overview end--}}
{{--main content start--}}
<div class="row">
  <div class="col-lg-12">
    <section class="panel">
      <header class="panel-heading">
        <h3>Article Registration Form</h3>
      </header>
      <div class="panel-body">
        <div class="form">
          <form class="form-validate form-horizontal " id="register_form" method="POST"
            action="{{ route('articles.store')}}" enctype="multipart/form-data">
            @if (session('status'))
            <div class="alert alert-success col-lg-offset-2" role="alert">
              {{ session('status') }}
            </div>
            @endif
            <div class="form-group">
              <label for="category_id" class="control-label col-lg-2">Category <span class="required">*</span></label>
              <div class="col-lg-4">
                @if ($errors->has('category_id'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first() }}
                </div>
                @endif
                {{ $errors->first('category_id') }}
                <select class="form-control m-bot15" name="category_id" required>
                  <option value=""></option>
                  @foreach ($categories as $category)
                  <option value="{{ $category->id }}">{{ $category->name }}</option>
                  @endforeach
                </select>
              </div>
              <label for="image" class="control-label col-lg-2" id="image">Image <span class="required">*</span></label>
              <div class="col-lg-4">
                @if ($errors->has('image'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first() }}
                </div>
                @endif
                <input class="control-label" id="image" name="image" type="file" />
              </div>
            </div>
            <div class="form-group">
              <label for="name" class="control-label col-lg-2">Article <span class="required">*</span></label>
              <div class="col-lg-4">
                @if ($errors->has('name'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first() }}
                </div>
                @endif
                <input class="form-control" id="name" name="name" type="text" value="{{ old('name') }}" required />
              </div>
              <label for="model" class="control-label col-lg-2" id="category_id">Model <span
                  class="required">*</span></label>
              <div class="col-lg-4">
                @if ($errors->has('model'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first() }}
                </div>
                @endif
                <input type="text" name="model" id="model" class="form-control" value="{{old('model')}}" required>
              </div>
            </div>
            <div class="form-group">
              <label for="brand" class="control-label col-lg-2">Brand<span class="required">*</span></label>
              <div class="col-lg-4">
                @if ($errors->has('brand'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first() }}
                </div>
                @endif
                <select class="form-control m-bot15" name="brand" required>
                  <option value=""></option>
                  @foreach ($brands as $brand)
                  <option value="{{ $brand->name }}">{{ $brand->name }}</option>
                  @endforeach
                </select>
              </div>
              <label for="serial" class="control-label col-lg-2">Serial<span class="required">*</span></label>
              <div class="col-lg-4">
                @if ($errors->has('serial'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first() }}
                </div>
                @endif
                <input class="form-control" id="serial" name="serial" type="text" required value="{{old('serial') }}"
                  required />
              </div>
            </div>
            <div class="form-group ">
              <label for="quantity" class="control-label col-lg-2">Quantity<span class="required">*</span></label>
              <div class="col-lg-4">
                @if ($errors->has('quantity'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first() }}
                </div>
                @endif
                <input class="form-control" id="quantity" name="quantity" type="number" value="{{ old('quantity') }}"
                  required />
              </div>
              <label for="price" class="control-label col-lg-2">Price<span class="required">*</span></label>
              <div class="col-lg-4">
                @if ($errors->has('price'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first() }}
                </div>
                @endif
                <div class="input-group input-group-md">
                  <span class="input-group-addon">$</span>
                  <input class="form-control" id="price" name="price" type="number" value="{{ old('price') }}"
                    required />
                </div>
              </div>
            </div>
            <div class="form-group ">
              <label for="description" class="control-label col-lg-2">Description <span
                  class="required">*</span></label>
              <div class="col-lg-10">
                @if ($errors->has('description'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first() }}
                </div>
                @endif
                <textarea class="form-control" id="description" name="description" rows="4"
                  required>{{ old('description') }}</textarea>
              </div>
            </div>
            <div class="form-group">
              <div class="col-lg-offset-2 col-lg-10">
                <button class="btn btn-primary" type="submit">Save</button>
                <a class="btn btn-danger" href="{{ route('articles.index') }}">Cancel</a>
              </div>
            </div>
            {{ csrf_field() }}
          </form>
        </div>
      </div>
    </section>
  </div>
</div>
{{--main content end--}}
@endsection