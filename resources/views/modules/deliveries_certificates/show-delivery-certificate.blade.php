@extends('template')
@section('main-content')
{{--overview start--}}
<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header"><i class="icon_documents_alt"></i>Delivery Certificate N° {{ $code }}</h3>
    <ol class="breadcrumb">
      <li><i class="fa fa-home"></i><a href="{{ route('dashboard') }}">Dashboard</a></li>
      <li><i class="icon_documents_alt"></i><a href="{{ route('delivery-certificates.index') }}">Delivery
          Certificate</a></li>
      <li><i class="fa fa-file-text-o"></i>Delivery Certificate N° {{ $code }}</li>
    </ol>
  </div>
</div>
{{--overview end--}}
{{--main content start--}}
<div class="row">
  <div class="col-lg-12">
    <div class="panel">
      <div class="panel-body">
        <header class="row margin-bottom bottom-line">
          <div class="col-lg-2">
            <img class="img-responsive" src="{{asset('img/multi.png')}}" alt="">
          </div>
          <div class="col-lg-10">
            <div class="row">
              <div class="col-lg-7 col-lg-push-1">
                <h3 class="page-header title center">Multipartes Sas</h3>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-7 col-lg-push-1">
                <div class="form-horizontal">
                  <div class="">
                    <p class="page-header sub-title center">Delivery Certificate N° <span>{{ $code }}</span></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </header>
        <div class="row">
          <div class="col-lg-5 page-header">
            <div class="form-horizontal">
              <div class="form-group">
                <span class="col-lg-4">Date</span>
                <span class="col-lg-1">:</span>
                <p class="col-lg-5 ">{{ $delivery->created_at }}</p>
                <span class="col-lg-5 ">{{ $delivery->created_at }}</span>
              </div>
            </div>
          </div>
          <div class="col-lg-6 page-header">
            <div class="form-horizontal">
              <div class="form-group">
                <span class="col-lg-3">Department</span>
                <span class="col-lg-1">:</span>
                <span class="col-lg-2">{{ $delivery->employee->department->name }}</span>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-5 page-header">
            <div class="form-horizontal">
              <div class="form-group">
                <span class="col-lg-4">Assigned To</span>
                <span class="col-lg-1">:</span>
                <span
                  class="col-lg-4">{{ $delivery->employee->first_name.' '.$delivery->employee->first_lastname }}</span>
              </div>
            </div>
          </div>
          <div class="col-lg-6 page-header">
            <div class="form-horizontal">
              <div class="form-group">
                <span class="col-lg-3">Cedula</span>
                <span class="col-lg-1">:</span>
                <span class="col-lg-4">{{ $delivery->employee->cedula }}</span>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <div class="table-responsive top-line bottom-line">
              <table class="table table-bordered ">
                <caption class="page-header-table">Description</caption>
                <thead>
                  <th>Fixed Asset</th>
                  <th>Article</th>
                  <th>Brand</th>
                  <th>Model</th>
                  <th>Serial</th>
                  <th>Quantity</th>
                  <th>Cost</th>
                </thead>
                <tbody>
                  @foreach ($delivery->fixedassets as $fixedasset)
                  @foreach ($fixedasset->articles as $article)
                  <tr>
                    <td>{{ $fixedasset->code }}</td>
                    <td>{{ $article->name }}</td>
                    <td>{{ $article->brand }}</td>
                    <td>{{ $article->model }}</td>
                    <td>{{ $article->serial }}</td>
                    <td>{{ $article->quantity }}</td>
                    <td>{{ $article->price }}</td>
                    @php
                    $total += $article->price
                    @endphp
                  </tr>
                  @endforeach
                  @endforeach
                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="6" class="center">Total</td>
                    <td>{{ $total }}</td>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12 page-header">
            <div class="form-horizontal">
              <div class="form-group">
                <span class="col-lg-2">Delivered By</span>
                <span class="col-lg-1">:</span>
                <span
                  class="col-lg-4">{{ $delivery->user->employee->first_name.' '.$delivery->user->employee->first_lastname }}</span>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12 page-header">
            <div class="form-horizontal">
              <div class="form-group">
                <div>
                  <span class="col-lg-2">Observation</span>
                  <span class="col-lg-1">:</span>
                  <span class="col-lg-9 page-header-obs">{{ $delivery->observation }}</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  {{--main content end--}}
  @endsection