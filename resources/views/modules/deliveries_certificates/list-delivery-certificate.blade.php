@extends('template')
@section('main-content')
{{--overview start--}}
<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header"><i class="icon_documents_alt"></i>Delivery Certificate Module</h3>
    <ol class="breadcrumb">
      <li><i class="fa fa-home"></i><a href="{{ route('dashboard') }}">Dashboard</a></li>
      <li><i class="icon_documents_alt"></i>Articles</li>
    </ol>
  </div>
</div>
{{--overview end--}}
{{--main content start--}}
<div class="row">
  <div class="col-lg-12">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <div class="table-responsive">
            <table class="table table-striped table-advance table-hover">
              <caption class="page-header-table">Delivery Certificates</caption>
              <tbody class="form-horizontal">
                <tr class="form-group">
                  <th class="col-lg-1 col-sm-1 center"> N°</th>
                  <th class="col-lg-1 col-sm-1 center"> <i class="icon_documents_alt"></i> Code</th>
                  <th class="col-lg-2 col-sm-2 center"><i class="fa fa-calendar" aria-hidden="true"></i> Date</th>
                  <th class="col-lg-3 col-sm-2"><i class="fa fa-user" aria-hidden="trues"></i> Employee</th>
                  <th class="col-lg-2 col-sm-2"><i class="fa fa-sitemap" aria-hidden="true"></i> Department</th>
                  <th class="col-lg-3 col-sm-3"><i class="icon_cogs"></i> Action</th>
                </tr>
                @foreach ($deliveriesC as $key => $deliveryC)
                @if (!$deliveryC->deleted_at)
                {{-- {{$key}} --}}
                <tr class="form-group">
                  <td class="col-lg-1 col-sm-1 center">{{ $count}}</td>
                  <td class="col-lg-1 col-sm-1 center">{{ $codes[$key] }}</td>
                  <td class="col-lg-2 col-sm-2 center">{{ $deliveryC->date }}</td>
                  <td class="col-lg-3 col-sm-2"><a
                      href="{{route('employees.show',$deliveryC->employee->id)}}">{{ $deliveryC->employee->first_name . ' '. $deliveryC->employee->first_lastname }}</a>
                  </td>
                  <td class="col-lg-2 col-sm-3 col-sm-3">{{ $deliveryC->employee->department->name }}</td>
                  <td class="col-sm-3">
                    <div class="btn-group btn-group-sm">
                      <a class="btn btn-info" href="{{ route('delivery-certificates.edit', $deliveryC->id) }}"><i
                          class="icon_plus_alt2"></i></a>
                      <a class="btn btn-success " href="{{ route('delivery-certificates.show', $deliveryC->id) }}"><i
                          class="icon_check_alt2"></i></a>
                      <form class="form-delete" method="POST"
                        action="{{ route('delivery-certificates.destroy', $deliveryC->id) }}">
                        <button class="btn btn-sm btn-danger form-delete-button" type="submit"><i
                            class="icon_close_alt2"></i></button>
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                      </form>
                    </div>
                  </td>
                </tr>
                @php $count++ @endphp
                @endif
                @endforeach
              </tbody>
            </table>
            <div class="row">
              <div class="col-lg-offset-5">
                {{$deliveriesC->links() }}
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
</div>
{{--main content end--}}
@endsection