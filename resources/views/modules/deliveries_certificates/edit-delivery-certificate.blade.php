@extends('template.layout')
@section('main-content')
  {{--overview start--}}
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
        <h3 class="page-header"><i class="icon_documents_alt"></i>Delivery Certificate Module</h3>
        <ol class="breadcrumb">
          <li><i class="fa fa-home"></i><a href="{{ route('home') }}">Home</a></li>
          <li><i class="icon_documents_alt"></i><a href="{{ route('delivery-certificates.index') }}">Delivery Certificate</a></li>
          <li><i class="fa fa-file-text-o"></i>Update Delivery Certificate</li>
        </ol>
      </div>
    </div>
  </section>
  {{--overview end--}}
  {{--main content start--}}
  <div class="row">
    <div class="col-lg-12">
      <div class="panel-body panel-body-j">
        <section class="panel">
          <header class="panel-heading">
            <h3>Delivery Certificate Update Form</h3>
          </header>
          <div class="panel-body">
            <div class="form">
                <div>
                  <select class="form-control col-lg-3 articles" id="selectFixedasset" style="visibility:hidden">
                    <option></option>
                    @foreach ($fixedAssets as $fixedasset)
                      <option value="{{ $fixedasset->code }}">{{ $fixedasset->code }}</option>
                    @endforeach
                  </select>
                </div>
              <form class="form-validate form-horizontal " id="register_form" method="POST" action="{{ route('delivery-certificates.update', $deliveryC->id) }}">
                @if (session()->has('status'))
                  <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                  </div>
                @endif
                <div class="form-group ">
                  <label for="code" class="control-label col-lg-2">Code</label>
                  <div class="col-lg-3">
                    @if ($errors->has('code'))
                      <div class="alert alert-danger" role="alert">
                        {{ $errors->first() }}  
                      </div>
                    @endif
                  <input class=" form-control" id="code" name="code" type="text" value="{{ $code }}" readonly/>
                  </div>
                  <label for="date" class="control-label col-lg-2">Date</label>
                  <div class="col-lg-3  ">
                    @if ($errors->has('date'))
                      <div class="alert alert-danger" role="alert">
                        {{ $errors->first() }}  
                      </div>
                    @endif
                    <input class=" form-control" id="date" name="date" type="text" value="{{ $deliveryC->date}}" readonly/>
                  </div>
                </div>
                <div class="form-group">
                  <label for="department_id" class="control-label col-lg-2">Deparment</label>
                  <div class="col-lg-3">
                    @if ($errors->has('department_id'))
                      <div class="alert alert-danger" role="alert">
                        {{ $errors->first() }}  
                      </div>
                    @endif
                    <input class=" form-control" id="department_id" name="department_id" type="text" value="{{ $deliveryC->department}}" readonly/>
                  </div>
                  <label for="cedula" class="control-label col-lg-2">Cedula </label>
                  <div class="col-lg-3">
                    @if ($errors->has('cedula'))
                      <div class="alert alert-danger" role="alert">
                        {{ $errors->first() }}  
                      </div>
                    @endif
                    <input class=" form-control" id="cedula" name="cedula" type="text" value="{{$deliveryC->employee->cedula }}" readonly/>
                  </div>
                </div>
                <div class="form-group ">
                  <label for="employee_id" class="control-label col-lg-2">Received By</label>
                  <div class="col-lg-8">
                    @if ($errors->has('employee_id'))
                      <div class="alert alert-danger" role="alert">
                        {{ $errors->first() }}  
                      </div>
                    @endif
                    <input class=" form-control" id="employee_id" name="employee_id" type="text" value="{{ $deliveryC->employee->first_name .'  '.  $deliveryC->employee->second_name .'  '. $deliveryC->employee->first_lastname .'  '.  $deliveryC->employee->second_lastname}}" readonly/>
                  </div>
                </div>
                <section class="panel content-table">
                  <header class="panel-heading center"><h3>Articles Descrition</h3></header>
                  <div class="table-responsive nicescroll-rails" id="ascrail2000">
                    <table class="table table-bordered table-advance table-hover">
                      <thead class="form-horizontal">
                        <tr>
                          <th class="col-lg-1"> Fixes Asset</th>
                          <th class="col-lg-1"> Articles</th>
                          <th class="col-lg-1"> Brand</th>
                          <th class="col-lg-1"> Model</th>
                          <th class="col-lg-2"> Serial</th>
                          <th class="col-lg-1"> Quantity</th>
                          <th class="col-lg-2"> Price</th>
                          <th class="col-lg-2"> Action</th>
                        </tr>
                      </thead>
                      <tbody class="form-horizontal" id="tbody">
                        @foreach ($deliveryC->fixedassets as $key => $fixedasset)
                          @foreach ($fixedasset->articles as $key2 => $article)
                            @if ($article->status === 1)
                              <tr>
                                <td>
                                  @if ($errors->has('fixedassets[]'))
                                    <div class="alert alert-danger" role="alert">
                                      {{ $errors->first() }}  
                                    </div>
                                  @endif
                                  <input class="form-control" type="text" name="fixedassets[]"  id="fixedasset{{$key2}}" value="{{ $fixedasset->code}}" data-fixed="{{$fixedasset->id}}" readonly>
                                </td>
                                <td>
                                  @if ($errors->has('articles[]'))
                                    <div class="alert alert-danger" role="alert">
                                      {{ $errors->first() }}  
                                    </div>
                                  @endif
                                  <input readonly data-article="{{$article->id}}" type="text" name="articles[]" id="article{{$key2}}" value="{{ $article->name }}" class="form-control"
                                    required>
                                </td>
                                <td>
                                  @if ($errors->has('brands[]'))
                                    <div class="alert alert-danger" role="alert">
                                      {{ $errors->first() }}  
                                    </div>
                                  @endif
                                  <input readonly type="text" name="brands[]" id="brands{{$key2}}" value="{{ $article->brand}}" class="form-control" required>
                                </td>
                                <td>
                                  @if ($errors->has('models[]'))
                                    <div class="alert alert-danger" role="alert">
                                      {{ $errors->first() }}  
                                    </div>
                                  @endif
                                  <input readonly type="text" name="models[]" id="model{{$key2}}" value="{{ $article->model}}" class="form-control" required>
                                </td>
                                <td>
                                  @if ($errors->has('serials[]'))
                                    <div class="alert alert-danger" role="alert">
                                      {{ $errors->first() }}  
                                    </div>
                                  @endif
                                  <input readonly type="text" name="serials[]" id="serial{{$key2}}" value="{{ $article->serial}}" class="form-control" required>
                                </td>
                                <td>
                                  @if ($errors->has('quantitys[]'))
                                    <div class="alert alert-danger" role="alert">
                                      {{ $errors->first() }}  
                                    </div>
                                  @endif
                                  <input readonly type="text" name="quantity[]" id="quantity{{$key2}}" value="{{$article->quantity}}" class="form-control" required>
                                </td>
                                <td>
                                  @if ($errors->has('prices[]'))
                                    <div class="alert alert-danger" role="alert">
                                      {{ $errors->first() }}  
                                    </div>
                                  @endif
                                  <input readonly type="text" name="prices[]" id="price{{$key2}}" value="{{$article->price}}" class="form-control" required>
                                </td>
                                <td>
                                  <div class="btn-group">
                                    <button data-btn="btn" class="btn btn-danger" type="button"><i data-icon="icon" class="icon_close_alt2 damagedAjax"></i></button>
                                    <button data-btn="btn" class="btn btn-warning" type="button"><i data-icon="icon" class="icon_close_alt2 failureAjax"></i></button>
                                    <button data-btn="btn" class="btn btn-primary" type="button"><i data-icon="icon" class="icon_close_alt2 disconfortAjax"></i></button>
                                  </div>
                                </td>
                              </tr>
                              <input type="hidden" value="{{$total += $article->price}}"> 
                            @endif
                          @endforeach
                        @endforeach
                      </tbody>
                      <tfoot>
                        <tr>
                          <td colspan="6" class="center">Total</td>
                          <td class="price"> <input class="form-control" type="text" name="total_edit_delivery" id="total_edit_delivery"  value="{{$total}}" readonly></td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                </section>
                <div class="form-group">
                  <div class="col-lg-offset-10 col-lg-2">
                    <button class="btn btn-success" type="button" id="btnAddFA">Add Fixed Asset</button>
                  </div>
                </div>
                <div class="form-group ">
                  <label for="deliveredBy" class="control-label col-lg-2">Delivered By <span class="required">*</span></label>
                  <div class="col-lg-8">
                    <input class=" form-control" id="deliveredBy" value="{{$deliveryC->user->employee->first_name.'  '.$deliveryC->user->employee->second_name.'  '.$deliveryC->user->employee->first_lastname.'  '.$deliveryC->user->employee->second_lastname}}" name="deliveredBy" type="text" />
                  </div>
                </div>
                <div class="form-group ">
                  <label for="observation" class="control-label col-lg-2">Observation </label>
                  <div class="col-lg-10">
                  <textarea class="form-control " id="observation" name="observation" required>{{$deliveryC->observation}}</textarea> 
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                    <button class="btn btn-primary" type="submit">Save</button>
                    <button class="btn btn-default" type="button">Cancel</button>
                  </div>
                </div>
                {{ csrf_field() }}
                {{ method_field('PUT') }}
              </form>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
  {{--main content end--}}
  @section('js')
  <script src="{{asset('js/myscript4.js')}}"></script>
  @endsection
@endsection