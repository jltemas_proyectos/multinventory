@extends('template')
@section('main-content') {{--overview start--}}
<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header"><i class="icon_documents_alt"></i>Delivery Certificate Module</h3>
    <ol class="breadcrumb">
      <li><i class="fa fa-home"></i><a href="{{ route('dashboard') }}">Dashboard</a></li>
      <li><i class="icon_documents_alt"></i><a href="{{ route('delivery-certificates.index') }}">Delivery
          Certificate</a></li>
      <li><i class="fa fa-file-text-o"></i>Create Delivery Certificate</li>
    </ol>
  </div>
</div>
{{--overview end--}} {{--main content start--}}
<div class="row">
  <div class="col-lg-12">
    <section class="panel" id="conten-form">
      <header class="panel-heading">
        <h3>Delivery Certificate Registration Form</h3>
      </header>
      <div class="panel-body">
        <div class="form">
          <form class="form-validate form-horizontal " id="register_form" method="POST"
            action="{{ route('delivery-certificates.store') }}">
            @if (session()->has('status'))
            <div class="alert alert-success center" role="alert">
              {{ session('status') }}
            </div>
            @endif
            <div class="form-group ">
              <label for="code" class="control-label col-lg-2">Code <span class="required">*</span></label>
              <div class="col-lg-3">
                @if ($errors->has('code'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first() }}
                </div>
                @endif
                <input class=" form-control" id="code" name="code" type="text" value="{{ old('code') ?? ($code) }}"
                  readonly required />
              </div>
              <label for="date" class="control-label col-lg-2">Date <span class="required">*</span></label>
              <div class="col-lg-3">
                @if ($errors->has('date'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first() }}
                </div>
                @endif
                <input class=" form-control" id="date" name="date" type="text" value="{{ date('Y-m-d') }}" readonly
                  required />
              </div>
            </div>
            <div class="form-group">
              <label for="department_id" class="control-label col-lg-2">Deparment <span
                  class="required">*</span></label>
              <div class="col-lg-3">
                @if ($errors->has('department_id'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first() }}
                </div>
                @endif
                <select class="form-control" name="department_id" id="department_id" required>
                  <option></option>
                  @foreach ($departments as $department)
                  <option value="{{ $department->id }}">{{ $department->name }}</option>
                  @endforeach
                </select>
              </div>
              <label for="cedula" class="control-label col-lg-2">Cedula <span class="required">*</span></label>
              <div class="col-lg-3">
                @if ($errors->has('cedula'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first() }}
                </div>
                @endif
                <input class=" form-control" id="cedula" name="cedula" type="text" value="{{old('cedula')}}" required />
              </div>
            </div>
            <div class="form-group">
              <label for="employee_id" class="control-label col-lg-2">Received By <span
                  class="required">*</span></label>
              <div class="col-lg-8">
                @if ($errors->has('employee_id'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first() }}
                </div>
                @endif
                <select class="form-control" name="employee_id" id="employee_id" required>
                  <option value=""></option>
                </select>
              </div>
            </div>
            <div class="table-responsive nicescroll-rails top-line" id="ascrail2000">
              <table class="table table-bordered table-advance table-hover">
                <caption class="page-header-table">Articles Descrition</caption>
                <thead class="form-horizontal">
                  <tr>
                    <th class="col-lg-2"><i class="icon_cogs"></i> Fixes Asset</th>
                    <th class="col-lg-2"> Articles</th>
                    <th class="col-lg-1"> Brand</th>
                    <th class="col-lg-1"> Model</th>
                    <th class="col-lg-2">Serial</th>
                    <th class="col-lg-1"> Quantity</th>
                    <th class="col-lg-2"> Price</th>
                    <th class="col-lg-1"> Action</th>
                  </tr>
                </thead>
                <tbody class="form-horizontal" id="tbody">
                  <tr>
                    <td>
                      @if ($errors->has('fixedassets[]'))
                      <div class="alert alert-danger" role="alert">
                        {{ $errors->first() }}
                      </div>
                      @endif
                      <select name="fixedassets[]" id="fixedasset" class="form-control fixedasset" required>
                        <option></option>
                        @foreach ($codefixedassets as $key=>$fixedasset)
                        @switch(strlen($fixedasset->code))
                        @case(1)
                        <option value="{{$fixedasset->id}}">{{"000$fixedasset->code"}}</option>
                        @break
                        @case(2)
                        <option value="{{$fixedasset->id}}">{{"00$fixedasset->code"}}</option>
                        @break
                        @case(3)
                        <option value="{{$fixedasset->id}}">{{"0$fixedasset->code"}}</option>
                        @break
                        @case(4)
                        <option value="{{$fixedasset->id}}">{{$fixedasset->code}}</option>
                        @break
                        @endswitch
                        @endforeach
                      </select>
                    </td>
                    <td>
                      @if ($errors->has('articles[]'))
                      <div class="alert alert-danger" role="alert">
                        {{ $errors->first() }}
                      </div>
                      @endif
                      <input readonly type="text" name="articles[]" id="article" value="" class="form-control" required>
                    </td>
                    <td>
                      @if ($errors->has('brands[]'))
                      <div class="alert alert-danger" role="alert">
                        {{ $errors->first() }}
                      </div>
                      @endif
                      <input readonly type="text" name="brands[]" id="brands" value="" class="form-control" required>
                    </td>
                    <td>
                      @if ($errors->has('models[]'))
                      <div class="alert alert-danger" role="alert">
                        {{ $errors->first() }}
                      </div>
                      @endif
                      <input readonly type="text" name="models[]" id="model" value="" class="form-control" required>
                    </td>
                    <td>
                      @if ($errors->has('serials[]'))
                      <div class="alert alert-danger" role="alert">
                        {{ $errors->first() }}
                      </div>
                      @endif
                      <input readonly type="text" name="serials[]" id="serial" class="form-control" required>
                    </td>
                    <td>
                      @if ($errors->has('quantitys[]'))
                      <div class="alert alert-danger" role="alert">
                        {{ $errors->first() }}
                      </div>
                      @endif
                      <input readonly type="text" name="quantitys[]" id="quantity" class="form-control" required>
                    </td>
                    <td>
                      @if ($errors->has('prices[]'))
                      <div class="alert alert-danger" role="alert">
                        {{ $errors->first() }}
                      </div>
                      @endif
                      <input readonly type="text" name="prices[]" id="price" class="form-control" required>
                    </td>
                    <td>
                      <button class="btn btn-danger" type="button" id="btn-detele"><i
                          class="icon_close_alt2"></i></button>
                    </td>
                  </tr>
                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="6" class="center">Total</td>
                    <td id="priceTotal" class="price">0.00</td>
                  </tr>
                </tfoot>
              </table>
            </div>

            <div class="form-group">
              <div class="col-lg-offset-10 col-lg-2">
                <button class="btn btn-success" type="button" id="addArticle">Add Fixed Asset</button>
              </div>
            </div>
            <div class="form-group ">
              <label for="deliveredBy" class="control-label col-lg-2">Delivered By <span
                  class="required">*</span></label>
              <div class="col-lg-8">
                <span class="form-control">{{Auth::user()->employee->first_name}}
                  {{Auth::user()->employee->first_lastname}}</span>
                @if ($errors->has('deliveredBy[]'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first() }}
                </div>
                @endif
                <input class="form-control" id="deliveredBy" name="deliveredBy" type="hidden"
                  value="{{Auth::user()->id}}" required />
              </div>
            </div>
            <div class="form-group ">
              <label for="observation" class="control-label col-lg-2">Observation </label>
              <div class="col-lg-10">
                @if ($errors->has('observation'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first() }}
                </div>
                @endif
                <textarea class="form-control " id="observation" name="observation"></textarea>
              </div>
            </div>
            <div class="form-group">
              <div class="col-lg-offset-2 col-lg-10">
                <button class="btn btn-primary" type="submit">Save</button>
                <button class="btn btn-danger" type="button">Cancel</button>
              </div>
            </div>
            {{ csrf_field() }}
          </form>
        </div>
      </div>
    </section>
  </div>
</div>
{{--main content end--}}
@endsection
@section('js')
<script src="{{asset('js/myscript2.js')}}"></script>
@endsection