@extends('template')
@section('main-content')
{{--overview start--}}
<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header"><i class="icon_tools"></i>Maintenances Module</h3>
    <ol class="breadcrumb">
      <li><i class="fa fa-home"></i><a href="{{ route('dashboard') }}">Dashboard</a></li>
      <li><i class="fa fa-cogs"></i>Maintenances</li>
    </ol>
  </div>
</div>
{{--overview end--}}
{{--main content start--}}
<div class="row">
  <div class="col-lg-12">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <div class="table-responsive">
            <table class="table table-striped table-advance table-hover">
              <caption class="page-header-table">Maintenances</caption>
              <tbody class="form-horizontal">
                <tr class="form-group">
                  <th class="col-lg-1 col-sm-1 center"> N°</th>
                  <th class="col-lg-2 col-sm-1 center"> <i class="fa fa-cogs"></i> Code</th>
                  <th class="col-lg-3 col-sm-2 center"><i class="fa fa-calendar" aria-hidden="true"></i> Date</th>
                  <th class="col-lg-3 col-sm-3 "><i class="fa fa-user-o" aria-hidden="trues"></i> Responsable</th>
                  <th class="col-lg-3 col-sm-3"><i class="icon_cogs"></i> Action</th>
                </tr>
                @foreach ($maintenances as $maintenance)
                <tr class="form-group">
                  <td class="col-lg-1 col-sm-1 center">{{ $maintenance->id }}</td>
                  <td class="col-lg-1 col-sm-1 center">{{ $maintenance->code }}</td>
                  <td class="col-lg-2 col-sm-2 center">{{ $maintenance->date }}</td>
                  <td class="col-lg-3  col-sm-3 col-sm-3">{{ $maintenance->observations }}</td>
                  <td class="col-sm-3">
                    <div class="btn-group">
                      <a class="btn btn-primary" href="{{ route('maintenances.edit', $maintenance->id) }}"><i
                          class="icon_plus_alt2"></i></a>
                      <a class="btn btn-success" href="{{ route('maintenances.show', $maintenance->id) }}"><i
                          class="icon_check_alt2"></i></a>
                      <form class="form-delete" method="POST"
                        action="{{ route('maintenances.destroy', $maintenance->id) }}">
                        <button class="btn btn-danger form-delete-button" type="submit"><i
                            class="icon_close_alt2"></i></button>
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                      </form>
                    </div>
                  </td>
                </tr>
                @endforeach
                {{--  <tr class="form-group">
                      <td class="col-lg-1 col-sm-1 center">1</td>
                      <td class="col-lg-1 col-sm-1 center">1144859213</td>
                      <td class="col-lg-2 col-sm-2 center">27-09-2018</td>
                      <td class="col-lg-3  col-sm-3 col-sm-3">Maria de los angeles Cordoba Diaz</td>
                      <td class="col-sm-3">
                        <div class="btn-group">
                            <a class="btn btn-primary" href=""><i class="icon_plus_alt2"></i></a>
                            <a class="btn btn-success" href="#"><i class="icon_check_alt2"></i></a>
                            <a class="btn btn-danger" href="#"><i class="icon_close_alt2"></i></a>
                        </div>
                      </td>
                    </tr>
                    <tr class="form-group">
                      <td class="col-lg-1 col-sm-1 center">1</td>
                      <td class="col-lg-1 col-sm-1 center">1144859213</td>
                      <td class="col-lg-2 col-sm-2 center">27-09-2018</td>
                      <td class="col-lg-3  col-sm-3 col-sm-3">Maria de los angeles Cordoba Diaz</td>
                      <td class="col-sm-3">
                        <div class="btn-group">
                            <a class="btn btn-primary" href=""><i class="icon_plus_alt2"></i></a>
                            <a class="btn btn-success" href="#"><i class="icon_check_alt2"></i></a>
                            <a class="btn btn-danger" href="#"><i class="icon_close_alt2"></i></a>
                        </div>
                      </td>
                    </tr>  --}}
              </tbody>
            </table>
          </div>
        </section>
      </div>
    </div>
  </div>
</div>
{{--main content end--}}
@endsection