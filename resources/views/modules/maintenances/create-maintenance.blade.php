@extends('template')
@section('main-content')
{{--overview start--}}
<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header"><i class="icon_tools"></i>Maintenances Module</h3>
    <ol class="breadcrumb">
      <li><i class="fa fa-home"></i><a href="{{ route('dashboard') }}">Dashboard</a></li>
      <li><i class="fa fa-cogs"></i><a href="{{ route('maintenances.index')}}">Maintenances</a></li>
      <li><i class="fa fa-cog"></i>Create Maintenance</li>
    </ol>
  </div>
</div>
{{--overview end--}}
{{--main content start--}}
<div class="row">
  <div class="col-lg-12">
    <section class="panel">
      <header class="panel-heading">
        <h3>Maintenance Registration Form</h3>
      </header>
      <div class="panel-body">
        <div class="form">
          <form class="form-validate form-horizontal " id="register_form" method="get" action="">
            <div class="form-group ">
              <label for="fullname" class="control-label col-lg-2">Order</label>
              <div class="col-lg-4">
                <input class=" form-control" id="fullname" name="fullname" type="text" />
              </div>
              <label for="fullname" class="control-label col-lg-2">Date <span class="required">*</span></label>
              <div class="col-lg-4">
                <input class=" form-control" id="fullname" name="fullname" type="date" />
              </div>
            </div>
            <section class="panel content-table">
              <header class="panel-heading center">
                <h3>Articles Descrition</h3>
              </header>
              <div class="table-responsive nicescroll-rails" id="ascrail2000">
                <table class="table table-striped table-advance table-hover">
                  <tbody class="form-horizontal">
                    <tr class="form-group">
                      <th class="col-lg-2"><i class="icon_genius"></i> Fixid Asset</th>
                      <th class="col-lg-2"><i class="fa fa-desktop" aria-hidden="true"></i> Article</th>
                      <th class="col-lg-2"><i class="icon_mail_alt"></i> Maintenance Type</th>
                      <th class="col-lg-5"><i class="fa fa-cogs" aria-hidden="true"></i> Description</th>
                      <th class="col-lg-1"><i class="icon_cogs"></i> Action</th>
                    </tr>
                    <tr class="form-group">
                      <td>
                        <select name="" id="" class="form-control">
                          <option value="">451</option>
                          <option value="">452</option>
                        </select>
                      </td>
                      <td>
                        <select name="" id="" class="form-control">
                          <option value="">Mouse</option>
                          <option value="">Monitor</option>
                        </select>
                      </td>
                      <td>
                        <select name="" id="" class="form-control">
                          <option value="">Preventive</option>
                          <option value="">Corrective</option>
                        </select>
                      </td>
                      <td>
                        <textarea name="" id="" class="form-control"></textarea>
                      </td>
                      <td>
                        <div class="btn-group">
                          <a class="btn btn-danger" href="#"><i class="icon_close_alt2"></i></a>
                        </div>
                      </td>
                    </tr>
                    <tr class="form-group">
                      <td>
                        <select name="" id="" class="form-control">
                          <option value="">451</option>
                          <option value="">452</option>
                        </select>
                      </td>
                      <td>
                        <select name="" id="" class="form-control">
                          <option value="">Mouse</option>
                          <option value="">Monitor</option>
                        </select>
                      </td>
                      <td>
                        <select name="" id="" class="form-control">
                          <option value="">Preventive</option>
                          <option value="">Corrective</option>
                        </select>
                      </td>
                      <td>
                        <textarea name="" id="" class="form-control"></textarea>
                      </td>
                      <td>
                        <div class="btn-group">
                          <a class="btn btn-danger" href="#"><i class="icon_close_alt2"></i></a>
                        </div>
                      </td>
                    </tr>
                    <tr class="form-group">
                      <td>
                        <select name="" id="" class="form-control">
                          <option value="">451</option>
                          <option value="">452</option>
                        </select>
                      </td>
                      <td>
                        <select name="" id="" class="form-control">
                          <option value="">Mouse</option>
                          <option value="">Monitor</option>
                        </select>
                      </td>
                      <td>
                        <select name="" id="" class="form-control">
                          <option value="">Preventive</option>
                          <option value="">Corrective</option>
                        </select>
                      </td>
                      <td>
                        <textarea name="" id="" class="form-control"></textarea>
                      </td>
                      <td>
                        <div class="btn-group">
                          <a class="btn btn-danger" href="#"><i class="icon_close_alt2"></i></a>
                        </div>
                      </td>
                    </tr>
                    <tr class="form-group">
                      <td>
                        <select name="" id="" class="form-control">
                          <option value="">451</option>
                          <option value="">452</option>
                        </select>
                      </td>
                      <td>
                        <select name="" id="" class="form-control">
                          <option value="">Mouse</option>
                          <option value="">Monitor</option>
                        </select>
                      </td>
                      <td>
                        <select name="" id="" class="form-control">
                          <option value="">Preventive</option>
                          <option value="">Corrective</option>
                        </select>
                      </td>
                      <td>
                        <textarea name="" id="" class="form-control"></textarea>
                      </td>
                      <td>
                        <div class="btn-group">
                          <a class="btn btn-danger" href="#"><i class="icon_close_alt2"></i></a>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </section>
            <div class="form-group">
              <div class="col-lg-offset-10 col-lg-2">
                <button class="btn btn-success" type="submit">Add Article</button>
              </div>
            </div>
            <div class="form-group ">
              <label for="address" class="control-label col-lg-2">Observations <span class="required">*</span></label>
              <div class="col-lg-10">
                <textarea class="form-control " id="address" name="address" required></textarea>
              </div>
            </div>
            <div class="form-group ">
              <label for="username" class="control-label col-lg-2">Responsable</label>
              <div class="col-lg-10">
                <input class="form-control " id="username" name="username" type="text" value="Jhon Medina" desable />
              </div>
            </div>
            <div class="form-group">
              <div class="col-lg-offset-2 col-lg-10">
                <button class="btn btn-primary" type="submit">Save</button>
                <button class="btn btn-default" type="button">Cancel</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </section>
  </div>
</div>
{{--main content end--}}
@endsection