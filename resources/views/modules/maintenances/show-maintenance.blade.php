@extends('template')
@section('main-content')
{{--overview start--}}
<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header"><i class="icon_tools"></i>Maintenances N°</h3>
    <ol class="breadcrumb">
      <li><i class="fa fa-home"></i><a href="{{ route('dashboard') }}">Dashboard</a></li>
      <li><i class="fa fa-cogs"></i><a href="{{ route('maintenances.index') }}">Maintenances</a></li>
      <li><i class="fa fa-cog"></i>Maintenance N°</li>
    </ol>
  </div>
</div>
{{--overview end--}}
{{--main content start--}}
<div class="row">
  <div class="col-lg-12">
    <div class="panel">
      <div class="panel-body">
        <header class="row margin-bottom bottom-line">
          <div class="col-lg-2">
            <img class="img-responsive" src="{{asset('img/multi.png')}}" alt="">
          </div>
          <div class="col-lg-10">
            <div class="row">
              <div class="col-lg-7 col-lg-push-1">
                <h3 class="page-header title center">Multipartes Sas</h3>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-7 col-lg-push-1">
                <div class="form-horizontal">
                  <div class="">
                    <p class="page-header sub-title center">Maintenance Order N°
                      <span>5555</span>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </header>
        <div class="row">
          <div class="col-lg-5 page-header">
            <div class="form-horizontal">
              <div class="form-group">
                <span class="col-lg-3">Date</span>
                <span class="col-lg-1">:</span>
                <p class="col-lg-4">21/09/2018</p>
              </div>
            </div>
          </div>
          <div class="col-lg-6 page-header">
            <div class="form-horizontal">
              <div class="form-group">
                <span class="col-lg-3">Department</span>
                <span class="col-lg-1">:</span>
                <span class="col-lg-2">Compras</span>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-5 page-header">
            <div class="form-horizontal">
              <div class="form-group">
                <span class="col-lg-4">Assigned To</span>
                <span class="col-lg-1">:</span>
                <span class="col-lg-4">Jhon Medina</span>
              </div>
            </div>
          </div>
          <div class="col-lg-6 page-header">
            <div class="form-horizontal">
              <div class="form-group">
                <span class="col-lg-3">Cedula</span>
                <span class="col-lg-1">:</span>
                <span class="col-lg-4">1234562</span>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <div class="table-responsive top-line bottom-line">
              <table class="table table-bordered ">
                <caption class="page-header-table">Description</caption>
                <thead>
                  <th>Fixed Asset</th>
                  <th>Article</th>
                  <th>Type Mantenance</th>
                  <th>Description</th>
                </thead>
                <tbody>
                  <tr>
                    <td>Dell</td>
                    <td>E45D56</td>
                    <td>23</td>
                    <td>15.000</td>
                  </tr>
                  <tr>
                    <td>Dell</td>
                    <td>E45D56</td>
                    <td>23</td>
                    <td>15.000</td>
                  </tr>
                  <tr>
                    <td>Mouse</td>
                    <td>Dell</td>
                    <td>E45D56</td>
                    <td>23</td>
                  </tr>
                  <tr>
                    <td>Mouse</td>
                    <td>Dell</td>
                    <td>E45D56</td>
                    <td>23</td>
                  </tr>
                  <tr>
                    <td>01</td>
                    <td>Dell</td>
                    <td>E45D56</td>
                    <td>23</td>
                  </tr>
                  <tr>
                    <td>01</td>
                    <td>Dell</td>
                    <td>E45D56</td>
                    <td>23</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12 page-header">
            <div class="form-horizontal">
              <div class="form-group">
                <span class="col-lg-2">Responsable</span>
                <span class="col-lg-1">:</span>
                <span class="col-lg-4">Carlos vivas</span>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <div class="form-horizontal">
              <div class="form-group">
                <span class="col-lg-2 page-header">Observations</span>
                <span class="col-lg-10 col-sm-12 page-paragreph">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Et
                  beatae, expedita, debitis quia harum reiciendis saepe est dicta eaque, necessitatibus asperiores?
                  Culpa,
                  quas sapiente voluptatibus perferendis saepe pariatur similique aspernatur?</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  {{--main content end--}}
  @endsection