{{-- {{ dd($fixedassets) }} --}} 
@extends('template.layout') 
@section('main-content') {{--overview start--}}
<section class="wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header"><i class="fa fa-desktop" aria-hidden="true"></i>Article Profile</h3>
      <ol class="breadcrumb">
        <li><i class="fa fa-home" aria-hidden="true"></i><a href="{{ route('home') }}">Home</a></li>
        <li><i class="fa fa-laptop" aria-hidden="true"></i><a href="{{ route('articles.index') }}">Articles</a></li>
        <li><i class="fa fa-hdd-o" aria-hidden="true"></i>Show Article</li>
      </ol>
    </div>
  </div>
</section>
{{--overview end--}} {{--main content start--}}
<div class="row">
  <div class="col-lg-12">
    <div class="panel-body panel-body-j">
      {{-- profile-widget start--}}
      <div class="row">
        <div class="col-lg-12">
          <div class="profile-widget profile-widget-info">
            <div class="panel-body">
              <div class="col-lg-2 col-sm-2">
                <h4>{{ $article->name }}</h4>
                <div class="follow-ava">
                  <img src="{{asset(" $article->image") }}" alt="" class="img-rounded img-responsive">
                </div>
              </div>
              <div class="col-lg-4 col-sm-4 follow-info">
                <p><span>Marca: </span>{{ $article->brand->name }}</p>
                <p><span>Serial: </span>{{ $article->serial }}</p>
                <p><span>Description: </span>{{ $article->description }}</p>
              </div>
              <div class="col-lg-2 col-sm-6 follow-info weather-category">
                <ul>
                  <li class="active">
                    <span class="block sub-title-show-article">Category</span>
                    <i class="block fa fa-tachometer fa-2x"></i>
                    <span>{{ $article->category->name }}</span>
                  </li>
                </ul>
              </div>
              <div class="col-lg-2 col-sm-6 follow-info weather-category">
                <ul>
                  <li class="active">
                    <span class="block sub-title-show-article">Fixed Asset</span>
                    <i class="block icon_genius fa-2x"></i>
                    <span>{{ $article->fixed->code }}</span>
                  </li>
                </ul>
              </div>
              <div class="col-lg-2 col-sm-6 follow-info weather-category">
                <ul>
                  <li class="active">
                    <span class="block sub-title-show-article">Employee</span>
                    <i class="block fa fa-user-circle fa-2x"></i>
                    <span>{{  $article->fixed->employees[0]->first_name ??  "Sin asignar"}}</span>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      {{-- profile-widget end--}} {{-- page start --}}
      <div class="row">
        <div class="col-lg-12">
          <section class="panel">
            <header class="panel-heading tab-bg-info" id="none-border">
              <ul class="nav nav-tabs">
                <li class="active">
                  <a data-toggle="tab" href="#delivery-certificate">
                      <i class="icon-home"></i>
                      <span>Delivery Certificate</span>
                    </a>
                </li>
                <li class="">
                  <a data-toggle="tab" href="#edit-profile">
                      <i class="icon-envelope"></i>
                      <span>Edit Article</span>
                    </a>
                </li>
              </ul>
            </header>
            <div class="panel-body">
              <div class="tab-content">
                {{-- delivery-certificate start --}}
                <div id="delivery-certificate" class="tab-pane active">
                  <div class="profile-activity">
                    <div class="act-time">
                      <div class="activity-body act-in">
                        <span class="arrow"></span>
                        <div class="text">
                          <a href="#" class="activity-img"><img class="avatar" src="img/chat-avatar.jpg" alt=""></a>
                          <p class="attribution"><a href="#">Jonatanh Doe</a> at 4:25pm, 30th Octmber 2014</p>
                          <p>It is a long established fact that a reader will be distracted layout</p>
                        </div>
                      </div>
                    </div>
                    <div class="act-time">
                      <div class="activity-body act-in">
                        <span class="arrow"></span>
                        <div class="text">
                          <a href="#" class="activity-img"><img class="avatar" src="img/chat-avatar.jpg" alt=""></a>
                          <p class="attribution"><a href="#">Jhon Loves </a> at 5:25am, 30th Octmber 2014</p>
                          <p>Knowledge speaks, but wisdom listens.</p>
                        </div>
                      </div>
                    </div>
                    <div class="act-time">
                      <div class="activity-body act-in">
                        <span class="arrow"></span>
                        <div class="text">
                          <a href="#" class="activity-img"><img class="avatar" src="img/chat-avatar.jpg" alt=""></a>
                          <p class="attribution"><a href="#">Jimy Smith</a> at 5:25am, 30th Octmber 2014</p>
                          <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum
                            tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam
                            egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                {{-- delivery-certificate end --}} {{--edit-profile start--}}
                <div id="edit-profile" class="tab-pane">
                  <section class="panel">
                    <div class="panel-body bio-graph-info">
                      <h1> Article Info</h1>
                      <form class="form-validate form-horizontal " id="register_form" method="post" action="{{ route('articles.update',$article->id)}}"
                        enctype="multipart/form-data">
                        <div class="form-group ">
                          <label for="article-fixed" class="control-label col-lg-2">Fixedasset <span class="required">*</span></label>
                          <div class="col-lg-10">
                            @if ($errors->has('article-fixed'))
                            <div class="alert alert-danger" role="alert">
                              {{ $errors->first() }}
                            </div>
                            @endif
                            <select class="form-control m-bot15" name="article-fixed">
                                @foreach ($fixedassets as $fixedasset)
                                 @if ($fixedasset->code === $article->fixed->code)
                                  <option selected value="{{ $fixedasset->id }}">{{ $fixedasset->code }}</option>
                                  @continue
                                 @endif
                                  <option value="{{ $fixedasset->id }}">{{ $fixedasset->code }}</option>
                                @endforeach
                              </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="article-name" class="control-label col-lg-2">Article name <span class="required">*</span></label>
                          <div class="col-lg-4">
                            @if ($errors->has('article-name'))
                            <div class="alert alert-danger" role="alert">
                              {{ $errors->first() }}
                            </div>
                            @endif
                            <input class="form-control" id="article-name" name="article-name" type="text" value="{{ old('article-name') ?? $article->name }}"
                              required/>
                          </div>
                          <label for="article-serial" class="control-label col-lg-2">Serial <span class="required">*</span></label>
                          <div class="col-lg-4">
                            @if ($errors->has('article-serial'))
                            <div class="alert alert-danger" role="alert">
                              {{ $errors->first() }}
                            </div>
                            @endif
                            <input class="form-control" id="article-serial" name="article-serial" type="text" value="{{ old('article->serial') ?? $article->serial }}"
                              required/>
                          </div>
                        </div>
                        <div class="form-group ">
                          <label for="article-quantity" class="control-label col-lg-2">Quantity <span class="required">*</span></label>
                          <div class="col-lg-4">
                            @if ($errors->has('article-quantity'))
                            <div class="alert alert-danger" role="alert">
                              {{ $errors->first() }}
                            </div>
                            @endif
                            <input class="form-control" id="article-quantity" name="article-quantity" type="text" value="{{ old('article-quantity') ?? $article->quantity }}"
                              required />
                          </div>
                          <label for="article-price" class="control-label col-lg-2">Price <span class="required">*</span></label>
                          <div class="col-lg-4">
                            @if ($errors->has('article-price'))
                            <div class="alert alert-danger" role="alert">
                              {{ $errors->first() }}
                            </div>
                            @endif
                            <input class="form-control" id="article-price" name="article-price" type="text" value="{{ old('article-price') ?? $article->price }}"
                              required />
                          </div>
                        </div>
                        <div class="form-group ">
                          <label for="article-category" class="control-label col-lg-2">Category <span class="required">*</span></label>
                          <div class="col-lg-4">
                            @if ($errors->has('article-category'))
                            <div class="alert alert-danger" role="alert">
                              {{ $errors->first() }}
                            </div>
                            @endif
                            <select class="form-control m-bot15" name="article-category">
                                @foreach ($categories as $category)
                                  @if ($category->id === $article->category->id)
                                    <option selected value="{{ $category->id }}">{{ $category->name }}</option>
                                    @continue  
                                  @endif
                                 <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                              </select>
                          </div>
                          <label for="article-brand" class="control-label col-lg-2">Brand <span class="required">*</span></label>
                          <div class="col-lg-4">
                            @if ($errors->has('article-brand'))
                            <div class="alert alert-danger" role="alert">
                              {{ $errors->first() }}
                            </div>
                            @endif
                            <select class="form-control m-bot15" name="article-brand">
                                  @foreach ($brands as $brand)
                                    @if ($brand->id === $article->brand->id)
                                      <option selected value="{{ $brand->id }}">{{ $brand->name }}</option>
                                      @continue  
                                    @endif
                                   <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                                  @endforeach
                                </select>
                          </div>
                        </div>
                        <div class="form-group ">
                          <label for="article-description" class="control-label col-lg-2">Description <span class="required">*</span></label>
                          <div class="col-lg-10">
                            @if ($errors->has('article-description'))
                            <div class="alert alert-danger" role="alert">
                              {{ $errors->first() }}
                            </div>
                            @endif
                            <textarea class="form-control " id="article-description" name="article-description" required>{{ old('articles-descrition') ?? $article->description }}</textarea>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-lg-offset-2 col-lg-10">
                            <button class="btn btn-primary" type="submit">Save</button>
                            <a class="btn btn-danger" href="{{ route('articles.show', $article->id) }}">Cancel</a>
                          </div>
                        </div>
                        {{ csrf_field() }}
                      </form>
                    </div>
                  </section>
                </div>
                {{--edit-profile end--}}
              </div>
            </div>
          </section>
        </div>
      </div>
      {{-- page end --}}
    </div>
  </div>
</div>
{{--main content end--}}
@endsection