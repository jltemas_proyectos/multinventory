@extends('template.layout')
@section('main-content')
{{--overview start--}}
<section class="wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header"><i class="fa fa-desktop"></i>Networks </h3>
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i><a href="{{ route('dashboard') }}">Dashboard</a></li>
        <li><i class="fa fa-home"></i><a href="{{ route('articles.index') }}">Articles</a></li>
        <li><i class="fa fa-desktop"></i>Networks</li>
      </ol>
    </div>
  </div>
</section>
{{--overview end--}}
{{--main content start--}}
<div class="row">
  <div class="col-lg-12">
    <div class="panel-body panel-body-j">
      <div class="row">
        <div class="col-lg-12">
          <section class="panel">
            <div class="table-responsive">
              <table class="table table-striped table-advance table-hover">
                <caption class="page-header-table">Networks</caption>
                <tbody class="form-horizontal">
                  <tr class="form-group row">
                    <th class="col-lg-1 col-sm-1 center"> N°</th>
                    <th class="col-lg-2 col-sm-2 center"><i class="fa fa-desktop"></i> Name</th>
                    <th class="col-lg-2 col-sm-2 center"><i class="fa fa-linode"></i> Brad</th>
                    <th class="col-lg-2 col-sm-2 center"><i class="fa fa-modx"></i> Model</th>
                    <th class="col-lg-2 col-sm-2 center"><i class="fa fa-archive"></i> Quantity</th>
                    <th class="col-lg-3 col-sm-2"><i class="icon_cogs"></i> Action</th>
                  </tr>
                  @foreach ($networks as $network)
                  @if (!$network->deleted_at)
                  <tr class="form-group">
                    <td class="col-lg-1 col-sm-1 center">{{ $count }}</td>
                    <td class="col-lg-2 col-sm-2 center">{{ $network->name }}</td>
                    <td class="col-lg-2 col-sm-2 center">{{ $network->brand }}</td>
                    <td class="col-lg-2 col-sm-2 center">{{$network->model}}</td>
                    <td class="col-lg-2 col-sm-2 center">515</td>
                    <td class="col-sm-3">
                      <div class="btn-group">
                        <a class="btn btn-primary" href="{{ route('articles.edit', $network->id) }}"><i
                            class="icon_plus_alt2"></i></a>
                        <a class="btn btn-success" href="{{ route('articles.show', $network->id) }}"><i
                            class="icon_check_alt2"></i></a>
                        <form class="form-delete" method="POST" action="{{ route('articles.destroy', $network->id) }}">
                          <button class="btn btn-danger form-delete-button" type="submit"><i
                              class="icon_close_alt2"></i></button>
                          {{ csrf_field() }}
                          {{ method_field('DELETE') }}
                        </form>
                      </div>
                    </td>
                  </tr>
                  @php $count++ @endphp
                  @endif
                  @endforeach
                </tbody>
              </table>
              {{-- paginación --}}
              <div class="row">
                <div class="col-lg-offset-5">
                  {{$networks->links() }}
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  </div>
</div>
{{--main content end--}}
@endsection