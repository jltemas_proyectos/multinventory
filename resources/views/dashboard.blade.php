@extends('template')
@section('main-content')
{{--overview start--}}
{{-- <section class="wrapper"> --}}
<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header"><i class="fa fa-tachometer"></i>Inventory</h3>
    <ul class="breadcrumb">
      <li><i class="fa fa-tachometer"></i>Dashboard</li>
    </ul>
  </div>
</div>
{{-- </section> --}}
{{--overview end--}}
{{--main content start--}}
<div class="row">
  <div class="col-lg-12">
    <div class="tab-pane" id="chartjs">
      <div class="row">
        <div class="col-lg-6">
          <section>
            <header class="panel-heading ">
              <h3 class="card-title">
                <a href="{{route('dashboard.list-desktops')}}">PC Desktop</a>
              </h3>
            </header>
            <div class="panel-body text-center">
              <a href="{{route('dashboard.list-desktops')}}">
                <img src="{{asset('storage/images/home/desktop.jpg')}}" class="img-responsive card-image"
                  alt="desktop">
              </a>
              <p class="card-content text-left">Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae
                repudiandae, illum eveniet exercitationem culpa perferendis ullam praesentium nobis eum at tempora qui
                sint? Harum, amet itaque dicta porro fugiat voluptatum?</p>
              <p class="card-content text-left">Cantida de equipos: <span class="card-content-info">20</span></p>
            </div>
          </section>
        </div>
        <div class="col-lg-6">
          <section>
            <header class="panel-heading ">
              <h3 class="card-title">
                <a href="{{route('dashboard.list-laptops')}}">Laptop</a>
              </h3>
            </header>
            <div class="panel-body text-center">
              <a href="{{route('dashboard.list-laptops')}}">
                <img src="{{asset('storage/images/home/laptop.png')}}" class="img-responsive card-image"
                  alt="laptop">
              </a>
              <p class="card-content text-left">Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae
                repudiandae, illum eveniet exercitationem culpa perferendis ullam praesentium nobis eum at tempora qui
                sint? Harum, amet itaque dicta porro fugiat voluptatum?</p>
              <p class="card-content text-left">Cantida de equipos: <span class="card-content-info">20</span></p>
            </div>
          </section>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6">
          <section>
            <header class="panel-heading ">
              <h3 class="card-title">
                <a href="{{route('dashboard.list-desktops')}}">Servers</a>
              </h3>
            </header>
            <div class="panel-body text-center">
              <a href="{{route('dashboard.list-servers')}}">
                <img src="{{asset('storage/images/home/server.png')}}" class="img-responsive card-image"
                  alt="server">
              </a>
              <p class="card-content text-left">Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae
                repudiandae, illum eveniet exercitationem culpa perferendis ullam praesentium nobis eum at tempora qui
                sint? Harum, amet itaque dicta porro fugiat voluptatum?</p>
              <p class="card-content text-left">Cantida de equipos: <span class="card-content-info">20</span></p>
            </div>
          </section>
        </div>
        <div class="col-lg-6">
          <section>
            <header class="panel-heading ">
              <h3 class="card-title">
                <a href="{{route('dashboard.list-desktops')}}">Peripherals</a>
              </h3>
            </header>
            <div class="panel-body text-center">
              <a href="{{route('dashboard.list-peripherals')}}">
                <img src="{{asset('storage/images/home/peripherals.png')}}" class="img-responsive card-image"
                  alt="peripherals">
              </a>
              <p class="card-content text-left">Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae
                repudiandae, illum eveniet exercitationem culpa perferendis ullam praesentium nobis eum at tempora qui
                sint? Harum, amet itaque dicta porro fugiat voluptatum?</p>
              <p class="card-content text-left">Cantida de equipos: <span class="card-content-info">20</span></p>
            </div>
          </section>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6">
          <section>
            <header class="panel-heading ">
              <h3 class="card-title">
                <a href="{{route('dashboard.list-desktops')}}">Printers</a>
              </h3>
            </header>
            <div class="panel-body text-center">
              <a href="{{route('dashboard.list-printers')}}">
                <img src="{{asset('storage/images/home/printers.jpg')}}" class="img-responsive card-image"
                  alt="printers">
              </a>
              <p class="card-content text-left">Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae
                repudiandae, illum eveniet exercitationem culpa perferendis ullam praesentium nobis eum at tempora qui
                sint? Harum, amet itaque dicta porro fugiat voluptatum?</p>
              <p class="card-content text-left">Cantida de equipos: <span class="card-content-info">20</span></p>
            </div>
          </section>
        </div>
        <div class="col-lg-6">
          <section>
            <header class="panel-heading ">
              <h3 class="card-title">
                <a href="{{route('dashboard.list-networks')}}">Network</a>
              </h3>
            </header>
            <div class="panel-body text-center">
              <a href="{{route('dashboard.list-networks')}}">
                <img src="{{asset('storage/images/home/network.jpg')}}" class="img-responsive card-image"
                  alt="networks">
              </a>
              <p class="card-content text-left">Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae
                repudiandae, illum eveniet exercitationem culpa perferendis ullam praesentium nobis eum at tempora qui
                sint? Harum, amet itaque dicta porro fugiat voluptatum?</p>
              <p class="card-content text-left">Cantida de equipos: <span class="card-content-info">20</span></p>
            </div>
          </section>
        </div>
      </div>
    </div>
  </div>
</div>
{{--main content end--}}
@endsection